<?php

namespace backend\models;

use backend\components\behaviors\ImageBehavior;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "accessories".
 *
 * @property integer $id
 * @property string $name
 * @property double $price
 * @property string $descr
 * @property integer $accessory_type_id
 *
 * @property AccessoryType $accessoryType
 * @property Product[] $products
 * @property string $link
 */
class Accessory extends \yii\db\ActiveRecord
{

    const IMAGE = 'image';

    public $quantity = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accessories';
    }

    public function behaviors()
    {
        return [
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'product_ids' => 'products',
                ],
            ],
            [
                'class' => ImageBehavior::className(),
                'model_path' => 'accessories',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['accessory_type_id'], 'integer'],
            [['price'], 'double'],
            [['price'], 'required'],
            [['descr'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['accessory_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccessoryType::className(), 'targetAttribute' => ['accessory_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'descr' => Yii::t('app', 'Description'),
            'accessory_type_id' => Yii::t('app', 'Accessory Type'),
            'currency' => Yii::t('app', 'Currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessoryType()
    {
        return $this->hasOne(AccessoryType::className(), ['id' => 'accessory_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_accessory_map', ['accessory_id' => 'id']);
    }

    public function getLink()
    {
        return Url::to(['accessory/view', 'id' => $this->id]);
    }

    public function getPriceRound()
    {
        if ($this->price)
            return Setting::roundPrice($this->price, 100);
        else
            return 0;
    }
}
