<?php

namespace backend\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "brands".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Product[] $products
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['brand_id' => 'id']);
    }

    public static function getProductBrands()
    {
        $query = new Query();
        $ids = $query
            ->select('brand_id')
            ->from('products')
            ->where(['product_type_id' => 1])
            ->groupBy('brand_id')
            ->column();

        $brands = Brand::find()->andFilterWhere(['IN', 'id', $ids])->all();
        return $brands;
    }

    public static function getGadgetBrands()
    {
        $query = new Query();
        $ids = $query
            ->select('brand_id')
            ->from('products')
            ->where(['!=', 'product_type_id', 1])
            ->groupBy('brand_id')
            ->column();

        $brands = Brand::find()->andFilterWhere(['IN', 'id', $ids])->all();
        return $brands;
    }
}

