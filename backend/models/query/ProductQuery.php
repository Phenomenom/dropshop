<?php

namespace backend\models\query;
use backend\models\Product;
use yii\db\ActiveQuery;
use yii\db\Expression;

class ProductQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere([
            'visible' => true,
        ]);
    }

    public function novelty()
    {
        return $this->andWhere([
            'like', 'states', Product::STATE_NEW
        ]);
    }

    public function hit() {
        return $this->andWhere([
            'like', 'states', Product::STATE_HIT
        ]);
    }

    public function random()
    {
        return $this->orderBy(new Expression('rand()'));
    }
}