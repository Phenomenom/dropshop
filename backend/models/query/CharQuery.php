<?php

namespace backend\models\query;

use yii\db\ActiveQuery;

class CharQuery extends ActiveQuery
{
    public function required()
    {
        return $this->andWhere([
            'is_required' => true
        ]);
    }

    public function productType($product_type_id)
    {
        return $this->andWhere([
            'product_type_id' => $product_type_id
        ]);
    }
}