<?php

namespace backend\models\query;
use backend\models\Post;
use yii\db\ActiveQuery;
use yii\db\Expression;

class PostQuery extends ActiveQuery
{
    public function published()
    {
        return $this->andWhere([
            'status' => Post::STATUS_PUBLISHED,
        ]);
    }

    public function latest()
    {
        return $this->orderBy([
            'add_date' => SORT_DESC
        ]);
    }
}