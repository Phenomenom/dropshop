<?php
namespace backend\models;
use yii\base\Model;

/**
 * ProductPrice form
 */
class ProductPrice extends Model
{
    public $name;
    public $price;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            [['name'], 'string'],
            [['price'], 'integer'],
        ];
    }
}
