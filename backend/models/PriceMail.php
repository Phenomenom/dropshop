<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "price_mail".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $email
 * @property string $hash
 *
 * @property Product $product
 */
class PriceMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],

            ['email', 'string', 'max' => 255],
            ['email', 'email'],
            [['email', 'product_id'], 'required'],

            [['product_id', 'email'], 'unique', 'targetAttribute' => ['product_id', 'email']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],

            ['hash', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
