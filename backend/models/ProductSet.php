<?php

namespace backend\models;

use backend\components\behaviors\ImageBehavior;
use Yii;

/**
 * This is the model class for table "product_sets".
 *
 * @property integer $id
 * @property double $price
 * @property string $text
 * @property integer $product_id
 *
 * @property Product $product
 *
 * @method getImages(string $type) string[]
 * @method getImage(string $type) string
 */
class ProductSet extends \yii\db\ActiveRecord
{

    const IMAGE = 'image';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_sets';
    }

    public function behaviors()
    {
        return [
            [
                'class' => ImageBehavior::className(),
                'model_path' => 'product-sets',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'required'],
            [['price'], 'double'],
            [['text'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'price' => Yii::t('app', 'Price'),
            'text' => Yii::t('app', 'Text'),
            'product_id' => Yii::t('app', 'Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
