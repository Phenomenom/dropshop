<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Accessory;

/**
 * AccessorySearch represents the model behind the search form about `backend\models\Accessory`.
 */
class AccessorySearch extends Accessory
{

    const CURRENCY_TG = 0;
    const CURRENCY_USD = 1;

    public $currency = self::CURRENCY_TG;

    public function formName() {
        return '';
    }

    public static function getCurrencyLabels() {
        return [
            self::CURRENCY_TG => Yii::t('app', 'Tenge'),
            self::CURRENCY_USD => Yii::t('app', 'USD'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price'], 'integer'],
            [['name', 'descr'], 'safe'],
            ['currency', 'integer'],
            ['accessory_type_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accessory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'accessory_type_id' => $this->accessory_type_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'descr', $this->descr]);

        return $dataProvider;
    }
}
