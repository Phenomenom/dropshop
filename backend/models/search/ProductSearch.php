<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Product;

/**
 * ProductSearch represents the model behind the search form about `backend\models\Product`.
 */
class ProductSearch extends Product
{

    const CURRENCY_TG = 0;
    const CURRENCY_USD = 1;

    public $key;
    public $active = false;
    public $currency = self::CURRENCY_TG;
    public $brands;

    public $isPhone = null;

    public function formName() {
        return '';
    }

    public static function getCurrencyLabels() {
        return [
            self::CURRENCY_TG => Yii::t('app', 'Tenge'),
            self::CURRENCY_USD => Yii::t('app', 'USD'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price', 'price_credit', 'priority'], 'integer'],
            [['name', 'text', 'hash', 'descr', 'states', 'video_url'], 'safe'],
            ['currency', 'integer'],
            ['brands', 'safe'],
            ['product_type_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();
        if ($this->active)
            $query->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'price',
                    'name',
                    'add_date',
                    'states',
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->isPhone === true) {
            $query->andFilterWhere(['product_type_id' => 1]);
        }

        if($this->isPhone === false) {
            $query->andFilterWhere(['!=', 'product_type_id', 1]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_type_id' => $this->product_type_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'states', $this->states])
//            ->andFilterWhere(['like', 'prices', $this->prices])
            ->andFilterWhere(['like', 'descr', $this->descr])
            ->andFilterWhere(['like', 'video_url', $this->video_url]);

        if ($this->key) {
            $query->andFilterWhere(['OR',
                ['like', 'name', $this->key],
                ['like', 'text', $this->key],
                ['like', 'descr', $this->key],
            ]);
        }

        if($this->brands) {
            $query->andFilterWhere([
                'IN', 'brand_id', $this->brands
            ]);
        }
//        echo $query->createCommand()->rawSql;die();

        return $dataProvider;
    }
}
