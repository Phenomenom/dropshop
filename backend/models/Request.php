<?php

namespace backend\models;

use common\helpers\shoppingcart\ShoppingCart;
use Yii;

/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $city
 * @property string $body
 * @property integer $product_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $type
 * @property integer $product_set_id
 *
 * @property Product $product
 * @property ProductSet $productSet
 */
class Request extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 0;
    const STATUS_VIEWED = 1;
    const STATUS_COMPLETED = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'city'], 'required'],
            [['body'], 'string'],
            [['product_id', 'status', 'type', 'product_set_id'], 'integer'],
            [['name', 'city'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 18],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Person Name'),
            'phone' => Yii::t('app', 'Phone'),
            'city' => Yii::t('app', 'City'),
            'body' => Yii::t('app', 'Body'),
            'product_id' => Yii::t('app', 'Product ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created at'),
            'type' => Yii::t('app', 'Type'),
            'product_set_id' => Yii::t('app', 'Product Set'),
        ];
    }

    public static function getStatusLabels() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Status New'),
            self::STATUS_VIEWED => Yii::t('app', 'Status Viewed'),
            self::STATUS_COMPLETED => Yii::t('app', 'Status Completed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return null|ProductSet
     */
    public function getProductSet()
    {
        if($this->product_set_id !== null) {
            return ProductSet::findOne(['id' => $this->product_set_id]);
        }
        return null;
    }

    public function generateBody()
    {
        if($this->product_id !== null && $this->type !== null) {

            /* @var $item Product */
            /* @var $productSet ProductSet */
            $item = null;
            $productSet = null;

            switch($this->type) {
                case ShoppingCart::TYPE_PRODUCT:
                    $item = Product::findOne($this->product_id);
                    $productSet = $this->productSet;
                    break;
                case ShoppingCart::TYPE_ACCESSORY:
                    $item = Accessory::findOne($this->product_id);
                    break;
                default:
                    return '';
                    break;
            }

            $rate = Setting::getExchange();
            $tengeCost = $rate * $item->price;

            $body = "";
            $body.= "<b>{$item->name}</b><br>";

            if($productSet !== null) {
                $body.= "<i>Разновидность {$productSet->text}</i><br>";
            }

            $body.= "Цена: {$item->price}$ ({$tengeCost} тг.)<br>";
            $body.= "Курс: {$rate} тг/$<br>";

            return $body;
        }
        return '';
    }
}
