<?php

namespace backend\models;

use backend\components\behaviors\ImageBehavior;
use backend\controllers\ProductController;
use backend\models\query\ProductQuery;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $name
 * @property string $priority
 * @property double $price
 * @property double $price_credit
 * @property integer $hash
 * @property string $descr
 * @property string $video_url
 * @property string $add_date
 * @property integer $visible
 * @property integer $brand_id
 * @property integer $product_type_id
 * @property string $states
 *
 *
 * @property Char[] $chars
 * @property ProductChar[] $productChars
 * @property int[] $stateArray
 * @property Product[] $productMates
 * @property ProductSet[] $productSets
 * @property ProductType $productType
 * @property Brand $brand
 * @property Accessory[] $accessories
 *
 * @method getImages(string $type) string[]
 * @method getImage(string $type) string
 * @property string $link
 *
 *
 * @property ProductChar phoneDisplay
 * @property ProductChar phoneBattery
 * @property ProductChar phoneCamera
 * @property ProductChar phoneRam
 *
 */
class Product extends \yii\db\ActiveRecord
{

    const IMAGE_MAJOR = 'major';
    const IMAGE_BANNER = 'banner';
    const IMAGE_MINOR = 'minor';

    const STATE_INSTOCK = 0;
    const STATE_ORDER = 1;
    const STATE_PENDING = 2;
    const STATE_NEW = 3;
    const STATE_SALE = 4;
    const STATE_INSTALLMENT = 5;
    const STATE_SOON = 6;
    const STATE_HIT = 7;

    public $quantity = 0;
    public $product_set_id = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],

            ['hash', 'required'],
            ['hash', 'string', 'max' => 63],

            [['visible', 'priority', 'brand_id', 'product_type_id'], 'integer'],
            [['price','price_credit'], 'double'],
            [['descr', 'video_url'], 'string'],
            [['add_date'], 'safe'],

            ['text', 'string'],
            ['states', 'string'],
            ['stateArray', 'safe'],

            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['product_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductType::className(), 'targetAttribute' => ['product_type_id' => 'id']],

            [['product_mate_ids'], 'each', 'rule' => ['integer']],
            [['accessory_ids'], 'each', 'rule' => ['integer']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'product_mate_ids' => 'productMates',
                    'accessory_ids' => 'accessories',
                ],
            ],
            [
                'class' => ImageBehavior::className(),
                'model_path' => 'products',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'price_credit' => Yii::t('app', 'Price Credit'),
            'priceRound' => Yii::t('app', 'Price Round'),
            'descr' => Yii::t('app', 'Description'),
            'video_url' => Yii::t('app', 'Video URL'),
            'priority' => Yii::t('app', 'Priority'),
            'add_date' => Yii::t('app', 'Add Date'),
            'visible' => Yii::t('app', 'Visible'),
            'brand_id' => Yii::t('app', 'Brand'),
            'product_type_id' => Yii::t('app', 'Product Type'),
            'states' => Yii::t('app', 'States'),
            'stateArray' => Yii::t('app', 'States'),
            'product_mate_ids' => Yii::t('app', 'Product Mates'),
            'accessory_ids' => Yii::t('app', 'Accessories'),
            'currency' => Yii::t('app', 'Currency'),
        ];
    }

    public static function getStateLabels()
    {
        return [
            self::STATE_INSTOCK => 'В наличии',
            self::STATE_ORDER => 'На заказ',
            self::STATE_PENDING => 'В ожидании',
            self::STATE_NEW => 'Новинка',
            self::STATE_SALE => 'Распродажа',
            self::STATE_INSTALLMENT => 'Рассрочка',
            self::STATE_SOON => 'Скоро в продаже',
            self::STATE_HIT => 'Хит',
        ];
    }

    public static function getStateLabelColor() {
        return [
            self::STATE_INSTOCK => 'success',
            self::STATE_ORDER => 'info',
            self::STATE_PENDING => 'danger',
            self::STATE_NEW => 'warning',
            self::STATE_SALE => 'warning',
            self::STATE_INSTALLMENT => 'danger',
            self::STATE_SOON => 'primary',
            self::STATE_HIT => 'danger',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChars()
    {
        return $this->hasMany(Char::className(), ['id' => 'char_id'])->viaTable('product_char', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductChars()
    {
        return $this->hasMany(ProductChar::className(), ['product_id' => 'id']);
    }

    public function getPhoneDisplay() {
        return $this->hasOne(ProductChar::className(), ['product_id' => 'id'])->andWhere(['char_id' => 11]);
    }
    public function getPhoneBattery() {
        return $this->hasOne(ProductChar::className(), ['product_id' => 'id'])->andWhere(['char_id' => 24]);
    }
    public function getPhoneCamera() {
        return $this->hasOne(ProductChar::className(), ['product_id' => 'id'])->andWhere(['char_id' => 19]);
    }
    public function getPhoneRam() {
        return $this->hasOne(ProductChar::className(), ['product_id' => 'id'])->andWhere(['char_id' => 14]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSets()
    {
        return $this->hasMany(ProductSet::className(), ['product_id' => 'id'])->orderBy(['price' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductMates()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_mate_id'])->viaTable('product_product_map', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessories()
    {
        return $this->hasMany(Accessory::className(), ['id' => 'accessory_id'])->viaTable('product_accessory_map', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'product_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public function beforeValidate()
    {

        if (!$this->price) $this->price = 0;
        if (!$this->price_credit) $this->price_credit = 0;
        if (!$this->priority) $this->priority = 0;

        return parent::beforeValidate();
    }

    public function beforeDelete()
    {
        foreach ($this->productChars as $productChar) {
            $productChar->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @return ProductPrice[]
     */
    public function getStateArray()
    {
        $result = [];
        if ($this->states) {
            $array = json_decode($this->states);
            foreach ($array as $item) {
                $result[] = $item;
            }
        }
        return $result;
    }

    public function setStateArray($array)
    {
        /* @var $array ProductPrice[] */
        $result = [];
        if ($array) {
            foreach ($array as $item) {
                $result[] = $item;
            }
        }
        $this->states = json_encode($result);
    }

    public function getLink()
    {
        return Url::to([($this->isPhone() ? 'phone' : 'gadget') . '/view', 'id' => $this->id]);
    }

    public function getPriceRound()
    {
        $price = $this->price;

        if($this->product_set_id === null) {
            /* @var $cheapestProductSet ProductSet */
            $cheapestProductSet = $this->getProductSets()->orderBy(['price' => SORT_ASC])->one();
            if($cheapestProductSet) {
                $price += $cheapestProductSet->price;
            }
        } else {
            /* @var $productSet ProductSet */
            $productSet = $this->getProductSets()->where(['id' => $this->product_set_id])->one();
            if($productSet) {
                $price += $productSet->price;
            }
        }

        if ($this->price)
            return Setting::roundPrice($price, 500);
        else
            return 0;
    }

    /**
     * @return null|ProductSet
     */
    public function getProductSet()
    {
        if($this->product_set_id !== null) {
            $productSet = ProductSet::findOne(['id' => $this->product_set_id]);
            if($productSet !== null) {
                return $productSet;
            }
        }
        return null;
    }

    /**
     * @return ProductQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public function sendPriceMail()
    {
        $priceMails = PriceMail::findAll(['product_id' => $this->id]);

        $subject = 'Снижение цены ' . $this->name;

        foreach($priceMails as $priceMail) {
            $params  = [
                'product' => $this,
                'rate' => Setting::getExchange(),
                'email' => $priceMail->email,
                'hash' => $priceMail->hash,
            ];

            $mail = Yii::$app->mailer->compose('layouts/price', $params)
                ->setFrom(Yii::$app->params['shopMail'])
                ->setTo($priceMail->email)
                ->setSubject($subject);

            $mail->send();
        }
    }

    public function isPhone() {
        return $this->product_type_id == 1;
    }

    public function isGadget() {
        return $this->product_type_id != 1;
    }
}
