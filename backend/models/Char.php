<?php

namespace backend\models;

use backend\models\query\CharQuery;
use Yii;

/**
 * This is the model class for table "chars".
 *
 * @property integer $id
 * @property string $name
 * @property string $values
 * @property integer $category_id
 * @property integer $product_type_id
 * @property boolean $is_required
 *
 * @property Category $category
 * @property Product[] $products
 * @property ProductChar[] $productChars
 * @property ProductType $productType
 */
class Char extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chars';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'is_required'], 'required'],
            [['values'], 'string'],
            [['category_id', 'product_type_id'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['product_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductType::className(), 'targetAttribute' => ['product_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'values' => Yii::t('app', 'Values'),
            'category_id' => Yii::t('app', 'Category ID'),
            'is_required' => Yii::t('app', 'Auto addition'),
            'product_type_id' => Yii::t('app', 'Product Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_char', ['char_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductChars()
    {
        return $this->hasMany(ProductChar::className(), ['char_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'product_type_id']);
    }

    public static function find()
    {
        return new CharQuery(get_called_class());
    }
}
