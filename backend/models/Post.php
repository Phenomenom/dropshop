<?php

namespace backend\models;

use backend\components\behaviors\ImageBehavior;
use backend\models\query\PostQuery;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_text
 * @property string $full_text
 * @property integer $views
 * @property integer $status
 * @property string $add_date
 *
 * @method getImages(string $type) string[]
 * @method getImage(string $type) string
 */
class Post extends \yii\db\ActiveRecord
{
    const STATUS_HIDDEN = 0;
    const STATUS_PUBLISHED = 1;

    const IMAGE_SMALL = 'small';
    const IMAGE_FULL = 'full';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['views', 'status'], 'required'],
            [['short_text', 'full_text'], 'string'],
            [['views', 'status'], 'integer'],
            [['add_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => ImageBehavior::className(),
                'model_path' => 'news',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'short_text' => Yii::t('app', 'Short Text'),
            'full_text' => Yii::t('app', 'Full Text'),
            'views' => Yii::t('app', 'Views'),
            'status' => Yii::t('app', 'Status'),
            'add_date' => Yii::t('app', 'Add Date'),
        ];
    }

    public static function getStatusLabels() {
        return [
            self::STATUS_HIDDEN => Yii::t('app', 'Post Hidden'),
            self::STATUS_PUBLISHED => Yii::t('app', 'Post Published'),
        ];
    }

    /**
     * @return PostQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }
}
