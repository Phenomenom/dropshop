<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

/**
 * This is the model class for table "chars".
 *
 *
 * @property Category $category
 * @property Product[] $products
 * @property ProductChar[] $productChars
 */
class Setting extends Model
{
    static $exchange = null;
    const ROUNDER = 500;

    public static function getExchange() {
        if(self::$exchange == null) {
            $query = new Query();
            $query->select('alias,value')
                ->from('settings')
                ->where(['alias' => 'exchange']);
            $row = $query->one();
            self::$exchange = intval($row['value']);
        }
        return self::$exchange;
    }

    public static function roundPrice($value, $round) {
        $value = ceil($value * self::getExchange() / $round) * $round;
        return $value;
    }
}
