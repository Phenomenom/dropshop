<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_char".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $char_id
 * @property string $value
 *
 * @property Char $char
 * @property Product $product
 */
class ProductChar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_char';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['char_id', 'value'], 'required'],
            [['product_id', 'char_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['product_id', 'char_id'], 'unique', 'targetAttribute' => ['product_id', 'char_id'], 'message' => 'The combination of Product ID and Char ID has already been taken.'],
            [['char_id'], 'exist', 'skipOnError' => true, 'targetClass' => Char::className(), 'targetAttribute' => ['char_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'char_id' => Yii::t('app', 'Char ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChar()
    {
        return $this->hasOne(Char::className(), ['id' => 'char_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
