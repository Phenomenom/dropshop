<?php

namespace backend\models;

use backend\components\behaviors\ImageBehavior;
use backend\controllers\SliderController;
use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $link
 * @property string $text
 */
class Slider extends \yii\db\ActiveRecord
{

    const IMAGE = 'image';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    public function behaviors()
    {
        return [
            [
                'class' => ImageBehavior::className(),
                'model_path' => 'slider',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link' => Yii::t('app', 'Link'),
            'text' => Yii::t('app', 'Text'),
        ];
    }
}
