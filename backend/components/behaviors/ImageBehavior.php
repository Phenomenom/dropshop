<?php

namespace backend\components\behaviors;

use backend\controllers\ImageController;
use kartik\file\FileInput;
use Yii;
use yii\base\Event;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class ImageBehavior extends Behavior
{

    public $model_path;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function beforeDelete(Event $event)
    {
        $frontend = Yii::getAlias('@frontend') . '/web/';
        $model = $event->sender;

        $dir = $frontend . ImageController::IMAGE_PATH . $this->model_path . '/' . $model->id;
        if (file_exists($dir) || is_dir($dir)) {
            FileHelper::removeDirectory($dir);
        }
    }

    public function createDirectory() {
        $frontend = Yii::getAlias('@frontend') . '/web/';

        $dir = $frontend . ImageController::IMAGE_PATH . $this->model_path . '/' . $this->owner->id;
        if (!file_exists($dir) && !is_dir($dir)) {
            FileHelper::createDirectory($dir);
        }
    }

    public function getImage($type)
    {
        $image = $this->getImages($type, true);
        if ($image)
            return $image;
        return null;
    }

    /**
     * @param $type
     * @param bool $is_first
     * @throws \yii\base\Exception
     * @return string[]
     */
    public function getImages($type, $is_first = false)
    {
        $frontend = Yii::getAlias('@frontend') . '/web/';
        $imagesPath = ImageController::IMAGE_PATH . $this->owner->model_path . '/';

        $model = $this->owner;
        /* @var $model ActiveRecord */

        try {
            $images = FileHelper::findFiles($frontend . $imagesPath . $model->id);
        } catch (InvalidParamException $exception) {
            FileHelper::createDirectory($frontend . $imagesPath . $model->id);
            return [];
        }

        $array = [];
        $frontend = str_replace('\\', '/', $frontend);

        foreach ($images as $image) {
            $image = str_replace('\\', '/', $image);
            $file = substr($image, strrpos($image, '/') + 1);
            if (strpos($file, $type) !== false) {
                $path = str_replace($frontend, '', $image);
                if($is_first)
                    return $path;
                else
                    $array[] = $path;
            }
        }
        return $array;
    }

    public function getImageUploader($type)
    {
        $preview = [];
        $previewConfig = [];
        $classname = $this->owner->className();

        foreach ($this->getImages($type) as $image) {
            $filename = substr($image, strrpos($image, '/') + 1);
            $preview[] = $this->getPreview($image);
            $previewConfig[] = $this->getPreviewConfig($classname, $this->owner->id, $filename);
        }

        return FileInput::widget([
            'name' => 'images[]',
            'language' => 'ru',
            'pluginOptions' => [
                'previewFileType' => 'image',
                'uploadUrl' => Url::to([
                    "/image/file-upload",
                    'id' => $this->owner->id,
                    'type' => $type,
                    'classname' => $classname,
                ]),
                'initialPreview' => $preview,
                'initialPreviewConfig' => $previewConfig,
                'overwriteInitial' => false,
                'maxFileCount' => 1,
                'allowedFileExtensions' => ["jpg", "png",],
                'showRemove' => false,
                'showClose' => false,
            ]
        ]);
    }

    public function getPreview($url)
    {
        return Html::img('/' . $url, ['class' => 'file-preview-image', 'alt' => '', 'title' => '', 'style' => 'max-width: 300px; max-height: 400px;']);
    }

    public function getPreviewConfig($classname, $id, $filename)
    {
        return [
            'caption' => $filename,
            'url' => Url::to([
                "/image/file-delete",
                'id' => $id,
                'filename' => $filename,
                'classname' => $classname,
            ]),
            'data' => $filename,
        ];
    }
}