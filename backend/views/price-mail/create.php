<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PriceMail */

$this->title = Yii::t('app', 'Create Price Mail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Price Mails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-mail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
