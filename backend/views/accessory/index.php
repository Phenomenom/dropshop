<?php

use backend\models\AccessoryType;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $accessoryType AccessoryType */

$this->title = Yii::t('app', 'Accessories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accessory-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create') . ' ' . ($accessoryType ? $accessoryType->name : ''),
            ['create', 'accessory_type_id' => ($accessoryType ? $accessoryType->id : '')],
            ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'price',
            'descr:ntext',
            'accessory_type_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
