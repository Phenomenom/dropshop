<?php

use backend\models\Accessory;

/* @var $this yii\web\View */
/* @var $model backend\models\Accessory */

?>

<style>
    .fileinput-remove, .fileinput-upload {
        display: none;
    }

    .file-drop-zone {
        height: auto;
    }
</style>

<br>

<div class="row">
    <div class="col-md-12">
        <h2>Изображение</h2>
        <?= $model->getImageUploader(Accessory::IMAGE) ?>
    </div>
</div>
