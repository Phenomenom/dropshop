<?php

use yii\bootstrap\Collapse;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <style>
        .slider {
            width: 100%;
        }

        .slider__image {
            width: 100%;
        }
    </style>

    <h2>Превью</h2>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?= $this->render('@frontend/views/layouts/_slider') ?>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create Slider'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'link',
            'text:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
