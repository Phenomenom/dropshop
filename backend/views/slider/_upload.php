<?php

/* @var $this yii\web\View */
use backend\models\Slider;

/* @var $model backend\models\Slider */

?>

<style>
    .fileinput-remove, .fileinput-upload {
    display: none;
}

    .file-drop-zone {
    height: auto;
}
</style>

<br>

<div class="row">
    <div class="col-md-12">
        <h2>Изображение</h2>
        <?= $model->getImageUploader(Slider::IMAGE) ?>
    </div>
</div>
