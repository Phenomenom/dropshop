<?php

/* @var $this yii\web\View */
use backend\models\ProductSet;

/* @var $model backend\models\ProductSet */

?>

<style>
    .fileinput-remove, .fileinput-upload {
        display: none;
    }

    .file-drop-zone {
        height: auto;
    }
</style>

<br>

<div class="row">
    <div class="col-md-12">
        <h2>Изображение</h2>
        <?= $model->getImageUploader(ProductSet::IMAGE) ?>
    </div>
</div>
