<?php

use backend\models\Char;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $productPrice backend\models\ProductPrice */
/* @var $index integer */

?>

<?php
// necessary for update action.
?>

<tr class="dynamic-price">
    <td>
        <?= $form->field($productPrice, "[{$index}]name", ['template' => "{input}\n{error}"])->textInput([
            'maxlength' => true,
            'placeholder'=> 'Если модификация одна, название можно оставить пустым..',
        ]) ?>
    </td>
    <td>
        <?= $form->field($productPrice, "[{$index}]price", ['template' => "{input}\n{error}"])->textInput(['maxlength' => true]) ?>
    </td>

    <td><a href="javascript:void(0);" class="remove-price"><span class="glyphicon glyphicon-trash"></span></a></td>
</tr>