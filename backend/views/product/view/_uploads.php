<?php

use backend\models\Product;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

?>

<style>
    .fileinput-remove, .fileinput-upload {
        display: none;
    }

    .file-drop-zone {
        height: auto;
    }
</style>

<br>
<div class="row text-center">
    <h3 style="color: red">Для загрузки изображений на сервер, нажмите на кнопку "Upload file" под каждой картинкой.</h3>
</div>

<div class="row">
    <div class="col-md-6">
        <h2>Основное изображение</h2>
        <?= $model->getImageUploader(Product::IMAGE_MAJOR);
        ?>
    </div>
    <div class="col-md-6">
        <h2>Баннер</h2>
        <?= $model->getImageUploader(Product::IMAGE_BANNER);
        ?>
    </div>
    <div class="col-md-12">
        <h2>Изображения</h2>
        <?= $model->getImageUploader(Product::IMAGE_MINOR);
        ?>
    </div>
</div>