<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

use backend\models\ProductChar;

$chars = "<table>";
    $productChars = $model->productChars;
    foreach ($productChars as $productChar) {
    /* @var $productChar ProductChar */
    $chars .= "<tr>";
        $chars .= "<td><b>{$productChar->char->name}</b>&nbsp;&nbsp;&nbsp;</td>";
        $chars .= "<td>{$productChar->value}</td>";
        $chars .= "</tr>";
    }
    $chars .= "</table>";

?>

<?= $chars ?>