<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $productSets yii\data\ActiveDataProvider */

use backend\models\ProductSet;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<p>
    <?= Html::a(Yii::t('app', 'Create Product Set'), ['product-set/create', 'product_id' => $model->id], ['class' => 'btn btn-success']) ?>
</p>

<style>
    .view-image {
        max-height: 400px;
        max-width: 400px;;
    }
</style>

<?= GridView::widget([
    'dataProvider' => $productSets,
    'columns' => [
        'id',
        'price',
        'text',
        [
            'label' => Yii::t('app', 'Image'),
            'format' => 'html',
            'value' => function(ProductSet $model) {
                $image = $model->getImage(ProductSet::IMAGE);
                if($image)
                    return Html::img('/'.$image, ['class' => 'view-image img-thumbnail']);
                else
                    return null;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function($action, $model, $key, $index) {
                return Url::toRoute(['product-set/'.$action, 'id' => $model->id]);
            }
        ],
    ]
]); ?>