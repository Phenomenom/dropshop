<?php

use backend\models\Product;
use backend\models\ProductType;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $productType ProductType */

$this->title = $productType ? $productType->name : Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create') . ' ' . ($productType ? $productType->name : ''),
            ['create', 'product_type_id' => ($productType ? $productType->id: '')],
            ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'priority',
            [
                'label' => Yii::t('app', 'Images'),
                'format' => 'html',
                'value' => function (Product $model) {
                    $result = '';
                    $result .= 'На главной: ' . ($model->getImages(Product::IMAGE_MAJOR) ? 'есть' : '<span style="color: red">нет</span>');
                    $result .= '<br>';
                    $result .= 'Баннер: ' . ($model->getImage(Product::IMAGE_BANNER) ? 'есть' : '<span style="color: red">нет</span>');
                    $result .= '<br>';
                    $result .= 'Другие: ' . ($model->getImage(Product::IMAGE_MINOR) ? 'есть' : '<span style="color: red">нет</span>');
                    return $result;
                }
            ],
            'price',
            [
                'attribute' => 'states',
                'value' => function (Product $model) {
                    return implode(', ', array_map(function ($i) {
                        return Product::getStateLabels()[$i];
                    }, $model->stateArray));
                },
            ],
            'add_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
