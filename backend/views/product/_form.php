<?php

use backend\models\Accessory;
use backend\models\Brand;
use backend\models\Product;
use backend\models\ProductState;
use backend\models\ProductType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $productChars backend\models\ProductChar[] */
/* @var $productPrices backend\models\ProductPrice[] */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'options' => ['enctype' => 'multipart/form-data'],
        'fieldConfig' => [
//            'template' => '<div{label}</div><div>{input}</div><div>{error}</div>',
//            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ],
    ]); ?>

    <div class="row">

        <div class="col-md-12">
            <?= $form->field($model, 'hash')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Название товара']) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'brand_id')->dropDownList(['' => null] + ArrayHelper::map(Brand::find()->all(),'id','name')) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'product_type_id')->dropDownList(ArrayHelper::map(ProductType::find()->all(),'id','name')) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'descr')->textarea(['placeholder' => 'Описание товара, отображается при просмотре товара', 'style' => 'resize: vertical']) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'video_url')->textInput(['maxlength' => true, 'placeholder' => 'example: https://www.youtube.com/embed/h69QQZVj0jQ']) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'priority')->textInput(['maxlength' => true, 'placeholder' => 'Отвечает за порядок отображения товаров в главном сайте (чем ниже значение, тем выше товар по списку)']) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'stateArray')->widget(Select2::classname(), [
                'data' => Product::getStateLabels(),
                'language' => 'ru',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ]); ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'product_mate_ids')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Product::find()->all(), 'id', 'name'),
                'language' => 'ru',
                'options' => ['placeholder' => 'Select product mates ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ]); ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'accessory_ids')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Accessory::find()->all(), 'id', 'name'),
                'language' => 'ru',
                'options' => ['placeholder' => 'Select accessories ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'placeholder' => 'Цена']) ?>
        </div>

    </div>

    <div class="text-center">
        <h2><a href="#demo" class="center-text" data-toggle="collapse">Характеристики</a></h2>
    </div>
    <div id="demo" class="collapse">
        <?=$this->render("dynamic/_chars", [
            'form' => $form,
            'productChars' => $productChars,
            'model' => $model,
        ])?>
    </div>

    <div class="form-group">
        <br>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
