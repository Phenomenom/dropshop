<?php

use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $productPrices backend\models\ProductPrice[] */

?>

<div>
    <h2 style="text-align: center">Цены и модификации</h2>
    <?php
    DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper2', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody'      => '.container-prices', // required: css class selector
        'widgetItem'      => '.dynamic-price', // required: css class
        'limit'           => 50, // the maximum times, an element can be cloned (default 999)
        'min'             => 1, // 0 or 1 (default 1)
        'insertButton'    => '.add-price', // css class
        'deleteButton'    => '.remove-price', // css class
        'model'           => $productPrices[0],
        'formId'          => 'dynamic-form',
        'formFields'      => [
            'char_id',
            'value',
        ],
    ]); ?>
    <table class="container-prices table table-bordered">
        <tr>
            <th><?=Yii::t('app', 'Name')?></th>
            <th><?=Yii::t('app', 'Price')?></th>
            <th></th>
        </tr>
        <?php foreach($productPrices as $index => $productPrice): ?>
            <?=$this->render("_price", [
                'form' => $form,
                'productPrice' => $productPrice,
                'index' => $index
            ])?>
        <?php endforeach; ?>
    </table>

    <button class="add-price btn btn-primary" style="width: 100%;">
        Добавить модификацию/цену
    </button>
    <?php DynamicFormWidget::end(); ?>
    <br><br>
</div>