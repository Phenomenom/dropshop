<?php

use backend\models\Char;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $productChar backend\models\ProductChar */
/* @var $index integer */

?>

<?php
// necessary for update action.
?>

<tr class="dynamic-char">
    <td>
        <?php
        if (! $productChar->isNewRecord) {
            echo Html::activeHiddenInput($productChar, "[{$index}]id");
        }
        ?>
        <?= $form->field($productChar, "[{$index}]char_id", ['template' => "{input}\n{error}"])
            ->dropDownList(ArrayHelper::map(Char::find()->all(),'id','name','category.name'),[ //
                'prompt' => Yii::t('app', 'Select chars')
            ]) ?>
    </td>
    <td>
        <?= $form->field($productChar, "[{$index}]value", ['template' => "{input}\n{error}"])->textarea(['style' => 'resize: vertical;']) ?>
    </td>

    <td><a href="javascript:void(0);" class="remove-char"><span class="glyphicon glyphicon-trash"></span></a></td>
</tr>