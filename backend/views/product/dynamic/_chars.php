<?php

use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $productChars backend\models\ProductChar[] */

?>

<div>
    <?php
    DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody'      => '.container-chars', // required: css class selector
        'widgetItem'      => '.dynamic-char', // required: css class
        'limit'           => 50, // the maximum times, an element can be cloned (default 999)
        'min'             => 1, // 0 or 1 (default 1)
        'insertButton'    => '.add-char', // css class
        'deleteButton'    => '.remove-char', // css class
        'model'           => $productChars[0],
        'formId'          => 'dynamic-form',
        'formFields'      => [
            'char_id',
            'value',
        ],
    ]); ?>
    <table class="container-chars table table-bordered">
        <tr>
            <th><?=Yii::t('app', 'Characteristic')?></th>
            <th><?=Yii::t('app', 'Value')?></th>
            <th></th>
        </tr>
        <?php foreach($productChars as $index => $productChar): ?>
            <?=$this->render("_char", [
                'form' => $form,
                'productChar' => $productChar,
                'index' => $index
            ])?>
        <?php endforeach; ?>
    </table>

    <button class="add-char btn btn-primary" style="width: 100%;">
        Добавить характеристику
    </button>
    <?php DynamicFormWidget::end(); ?>
    <br><br>
</div>