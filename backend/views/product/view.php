<?php

use backend\models\Product;
use backend\models\ProductChar;
use backend\models\ProductPrice;
use backend\models\ProductSet;
use yii\bootstrap\Collapse;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $productSets yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => [
            'class' => 'scrollable',
            'style' => 'border-spacing: 10px; border-collapse: separate;',
        ],
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'brand.name',
                'label' => Yii::t('app', 'Brand'),
            ],
            'priority',
            [
                'attribute' => 'productType.name',
                'label' => Yii::t('app', 'Product Type'),
            ],
            'descr',
            'video_url',
            [
                'label' => Yii::t('app', 'Product Mates'),
                'value' => implode(', ', ArrayHelper::getColumn($model->productMates, 'name'))
            ],
            [
                'label' => Yii::t('app', 'Accessories'),
                'value' => implode(', ', ArrayHelper::getColumn($model->accessories, 'name'))
            ],
            'price',
            [
                'attribute' => 'states',
                'value' => implode(', ', array_map(function($i) {
                    return Product::getStateLabels()[$i];
                },$model->stateArray))
            ],
        ],
    ]) ?>

    <h3>Дополнительно</h3>
    <?= Collapse::widget([
        'items' => [
            [
                'label' => 'Характеристики',
                'content' => $this->render("view/_chars", [
                    'model' => $model,
                ]),
                'contentOptions' => ['class' => 'in']
            ],
            [
                'label' => 'Разновидность продукта',
                'content' => $this->render("view/_product-sets", [
                    'model' => $model,
                    'productSets' => $productSets,
                ]),
            ],
            [
                'label' => 'Изображения',
                'content' => $this->render("view/_uploads", [
                    'model' => $model,
                ]),
            ],
        ]
    ]); ?>


</div>
