<?php

use backend\models\Product;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $exchange array */
/* @var $message string */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if(isset($message)):
        ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Сообщение:</strong> <?= $message ?>.
        </div>
    <?php
    endif
    ?>

    <?php $form = ActiveForm::begin([]); ?>

    <table class="table table-bordered">
        <tr>
            <td>Курс $:</td>
            <td><?=Html::textInput('exchange_value',$exchange['value']);?> тг.</td>
        </tr>
    </table>

    <div class="form-group">
        <br>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
