<?php

use backend\models\Accessory;
use backend\models\Product;
use backend\models\Request;
use common\helpers\shoppingcart\ShoppingCart;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Request */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if($model->status == Request::STATUS_NEW) {
    $model->status = Request::STATUS_VIEWED;
    $model->save();
}

$product = '';
if($model->product_id) {
    $url = '';
    $item = null;
    switch($model->type) {
        case ShoppingCart::TYPE_PRODUCT:
            $url = Url::to(['product/view', 'id' => $model->product_id]);
            $item = Product::findOne($model->product_id);
            break;
        case ShoppingCart::TYPE_ACCESSORY:
            $url = Url::to(['accessory/view', 'id' => $model->product_id]);
            $item = Accessory::findOne($model->product_id);
            break;
        default:
            break;
    }
    /* @var $item Product */
    if($item !== null) {
        $product = Html::a($item->name, $url);
    }
}

$productSet = '';
if($model->productSet !== null) {
    $productSet = Html::a($model->productSet->text,['product-set/view', 'id' => $model->productSet->id]);
}

?>
<div class="request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'phone',
            'city',
            [
                'attribute' => 'type',
                'value' => ShoppingCart::getTypeLabels()[$model->type],
            ],
            [
                'attribute' => 'product_id',
                'label' => Yii::t('app', 'Product'),
                'format' => 'html',
                'value' => $product,
            ],
            [
                'attribute' => 'product_set_id',
                'label' => Yii::t('app', 'Product Set'),
                'format' => 'html',
                'value' => $productSet,
            ],
            [
                'attribute' => 'body',
                'format' => 'html',
            ],
            [
                'attribute' => 'status',
                'value' => Request::getStatusLabels()[$model->status],
            ],
            'created_at',
        ],
    ]) ?>

</div>
