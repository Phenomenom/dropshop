<?php

use backend\models\Accessory;
use backend\models\Product;
use backend\models\Request;
use common\helpers\shoppingcart\ShoppingCart;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Request'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function(Request $model) {

            $color = 'background-color: none';
            if($model->status == Request::STATUS_NEW) $color = 'background-color: #99ff99';

            return [
                'style' => $color,
            ];

        },
        'columns' => [
            'id',
            'name',
            'phone',
            'city',
            [
                'attribute' => 'type',
                'value' => function(Request $model) {
                    return ShoppingCart::getTypeLabels()[$model->type];
                }
            ],
            [
                'attribute' => 'product_id',
                'format' => 'html',
                'value' => function(Request $model) {
                    if($model->product_id) {

                        $url = '';
                        $item = null;
                        switch($model->type) {
                            case ShoppingCart::TYPE_PRODUCT:
                                $url = Url::to(['product/view', 'id' => $model->product_id]);
                                $item = Product::findOne($model->product_id);
                                break;
                            case ShoppingCart::TYPE_ACCESSORY:
                                $url = Url::to(['accessory/view', 'id' => $model->product_id]);
                                $item = Accessory::findOne($model->product_id);
                                break;
                            default:
                                break;
                        }

                        /* @var $item Product */
                        if($item !== null) {
                            return Html::a($item->name, $url);
                        }
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'body',
                'format' => 'html',
            ],
            [
                'attribute' => 'product_set_id',
                'format' => 'html',
                'value' => function(Request $model) {
                    if(($productSet = $model->productSet) !== null) {
                        return Html::a($productSet->text,['product-set/view', 'id' => $productSet->id]);
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'status',
                'value' => function(Request $model) {
                    return Request::getStatusLabels()[$model->status];
                },
            ],
            'created_at',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
