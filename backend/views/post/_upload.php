<?php

/* @var $this yii\web\View */
use backend\models\Post;

/* @var $model backend\models\Post */

?>

<style>
    .fileinput-remove, .fileinput-upload {
        display: none;
    }

    .file-drop-zone {
        height: auto;
    }
</style>

<br>
<div class="row text-center">
    <h3 style="color: red">Для загрузки изображений на сервер, нажмите на кнопку "Upload file" под каждой картинкой.</h3>
</div>

<div class="row">
    <div class="col-md-6">
        <h2>Основное изображение</h2>
        <?= $model->getImageUploader(Post::IMAGE_FULL) ?>
    </div>
    <div class="col-md-6">
        <h2>Миниатюра</h2>
        <?= $model->getImageUploader(Post::IMAGE_SMALL) ?>
    </div>
</div>