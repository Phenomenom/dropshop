<?php

use backend\models\Post;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    td.breaker {
        overflow-wrap: break-word;
        height: 120px;
        white-space: pre-wrap;
    }
</style>

<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'title',
                'contentOptions' => ['class' => 'breaker'],
            ],
            [
                'attribute' => 'short_text',
                'contentOptions' => ['class' => 'breaker'],
            ],
            'views',
            [
                'attribute' => 'status',
                'value' => function(Post $model) {
                    return Post::getStatusLabels()[$model->status];
                }
            ],
            'add_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
