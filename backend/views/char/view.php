<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Char */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="char-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'is_required',
                'value' => ($model->is_required?'Да':'Нет')
            ],
            [
                'attribute' => 'category.name',
                'label' => Yii::t('app', 'Category'),
            ],
            [
                'attribute' => 'productType.name',
                'label' => Yii::t('app', 'Product Type'),
            ],
        ],
    ]) ?>

</div>
