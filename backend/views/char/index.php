<?php

use backend\models\Char;
use backend\models\ProductType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\search\CharSearch */

$this->title = Yii::t('app', 'Chars');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="char-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Char'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'is_required',
                'value' => function (Char $model) {
                    return $model->is_required ? 'Да' : 'Нет';
                },
//                'filter' => Html::activeDropDownList($searchModel, 'is_required', [true => 'Да', false => 'Нет'], ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'attribute' => 'productType.name',
                'label' => Yii::t('app', 'Product Type'),
            ],
//            [
//                'attribute' => 'product_type_id',
//                'value' => function (Char $model) {
//                    return $model->productType->name;
//                },
////                'filter' => Html::activeDropDownList($searchModel, 'is_required', ArrayHelper::map(ProductType::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => '']),
//            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
