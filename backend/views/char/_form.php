<?php

use backend\models\Category;
use backend\models\ProductType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Char */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="char-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_required')->checkbox() ?>

    <?= $form->field($model, 'product_type_id')->dropDownList(ArrayHelper::map(ProductType::find()->all(), 'id', 'name')); ?>

    <?= $form->field($model, 'category_id')->widget(Select2::className(), [
        'id' => 'js-form-category',
        'data' => ArrayHelper::map(Category::find()->all(),'id','name'),
        'options' => ['multiple' => false, 'placeholder' => Yii::t('app','-- Select Category --')],
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'showToggleAll' => false,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
