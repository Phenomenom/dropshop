<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Char */

$this->title = Yii::t('app', 'Create Char');
$this->params['breadcrumbs'][] = ['label' => 'Chars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="char-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
