<?php

namespace backend\controllers;

use backend\components\behaviors\ImageBehavior;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class ImageController extends Controller
{
    const IMAGE_PATH = 'img/';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['file-upload', 'file-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ],
            ],
        ];
    }

    public function actionFileUpload($id, $type, $classname)
    {
        if (Yii::$app->request->isPost) {

            /* @var $classname ActiveRecord */
            $model = $classname::findOne(['id' => $id]);
            /* @var $model ImageBehavior */
            $model->createDirectory();

            $files = UploadedFile::getInstancesByName('images');
            foreach ($files as $file) {
                /* @var $file UploadedFile */
                $frontend = Yii::getAlias('@frontend') . '/web/';
                $imagesPath = self::IMAGE_PATH . $model->model_path . '/';
                $index = uniqid();

                $webPath = $imagesPath . $id . '/';
                $filename = $type . '_' . $index . '.' . $file->extension;

                if (!$file->saveAs($frontend . $webPath . $filename))
                    throw new Exception('Could not save file');

                $url = $webPath . $filename;

                $result = [
                    'initialPreview' => $model->getPreview($url),
                    'initialPreviewConfig' => [
                        $model->getPreviewConfig($classname, $id, $filename)
                    ],
                    'append' => true,
                ];
                echo json_encode($result);
                return;
            }
        }
    }

    public function actionFileDelete($id, $filename, $classname)
    {
        $frontend = Yii::getAlias('@frontend') . '/web/';

        /* @var $classname ActiveRecord */
        $model = $classname::findOne(['id' => $id]);
        /* @var $model ImageBehavior */
        $imagesPath = self::IMAGE_PATH . $model->model_path . '/';

        if (unlink($frontend . $imagesPath . $id . '/' . $filename)) {
            echo json_encode(['status' => 'remove file ' . $filename]);
        } else {
            echo json_encode(['status' => 'error']);
        }
        return;
    }


}
