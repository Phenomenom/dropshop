<?php

namespace backend\controllers;

use backend\components\Model;
use backend\models\ProductChar;
use backend\models\ProductSet;
use backend\models\ProductType;
use Yii;
use backend\models\Char;
use backend\models\Product;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @param int|null $product_type_id
     * @return mixed
     */
    public function actionIndex($product_type_id = null)
    {
        if($product_type_id) {
            $query = Product::find()->where(['product_type_id' => $product_type_id]);
            $productType = ProductType::findOne(['id' => $product_type_id]);
        } else {
            $query = Product::find()->where(['product_type_id' => null]);
            $productType = null;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'productType' => $productType,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $productSets = new ActiveDataProvider([
            'query' => ProductSet::find()->where(['product_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $model,
            'productSets' => $productSets,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $product_type_id
     * @throws \yii\db\Exception
     * @return mixed
     */
    public function actionCreate($product_type_id = null)
    {
        $product = new Product();
        $product->hash = uniqid();

        if ($product->load(Yii::$app->request->post())) {

            $productChars = Model::createMultiple(ProductChar::classname());
            Model::loadMultiple($productChars, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($productChars),
                    ActiveForm::validate($product)
                );
            }

            // validate all models
            $valid = $product->validate();
            $valid = Model::validateMultiple($productChars) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $product->save(false)) {
                        foreach ($productChars as $modelChar) {
                            /* @var $modelChar ProductChar */
                            $modelChar->product_id = $product->id;
                            if (! ($flag = $modelChar->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {

                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $product->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
            echo 'Could not be saved';
            die();
        }

        if(empty($productChars)) {
            $array = Char::find()->required()->productType($product_type_id)->all();
            $productChars = [];
            foreach($array as $char) {
                /* @var $char Char */
                $productChar = new ProductChar();
                $productChar->char_id = $char->id;
//                var_dump($productChar->errors);
                $productChars[] = $productChar;
            }
            if(sizeof($productChars) == 0) $productChars = [new ProductChar()];
        }

        if($product_type_id)
            $product->product_type_id = $product_type_id;

        return $this->render('create', [
            'model' => $product,
            'productChars' => (empty($productChars)) ? [new ProductChar()] : $productChars,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $product = $this->findModel($id);
        $productChars = $product->productChars;

        if ($product->load(Yii::$app->request->post())) {

            // Удалить старые характеристики, редактировать, и добавить новые
            $oldIDs = ArrayHelper::map($productChars, 'id', 'id');
            $productChars = Model::createMultiple(ProductChar::classname(), $productChars);
            Model::loadMultiple($productChars, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($productChars, 'id', 'id')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($productChars),
                    ActiveForm::validate($product)
                );
            }

            // validate all models
            $valid = $product->validate();
            $valid = Model::validateMultiple($productChars) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    /* Send Email for subscribers */
                    $sendNotify = false;
                    $oldPrice = intval($product->oldAttributes['price']);
                    $newPrice = intval($product->attributes['price']);

                    if($newPrice < $oldPrice) $sendNotify = true;

                    if ($flag = $product->save(false)) {
                        if (! empty($deletedIDs)) {
                            ProductChar::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($productChars as $modelChar) {
                            /* @var $modelChar ProductChar */
                            $modelChar->product_id = $product->id;
                            if (! ($flag = $modelChar->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();

                        if($sendNotify) {
                            // TODO SEND EMAIL HERE
                            $product->sendPriceMail();
                        }

                        return $this->redirect(['view', 'id' => $product->id]);
                    }
                } catch (Exception $e) {
                    var_dump($e->getMessage());
                    die();
                }
                $transaction->rollBack();
            }
        }

        return $this->render('update', [
            'model' => $product,
            'productChars' => (empty($productChars)) ? [new ProductChar()] : $productChars,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
