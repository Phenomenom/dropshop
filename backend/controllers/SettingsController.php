<?php

namespace backend\controllers;

use Yii;
use backend\models\Char;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CharController implements the CRUD actions for Char model.
 */
class SettingsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Char models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if(Yii::$app->request->isPost) {
            $value = $request->post('exchange_value');
            $rows = \Yii::$app->db->createCommand("UPDATE settings SET value=:value WHERE alias=:alias")
                ->bindValues([
                    ':alias' => 'exchange',
                    ':value' => $value,
                ])
                ->execute();
            if($rows) $message = 'Успешно сохранено';
        }

        $query = new Query();
        $query->select('alias,value')
            ->from('settings')
            ->where(['alias' => 'exchange']);
        $exchange = $query->one();

        return $this->render('index', [
            'exchange' => $exchange,
            'message' => $message,
        ]);
    }
}
