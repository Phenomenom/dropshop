<?php

namespace backend\controllers;

use backend\models\AccessoryType;
use Yii;
use backend\models\Accessory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccessoryController implements the CRUD actions for Accessory model.
 */
class AccessoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Accessory models.
     * @param int|null $accessory_type_id
     * @return mixed
     */
    public function actionIndex($accessory_type_id = null)
    {
        if($accessory_type_id) {
            $query = Accessory::find()->where(['accessory_type_id' => $accessory_type_id]);
            $accessoryType = AccessoryType::findOne(['id' => $accessory_type_id]);
        } else {
            $query = Accessory::find()->where(['accessory_type_id' => null]);
            $accessoryType = null;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'accessoryType' => $accessoryType,
        ]);
    }

    /**
     * Displays a single Accessory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Accessory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $accessory_type_id
     * @return mixed
     */
    public function actionCreate($accessory_type_id = null)
    {
        $model = new Accessory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            if($accessory_type_id)
                $model->accessory_type_id = $accessory_type_id;

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Accessory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Accessory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Accessory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Accessory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Accessory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
