<?php
use backend\models\AccessoryType;
use backend\models\ProductType;
use backend\models\Request;
use yii\helpers\ArrayHelper;

$new_requests = Request::find()->where(['status' => Request::STATUS_NEW])->count();

?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Administrator</a>
            </div>
        </div>

        <?php
        $productTypes = [
            [
                'label' => Yii::t('app', 'Undefined'),
                'url' => ["/product", 'product_type_id' => false]
            ]
        ];
        $types = ArrayHelper::map(ProductType::find()->all(), 'id', 'name');
        foreach ($types as $id => $name) {
            $productTypes[] = [
                'label' => $name,
                'url' => ["/product", 'product_type_id' => $id]
            ];
        }
        ?>

        <?php
        $accessoryTypes = [
            [
                'label' => Yii::t('app', 'Undefined'),
                'url' => ["/accessory", 'accessory_type_id' => false]
            ]
        ];
        $types = ArrayHelper::map(AccessoryType::find()->all(), 'id', 'name');
        foreach ($types as $id => $name) {
            $accessoryTypes[] = [
                'label' => $name,
                'url' => ["/accessory", 'accessory_type_id' => $id]
            ];
        }
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    /* ************************************** Система ***************************************** */
                    [
                        'label' => 'Система',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('app', 'Users'),
                        'icon' => 'fa fa-users',
                        'url' => ['/user'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    /* ************************************** Гаджеты ***************************************** */
                    [
                        'label' => 'Гаджеты',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('app', 'Brand'),
                        'icon' => 'fa fa-apple',
                        'url' => ['/brand'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Chars'),
                        'icon' => 'fa fa-plus-square-o',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'Char List'), 'icon' => 'fa fa-list', 'url' => ['/char'],],
                            ['label' => Yii::t('app', 'Char Categories'), 'icon' => 'fa fa-sitemap', 'url' => ['/category'],],
                        ],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Product Types'),
                        'icon' => 'fa fa-tags',
                        'url' => ['/product-type'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Products'),
                        'icon' => 'fa fa-tablet',
                        'url' => ['/product'],
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => $productTypes,
                    ],
                    /* ************************************** Аксессуары ***************************************** */
                    [
                        'label' => 'Аксессуары',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('app', 'Accessory Types'),
                        'icon' => 'fa fa-tags',
                        'url' => ['/accessory-type'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Accessories'),
                        'icon' => 'fa fa-usb',
                        'url' => ['/accessory'],
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => $accessoryTypes,
                    ],
                    /* ************************************** Сайт ***************************************** */
                    [
                        'label' => 'Сайт',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('app', 'Posts'),
                        'format' => 'html',
                        'icon' => 'fa fa-newspaper-o',
                        'url' => ['/post'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Slider'),
                        'icon' => 'fa fa-map-o',
                        'url' => ['/slider'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    /* ************************************** Клиенты ***************************************** */
                    [
                        'label' => 'Клиенты',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('app', 'Requests'),
                        'number' => $new_requests,
                        'format' => 'html',
                        'icon' => 'fa fa-copy',
                        'url' => ['/request'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Broadcast'),
                        'icon' => 'fa fa-envelope-o',
                        'url' => ['/price-mail'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    /* ************************************** Другое ***************************************** */
                    [
                        'label' => 'Другое',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('app', 'Settings'),
                        'icon' => 'fa fa-cogs',
                        'url' => ['/settings'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Login'),
                        'url' => ['site/login'],
                        'visible' => Yii::$app->user->isGuest
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
