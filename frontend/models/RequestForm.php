<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class RequestForm extends Model
{
    public $name;
    public $phone;
    public $city;

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Person Name'),
            'phone' => Yii::t('app', 'Phone'),
            'city' => Yii::t('app', 'City Request'),
        ];
    }

    public function rules()
    {
        return [
            [['name', 'phone', 'city'], 'required'],
        ];
    }
}
