<?php
namespace frontend\controllers;

use backend\models\PriceMail;
use backend\models\Product;
use backend\models\search\ProductSearch;
use backend\models\Setting;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


class GadgetController extends Controller
{
    public $layout = 'design';

    public function actionIndex() {

        $searchModel = new ProductSearch();
        $searchModel->isPhone = false;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
            'pageSize' => 0,
        ];

//        $dataProvider->sort->attributes['new'] = [
//            'desc' => ['add_date' => SORT_DESC],
//        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currency' => $searchModel->currency,
            'rate' => Setting::getExchange(),
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        $priceMail = new PriceMail();
        $priceMail->product_id = $id;

        return $this->render('/phone/view', [
            'model' => $model,
            'priceMail' => $priceMail,
        ]);
    }

    public function actionAccessories() {
        return $this->getProducts(Product::TYPE_ACCESSORY);
    }

    public function actionParts() {
        return $this->getProducts(Product::TYPE_PART);
    }

    public function actionGadgets() {
        return $this->getProducts(Product::TYPE_GADGET);
    }

    public function actionSearch($key) {
        return $this->getProducts(null, $key);
    }

    private function getProducts($type = null, $key = null) {
        $searchModel = new ProductSearch();
        if($type)
            $searchModel->product_type_id = $type;
        if($key)
            $searchModel->product_type_id = $key;
        $searchModel->active = true;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
            'defaultPageSize' => 15,
        ];

//        $dataProvider->sort->attributes['new'] = [
//            'desc' => ['add_date' => SORT_DESC],
//        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currency' => $searchModel->currency,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
