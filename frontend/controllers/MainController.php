<?php
namespace frontend\controllers;

use backend\models\Product;
use backend\models\ProductPrice;
use backend\models\Request;
use backend\models\search\ProductSearch;
use backend\models\Setting;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\data\Sort;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class MainController extends Controller
{
    public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
//        $this->layout = 'empty';
        $products = Product::find()->orderBy('priority')->addOrderBy('name')->all();
        return $this->render('index',['products' => $products]);
    }

    public function actionDostavka()
    {
        $this->layout = 'empty';
        return $this->render('dostavka');
    }

    public function actionThank($name, $id) {
        $this->layout = 'empty';
        return $this->render('thank', [
            'name' => $name,
            'id' => $id,
        ]);
    }

    public function actionFaq()
    {
        $this->layout = 'empty';
        return $this->render('faq');
    }

    public function actionProduct($id) {
        $this->layout = 'empty';
        return $this->render('product', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionRequest() {
        if(Yii::$app->request->isAjax) {
            $values = [];
            parse_str(Yii::$app->request->post('form'),$values);

            $request = new Request();
            $request->name = $values['client_name'];
            $request->phone = $values['client_phone'];
            $request->city = $values['client_address'];

            $product_id = intval($values['phone_model']);
            $product = Product::findOne($product_id);
            /* @var $product Product */

            if($product) {
                $request->product_id = $product_id;

                $body = "";
                $body.= "<b>{$product->name}</b>";
                $body.= "<br>";
                $body.= "Курс: ".Setting::getExchange().' тг/$<br>';

                foreach($product->getPriceArray() as $productPrice) {
                    /* @var $productPrice ProductPrice*/
                    $body .= $productPrice->name.' - '.$productPrice->price.' ('.Setting::roundPrice($productPrice->price, 500).' тг.)<br>';
                }

                $request->body = $body;

                $result = $this->sendMail($body,$request->name,$request->phone,$request->city);
                $this->sendNotifyCopy($request->name);
                $result = true;

                if($result) {
                    if ($request->save()) {
                        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        echo json_encode([
                            'status' => 'success',
                            'id' => $request->id,
                            'name' => $request->name,
                        ]);
                        return;
                    }
                }
            }
        }
        echo json_encode(['status' => 'error']);
    }

    private function sendNotifyCopy($name) {
        $to_copy = Yii::$app->params['mail_to_copy'];
        return Yii::$app->mailer->compose()
            ->setFrom('info@dropshop.kz')
            ->setTo($to_copy)
            ->setSubject('Уведомление DropShop.kz - '.$name)
            ->setHtmlBody('Пришел заказ на DropShop')
            ->send();
    }

    private function sendMail($body,$name,$phone,$address) {

        $messageBody = $body.'<br><br>';
        $messageBody.= "Имя: <b>{$name}</b><br>";
        $messageBody.= "Телефон: <b>{$phone}</b><br>";
        $messageBody.= "Адрес: <b>{$address}</b><br>";

        $to = Yii::$app->params['mail_to'];

        $flag = Yii::$app->mailer->compose()
            ->setFrom('info@dropshop.kz')
            ->setTo($to)
            ->setSubject('Заказ с DropShop.kz - '.$name)
            ->setHtmlBody(
                $messageBody
            )
            ->send();

        return $flag;
    }

    public function actionAccessories() {
        $this->layout = 'empty';
        $products = Product::find()->where(['type' => Product::TYPE_ACCESSORY])->orderBy('priority')->addOrderBy('name')->all();
        return $this->render('accessories', [
            'products' => $products,
//            'path' => $path,
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
