<?php
namespace frontend\controllers;

use backend\models\Accessory;
use backend\models\search\AccessorySearch;
use backend\models\Setting;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class AccessoryController extends Controller
{
    public $layout = 'design';

    public function actionIndex() {

        $searchModel = new AccessorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
            'defaultPageSize' => 15,
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currency' => $searchModel->currency,
            'rate' => Setting::getExchange(),
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        $priceMail = new PriceMail();
        $priceMail->product_id = $id;

        return $this->render('view', [
            'model' => $model,
            'priceMail' => $priceMail,
        ]);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Accessory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}


