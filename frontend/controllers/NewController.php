<?php
namespace frontend\controllers;

use backend\models\Post;
use backend\models\Product;
use backend\models\ProductPrice;
use backend\models\Request;
use backend\models\search\ProductSearch;
use backend\models\Setting;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class NewController extends Controller
{
    public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $this->layout = 'design';

        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
            'defaultPageSize' => 15,
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNews() {
        $this->layout = 'design';

        $dataProvider = new ActiveDataProvider([
            'query' => Post::find(),
        ]);

        return $this->render('news', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
