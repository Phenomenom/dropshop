<?php
namespace frontend\controllers;

use backend\models\PriceMail;
use backend\models\Product;
use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class PriceMailController extends Controller
{
    public function actionSign()
    {
        $priceMail = new PriceMail();
        $priceMail->load(Yii::$app->request->post());

        $this->findModel($priceMail->product_id);

        $priceMail->hash = Yii::$app->security->generateRandomString();

        if($priceMail->save()) {
            echo '1';
        } else {
            echo 'Вы уже подписывались на данный продукт';
        }
    }

    public function actionUnsign($email, $hash)
    {
        $priceMail = PriceMail::find()->where(['email' => $email, 'hash' => $hash])->one();

        if($priceMail !== null) {
            $priceMail->delete();
            echo 'Successfully deleted';
        } else {
            throw new InvalidParamException('Record not found');
        }
    }

    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
