<?php
namespace frontend\controllers;

use backend\models\Accessory;
use backend\models\Post;
use backend\models\Product;
use backend\models\Request;
use backend\models\search\ProductSearch;
use backend\models\Setting;
use common\helpers\shoppingcart\ShoppingCart;
use frontend\models\RequestForm;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class CartController extends Controller
{
    public $layout = 'design';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add-product' => ['post'],
                    'remove-product' => ['post'],
                    'order' => ['post'],
                ],
            ],
        ];
    }

    public function runAction($id, $params = [])
    {
        // Extract the params from the request and bind them to params
        $params = \yii\helpers\BaseArrayHelper::merge(Yii::$app->getRequest()->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }

    public function actionIndex() {
        /* @var $cart ShoppingCart */
        $cart = Yii::$app->cart;

        $rate = Setting::getExchange();

        return $this->render('index', [
            'requestForm' => new RequestForm(),
            'products' => $cart->getProducts(),
            'accessories' => $cart->getAccessories(),
            'cost' => $cart->getCost(),
            'count' => $cart->getCount(),
            'rate' => $rate,
        ]);
    }

    public function actionAddProduct($type, $id, $product_set = null, $quantity = 1)
    {
        if($product_set == -1) $product_set = null;

        /* @var $cart ShoppingCart */
        $cart = Yii::$app->cart;
        $cart->put($type, $id, $product_set, $quantity);
        $count = $cart->getCount();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['count' => $count];
        } else {
            return $this->redirect(['cart/index']);
        }
    }

    public function actionRemoveProduct($type, $id, $product_set = null)
    {
        if($product_set == -1) $product_set = null;

        /* @var $cart ShoppingCart */
        $cart = Yii::$app->cart;
        $cart->remove($type, $id, $product_set);
        $count = $cart->getCount();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['count' => $count];
        } else {
            return $this->redirect(['cart/index']);
        }
    }

    public function actionOrder()
    {
        $form = new RequestForm();
        $form->load(Yii::$app->request->post());

        if($form->validate()) {
            /* @var $cart ShoppingCart */
            $cart       = Yii::$app->cart;

            if ($cart->getCount() > 0) {

                $products = $cart->getProducts();
                $accessories = $cart->getAccessories();

                $transaction = Yii::$app->db->beginTransaction();
                try {

                    /* @var $products Product[] */
                    foreach($products as $product) {

                        $request = new Request();
                        $request->product_id = $product->id;
                        $request->type = ShoppingCart::TYPE_PRODUCT;
                        $request->name = $form->name;
                        $request->phone = $form->phone;
                        $request->city = $form->city;
                        $request->product_set_id = $product->product_set_id;

                        $request->body = $request->generateBody();

                        if(!$request->save(false))
                            throw new Exception('Could not save Product');
                    }

                    /* @var $accessories Accessory[] */
                    foreach($accessories as $accessory) {

                        $request = new Request();
                        $request->product_id = $accessory->id;
                        $request->type = ShoppingCart::TYPE_ACCESSORY;
                        $request->name = $form->name;
                        $request->phone = $form->phone;
                        $request->city = $form->city;

                        $request->body = $request->generateBody();

                        if(!$request->save(false))
                            throw new Exception('Could not save Accessory');
                    }

                    $transaction->commit();
                    $cart->removeAll();

                    $this->redirect(['cart/success']);

                } catch(Exception $e) {
                    $transaction->rollBack();
                    throw new Exception('Произошла ошибка во время создания заказа');
                }

            } else {
                throw new ForbiddenHttpException('Shopping Cart is empty!');
            }
        } else {
            throw new InvalidParamException('Form parameters are not correct');
        }
    }

    public function actionSuccess()
    {
        return $this->render('success');
    }
}
