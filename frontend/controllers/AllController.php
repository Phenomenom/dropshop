<?php
namespace frontend\controllers;

use backend\models\PriceMail;
use backend\models\Product;
use backend\models\search\ProductSearch;
use backend\models\Setting;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


class AllController extends Controller
{
    public $layout = 'design';

    public function actionIndex() {

        $searchModel = new ProductSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = [
            'pageSize' => 0,
        ];

        return $this->render('/phone/index', [
            'title' => 'Продукты',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currency' => $searchModel->currency,
            'rate' => Setting::getExchange(),
        ]);
    }
}
