$('#pjax-container').on('change', '.form-control', function() {
    $('#wow').submit();
    $('.js-listview').addClass('loading');
});

$("#pjax-container").on({
    mouseenter: function () {
        $(this).addClass('content-item__background--zoom');
        $(this).siblings('.content-item__icon').addClass('content-item__icon--zoom');
    },
    mouseleave: function () {
        $(this).removeClass('content-item__background--zoom');
        $(this).siblings('.content-item__icon').removeClass('content-item__icon--zoom');
    }
}, ".content-item__background");