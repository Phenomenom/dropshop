/**
 * Created by Erkebulan on 27.06.2016.
 */

var productSets = JSON.parse($('[name="product-sets"]').val());
var priceProduct = parseInt($('[name="product-price"]').val());
var currency = parseInt($('[name="currency"]').val());

productSets.forEach(function(productSet, i) {
    $('.js-button-' + productSet).click(function() {
        $('.js-image').addClass('product-header__image-item--hidden');
        $('.js-image-' + productSet).removeClass('product-header__image-item--hidden');

        $('.js-button').addClass('button-disabled');
        $('.js-button-' + productSet).removeClass('button-disabled');

        var priceSet = parseInt($(this).attr('data-price'));

        var total = (priceSet + priceProduct) * currency;
        total = Math.ceil(total / 500) * 500;
        $('.js-price').text(total);
        $('[name="product_set"]').val(productSet);
    });
});

if(productSets.length > 0) {
    $('.js-button-' + productSets[0]).trigger( "click" );
}

$('#js-to-cart').removeAttr('disabled');

(function() {

    $('#js-subscribe-button').click(function() {
        $('#js-subscribe-form').yiiActiveForm('submitForm');
    });

    $('#js-subscribe-form').submit(function (event) {

        event.preventDefault();

        $('#js-subscribe').hide();
        $('#js-loading').show();

        var data = {
            PriceMail: {
                email: $('pricemail-email').val(),
                product_id: $('pricemail-product_id').val()
            }
        };

        data = $('#js-subscribe-form').serialize();


        $.post('/price-mail/sign', data, function(result) {
            $('#js-loading').hide();
            if(result == 1) {
                $('#js-success').show();
            } else {
                alert(result);
                $('#js-modal-price').modal('hide')
            }
        });
    });
})();
