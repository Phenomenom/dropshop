$(document).ready(function() {

    function processForm(e) {
        if (e.preventDefault) e.preventDefault();

        var csrfToken = $('input[name="_csrf"]').val();
        $('#Button1').get(0).disabled = true;
        //$('#Button1').css({
        //    backgroundColor: 'gray',
        //    borderColor: 'gray'
        //});
        $('#Button1').fadeOut(500);
        var form = $('#Form1').serialize();

        $.ajax({
            url: 'request',
            type: 'post',
            dataType: 'json',
            data: {form: form, _csrf : csrfToken},
            success: function(data) {
                console.log(data);
                var id = data['id'];
                var name = data['name'];
                window.location.replace("/main/thank?id="+id+"&name="+name);
                $('#Button1').hide();
                $('#Thank').show();
                $('#Order').hide();
            },
            error: function(data) {
                alert('Ошибка сети. Пожалуйста, повторите заказ позже..');
                $('#Button1').get(0).disabled = false;
                $('#Button1').fadeIn(500);
            }
        });

        return false;
    }

    var form = document.getElementById('Form1');
    if (form.attachEvent) {
        form.attachEvent("submit", processForm);
    } else {
        form.addEventListener("submit", processForm);
    }

});