function scrollToTop(callback) {
    if ($('html').scrollTop()) {
        $('html').animate({ scrollTop: 0 }, callback);
        return;
    }

    if ($('body').scrollTop()) {
        $('body').animate({ scrollTop: 0 }, callback);
        return;
    }

    callback();
}