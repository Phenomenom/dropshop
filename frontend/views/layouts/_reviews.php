<?php
use evgeniyrru\yii2slick\Slick;
use yii\web\JsExpression;

$items = [];

$items[] = <<<HTML
    <div class="reviewer">
        <div class="reviewer-image">
            <img src="http://www.oneplus.com.kz/img/ot9.jpg">
        </div>
        <div class="reviewer-text">
            <p style="color:#dc0212">«Доставили быстро..!»</p>
            <br>
            <p>
                Делал заказ дважды. Сначала заказал телефон OnePlus X.
                Затем сломал экран, заказали запчасть сделали по гарантии.
                Все как положено. Цены ниже, чем везде, доставили быстро,
                продавцы толковые. Молодцы, желаю успехов в будущем!
            </p>
            <br>
            <p>
                Александр Ковалев, г. Алматы
            </p>
        </div>
    </div>
HTML;

$items[] = <<<HTML
    <div class="reviewer">
        <div class="reviewer-image">
            <img src="http://www.oneplus.com.kz/img/ot10.jpg">
        </div>
        <div class="reviewer-text">
            <p style="color:#dc0212">«Дешевле не нашла..!»</p>
            <br>
            <p>
                Долго не могла определиться с телефоном. Брат показал мне
                этот сайт, здесь мне помогли выбрать телефон. Я очень рада!
                Сравнила цены, дешевле не нашла! Все советую!
            </p>
            <br>
            <p>
                Ельмира Еламанова,  г. Алматы
            </p>
        </div>
    </div>
HTML;

$items[] = <<<HTML
    <div class="reviewer">
        <div class="reviewer-image">
            <img src="http://www.oneplus.com.kz/img/ot8.jpg">
        </div>
        <div class="reviewer-text">
            <p style="color:#dc0212">«Теперь всегда буду покупать здесь!»</p>
            <br>
            <p>
                Покупал у вас One Plus One, вначале сомневался, цены были
                дешевые. Знакомый сходил к ним в офис в Алматы, продавцы
                внушили доверие, отправил деньги на счет. Понравились
                сроки доставки, телефон прише очень быстро и цены почти,
                как на алиекспресе. Теперь всегда буду покупать здесь.
            </p>
            <br>
            <p>
                Ержан Хибасов, г. Актау
            </p>
        </div>
    </div>
HTML;
?>


<h2 class="text-center">Отзывы наших покупателей</h2>
<br>

<div class="reviewers-background">
    <div class="reviewers">
        <?=Slick::widget([

            // HTML tag for container. Div is default.
            'itemContainer' => 'div',

            // HTML attributes for widget container
            'containerOptions' => ['class' => 'slider2'],

            // Items for carousel. Empty array not allowed, exception will be throw, if empty
            'items' => $items,

            // HTML attribute for every carousel item
            'itemOptions' => ['class' => 'slider__image'],

            // settings for js plugin
            // @see http://kenwheeler.github.io/slick/#settings
            'clientOptions' => [
                'autoplay' => true,
                'dots'     => true,
                // note, that for params passing function you should use JsExpression object
                'onAfterChange' => new JsExpression('function() {}'),
            ],

        ]); ?>
    </div>
</div>