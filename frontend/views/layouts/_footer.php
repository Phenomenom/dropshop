<?php

/* @var $this yii\web\View */

?>

<div class="footer-background container-fluid">
    <div class="footer">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6" style=";font-size:20px;">

                    <span style="font-size: 36px;">DropShop.<span style="color: #00CCFF">kz</span></span>
                    <br>
                    ИП "RAYANA"<br>Юридический и фактический адрес:<br> г.Алматы, ул.Абая, д.44, ТРЦ “Symphony”

                </div>
                <div class="col-md-2 text-center">
                    <img src="/img/barcode.gif" alt="" style="width:100px;height:100px; margin: 10px 0;">
                </div>

                <div class="col-md-4 text-right">
                    <span class="support-call text-strong" style="font-size: 20px;">
                    <span class="icon-phone"></span>
                        &nbsp;Телефон:
                        <br><a class="text-primary" href="tel:+7 (727) 224-2762">+7 (727) 335-6847</a>

                    </span>
                    <br><br>
                    Email : <a class="text-primary" href="mailto:info@oneplus.com.kz">dropshop.kz@gmail.com</a>
                    <br>
                    Skype : <a class="text-primary" href="skype:oneplus.com.kz">dropshop.kz</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <br>
                    <p style="font-size:8px; color:#c0c0c0; line-height: 1.1875;">
                        <span>ПОЛИТИКА КОНФИНДЕНЦИАЛЬНОСТИ<br>Сайт уважает ваше право и соблюдает конфиденциальность при заполнении, передачи и хранении ваших конфиденциальных сведений. Размещение заявки на сайте означает Ваше согласие на обработку данных. Под персональными данными подразумевается информация, относящаяся к субъекту персональных данных, в частности фамилия, имя и отчество, дата рождения, адрес, контактные реквизиты (телефон, адрес электронной почты), семейное, имущественное положение и иные данные.<br><br><br>Целью обработки персональных данных является оказание сайтом услуг. В случае отзыва согласия на обработку своих персональных данных мы обязуемся удалить Ваши персональные данные в срок не позднее 3 рабочих дней. Отзыв согласия можно отправить в электронном виде на наш электронный адресс. e-mail: info@dropshop.kz</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>