<?php

use backend\models\Slider;
use evgeniyrru\yii2slick\Slick;
use yii\bootstrap\Carousel;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $slides Slider[] */
$slides = Slider::find()->all();
$items = [];
foreach($slides as $slider) {
    $image = Html::img('/'.$slider->getImage(Slider::IMAGE), ['class' => 'slider__image']);
    $link = Html::a($image, $slider->link);
    $items[] = $link;
}
?>

<style>
    .slider__image:focus {outline:0 !important;}
</style>

<div class="slider-background">
    <?=Slick::widget([

        // HTML tag for container. Div is default.
        'itemContainer' => 'div',

        // HTML attributes for widget container
        'containerOptions' => [
            'class' => 'slider',
            'id' => 'slider',
        ],

        // Items for carousel. Empty array not allowed, exception will be throw, if empty
        'items' => $items,

        // HTML attribute for every carousel item
        'itemOptions' => ['class' => 'slider__image'],

        // settings for js plugin
        // @see http://kenwheeler.github.io/slick/#settings
        'clientOptions' => [
            'autoplay' => true,
            'dots'     => true,
            // note, that for params passing function you should use JsExpression object
            'onAfterChange' => new JsExpression('function() {}'),
        ],

    ]); ?>
</div>
