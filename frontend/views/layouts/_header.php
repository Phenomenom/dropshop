<?php
use backend\models\Setting;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

if(isset(Yii::$app->request->queryParams['key'])) {
    $key = Yii::$app->request->queryParams['key'];
}

?>

<header>
    <nav>
        <div class="nav-title"></div>
        <div class="nav-top">
<!--            <div class="nav-top__logo"></div>-->
            <?= Html::a("<div class='nav-top__item'>О магазине</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>Оплата</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>Доставка</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>Гарантии</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>Вопросы</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>Отзывы</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>Контакты</div>", ['/']) ?>
            <?= Html::a("<div class='nav-top__item'>(Корзина)</div>", ['cart/index']) ?>
        </div>
        <div class="nav-middle">
            <div class="nav-middle__logo">
                <a href="/"><h2>DropShop.<span class="nav-middle--color">kz</span></h2></a>
            </div>
            <div class="nav-middle__info nav-middle__info--discount">
                <p><i class="fa fa-gift fa-lg"></i>&nbsp;&nbsp;<b>Скидка 1000 тг.</b> на первую покупку</p>
            </div>
            <div class="nav-middle__info nav-middle__info--exchange">
                <p>Курс: <b><?=Setting::getExchange();?> тг/$</b></p>
            </div>

        </div>
        <div class="nav-buttons">
            <?= Html::a("<div class='nav-buttons__item'>Смартфоны</div>", ['phone/index']) ?>
            <?= Html::a("<div class='nav-buttons__item'>Гаджеты</div>", ['gadget/index']) ?>
            <?= Html::a("<div class='nav-buttons__item'>Аксессуары</div>", ['accessory/index']) ?>
<!--            --><?//= Html::a("<div class='nav-buttons__item'>Ремонт&Запчасти</div>", ['product/parts'], ['style' => 'color: gray']) ?>
            <?= Html::a("<div class='nav-buttons__item'>Новости&Обзоры</div>", ['post/index']) ?>
            <!--            <div class="nav-buttons__item">Форум</div>-->
            <!--            <div class="nav-buttons__item">Рассрочка</div>-->
            <!--            <div class="nav-buttons__item">Распродажа</div>-->
<!--            <div class="nav-buttons__item nav-buttons__item--search">-->
<!--                --><?php //$form = ActiveForm::begin([
//                    'action' => ['product/search'],
//                    'method' => 'get',
//                    'options' => ['style' => 'display:flex']
//                ]); ?>
<!--                <input name="key" type="text" value="--><?//= isset($key) ? $key : "" ?><!--">-->
<!--                --><?//= Html::submitButton(Yii::t('app', 'Search')) ?>
<!--                --><?php //ActiveForm::end() ?>
<!--            </div>-->
        </div>
    </nav>
</header>
