<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\WebAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

WebAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=800" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php if(false): ?>
<div class="background">
    <div class="background__inner background__inner--left"></div>
    <div class="background__inner background__inner--right"></div>
</div>
<?php endif; ?>

<a href="<?=\yii\helpers\Url::to(['cart/index'])?>">
    <div class="cart-top">
        <div class="cart-top__block">
            Товаров <?=Yii::$app->cart->getCount()?> (<i><?=Yii::$app->cart->getCost() * \backend\models\Setting::$exchange?> KZT</i>)
            &nbsp;
            <i class="cart-top__icon fa fa-shopping-cart"></i>
        </div>
    </div>
</a>

<div class="scroll-top">
    <a class="fa fa-4x fa-arrow-circle-up scroll-top__button" onclick="scrollToTop()"></a>
</div>

<div class="wrapper">
    <?= $this->render('_header.php') ?>
    <div class="body">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>

        <?= Alert::widget() ?>

    </div>
    <?= $this->render('_footer.php') ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
