<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
\Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->getAbsoluteUrl()]);
\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => 'Смартфоны по цене aliexpress.com. С доставкой в 3 раза быстрее! Получите свой телефон уже через 10 дней! *Работаем напрямую с китайскими заводами-изготовителями, которые дают низкие цены']);
\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => yii\helpers\Url::home(true) . 'img/logo.png']);

?>

<?= $this->render('/layouts/_slider'); ?>

<?= $this->render('/phone/index/_new') ?>

<?= $this->render('/phone/index/_hit') ?>

<div class="reviews-background">
    <div class="reviews-content">
        <h2 class="text-center">Отзывы наших покупателей</h2>
        <div id="spk-widget-reviews" style="display:none; width: 100%;"
             shop-id="<?= Yii::$app->params['review-id'] ?>"
             good-id="000000000000"
             good-title="<?= $model->name ?>"
             good-url="<?= yii\helpers\Url::to('/') ?>">
        </div>
        <script async="async" type="text/javascript"
                src="//static.sprosikupi.ru/js/widget/sprosikupi.bootstrap.js">
        </script>
    </div>
</div>

<div>
    <?= $this->render('/layouts/_map') ?>
</div>

<div>
    <?= $this->render('/layouts/_reviews') ?>
</div>

<div>
    <?= $this->render('/phone/index/_posts') ?>
</div>
