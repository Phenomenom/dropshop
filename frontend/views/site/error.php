<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<!-- Main content -->
<div class="login-box">
    <div class="login-box-body text-center">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>
        <div>
            <h1><?= $name ?></h1>

            <p>
                <?= nl2br(Html::encode($message)) ?>
            </p>

        </div>
    </div>
</div>
