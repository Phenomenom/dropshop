<?php

use backend\models\search\AccessorySearch;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel AccessorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $currency integer */
/* @var $rate integer */

$this->title = Yii::$app->name.' - Аксессуары';
$this->params['breadcrumbs'][] = ['label' => 'Аксессуары'];

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 6500,
    'clientOptions' => ['container' => '#pjax-container']]); ?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'id' => 'js-activeform',
    'options' => ['data-pjax' => true ],
]); ?>

<?php ActiveForm::end(); ?>

    <h1 class="text-center">Аксессуары</h1>

    <div class="text-center">
        <?= $this->render('_search', ['model' => $searchModel, 'dataProvider' => $dataProvider]) ?>
    </div>

    <div>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'content-item'],
            'itemView' => '_item',
            'viewParams' => [
                'currency' => $currency,
                'rate' => $rate,
            ],
            'layout' => '{pager}<div class="content">{items}</div>',
            'options' => [
                'class' => 'js-listview content-background',
            ]
        ]); ?>
    </div>
<?php \yii\widgets\Pjax::end(); ?>

<div>
    <?= $this->render('/layouts/_map') ?>
</div>

<?php
$this->registerJsFile('/js/index.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>