<?php

use backend\models\AccessoryType;
use backend\models\Brand;
use backend\models\search\AccessorySearch;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="col-sm-12">
    <div style="margin: 5px 0 15px 0" class="accessory-search">

        <h3>Поиск</h3>

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => true,
                'id' => 'wow',
                'class' => 'form-inline',
            ],
        ]); ?>

        <?= $form->field($model, 'name')->textInput([
            'placeholder' => 'Название',
        ])->label(false) ?>

        <?= $form->field($model, 'currency')->dropDownList(AccessorySearch::getCurrencyLabels())->label(false) ?>

        <?= $form->field($model, 'accessory_type_id', ['options' => ['class' => 'form-group', 'style' => 'min-width: 300px']])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(AccessoryType::find()->all(), 'id', 'name'),
            'language' => 'ru',
            'options' => [
                'placeholder' => 'Выберите тип аксессуара ...',
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ],
        ])->label(false); ?>

        <br>

        <h4 style="display: inline-block">Показать:</h4>
        <div style="display: inline-block">
            &nbsp;&nbsp;<?= Html::a('сначала дешевые '.$priceAsc,Url::current(['sort' => 'price'])) ?>
            &nbsp;&nbsp;<?= Html::a('сначала дорогие '.$priceDesc,Url::current(['sort' => '-price'])) ?>
        </div>

        <br>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Reset'), Url::toRoute([]), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>