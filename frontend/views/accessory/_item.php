<?php
use backend\models\Accessory;
use backend\models\search\AccessorySearch;
use backend\models\search\ProductSearch;
use common\helpers\shoppingcart\ShoppingCart;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Accessory */
/* @var $currency integer */
/* @var $rate integer */

if(!isset($noProductSet))
    $noProductSet = -1;

switch($currency) {
    case AccessorySearch::CURRENCY_USD:
        $price = $model->price;
        break;
    case AccessorySearch::CURRENCY_TG:
    default:
        $price = $model->getPriceRound();
        break;
}

?>

<?= Html::beginForm(['cart/add-product'], 'POST'); ?>
<div class="content-item__box content-item__box--submitable" onclick="this.parentNode.submit();">
    <div class="content-item__icon">
        <?=\rmrevin\yii\fontawesome\FontAwesome::i('cart-plus fa-3x')?>
    </div>
    <div class="content-item__background" style="background-image: url('/<?= $model->getImage(Accessory::IMAGE) ?>')">
    </div>
    <div class="content-item__text-box">
        <div class="content-item__text">
            <div class="content-item__text-brand"><?= $model->name ?></div>
            <div class="content-item__text-info">
                <?= $currency == ProductSearch::CURRENCY_USD ? "$" : "" ?><?= number_format($price, 0, ',', ' ') ?>
                <?= $currency == ProductSearch::CURRENCY_TG ? " тг" : "" ?>
            </div>
        </div>
    </div>
</div>
<?= Html::hiddenInput('id', $model->id) ?>
<?= Html::hiddenInput('type', ShoppingCart::TYPE_ACCESSORY) ?>
<?= Html::hiddenInput('product_set', $noProductSet) ?>
<?= Html::endForm() ?>