<?php
use backend\models\Product;
use backend\models\search\ProductSearch;

/* @var $this yii\web\View */
/* @var $model Product */
/* @var $currency integer */
/* @var $rate integer */

switch($currency) {
    case ProductSearch::CURRENCY_USD:
        $price = $model->price;
        break;
    case ProductSearch::CURRENCY_TG:
        $price = $model->getPriceRound();//$model->price * $rate;
        break;
    default:
        $price = $model->price;
}

?>

<a href="<?= \yii\helpers\Url::to(['view', 'id' => $model->id]) ?>">
    <div class="content-item__box">
        <div class="content-item__icon">
            <?=\rmrevin\yii\fontawesome\FontAwesome::i('eye fa-3x')?>
        </div>
        <div class="content-item__background" style="background-image: url('/<?= $model->getImage(Product::IMAGE_MAJOR) ?>')"></div>
        <div class="content-item__text-box">
            <div class="content-item__text">
                <div class="content-item__text-brand"><?= $model->name ?></div>
                <div class="content-item__text-info">
                    <?= $currency == ProductSearch::CURRENCY_USD ? "$" : "" ?><?= $price /*number_format($price, 0, ',', ' ')*/ ?>
                    <?= $currency == ProductSearch::CURRENCY_TG ? " тг" : "" ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-item__chars">
        <div class="content-item__char" title="<?=$model->productChars[0]->value?>">
            <?php if($model->productChars[0]): ?>
                <span class="content-item__char-name"><?=$model->productChars[0]->char->name?></span>
                <br>
                <span class="content-item__char-value"><?=$model->productChars[0]->value?></span>
            <?php endif; ?>
        </div>
        <div class="content-item__char" title="<?=$model->productChars[1]->value?>">
            <?php if($model->productChars[1]): ?>
                <span class="content-item__char-name"><?=$model->productChars[1]->char->name?></span>
                <br>
                <span class="content-item__char-value"><?=$model->productChars[1]->value?></span>
            <?php endif; ?>
        </div>
        <div class="content-item__char" title="<?=$model->productChars[2]->value?>">
            <?php if($model->productChars[2]): ?>
                <span class="content-item__char-name"><?=$model->productChars[2]->char->name?></span>
                <br>
                <span class="content-item__char-value"><?=$model->productChars[2]->value?></span>
            <?php endif; ?>
        </div>
        <div class="content-item__char" title="<?=$model->productChars[3]->value?>">
            <?php if($model->productChars[3]): ?>
                <span class="content-item__char-name"><?=$model->productChars[3]->char->name?></span>
                <br>
                <span class="content-item__char-value"><?=$model->productChars[3]->value?></span>
            <?php endif; ?>
        </div>
    </div>
</a>