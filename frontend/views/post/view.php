<?php

use backend\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Post */

$this->title = Yii::$app->name.' - '.$model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Новости',
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $model->title;

\Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->getAbsoluteUrl()]);
\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $model->title]);
\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $model->short_text]);
\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => Url::home(true) . $model->getImage(Post::IMAGE_SMALL)]);

?>
<div class="post">

    <div class="post-header">
        <div class="post-header__title"><?=$model->title?></div>
        <div class="post-header__info">
            <div class="post-header__info-date">Дата: <?=$model->add_date?></div>
            <div class="post-header__info-views">Просмотры: <?=$model->views?></div>
        </div>
    </div>
    <div class="post-body">
        <?=$model->full_text?>
    </div>
    <div class="post-footer">
        <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir"
             data-counter=""
             data-description="<?=$model->short_text?>"
             data-title="<?=$model->title?>"
             data-image="<? echo str_replace('\\','/',Url::base(true).'/'.$model->getImage(Post::IMAGE_SMALL)) ?>"
             data-url="<?=Url::toRoute(['post/view', 'id' => $model->id],true)?>"
            ></div>
    </div>

</div>