<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\search\ProductSearch */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = ['label' => 'Новости'];

?>
<div class="">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'news-item'],
        'itemView' => '_item',
        'layout' => '{pager}<div class="news-block">{items}</div>{summary}'
    ]); ?>
</div>
