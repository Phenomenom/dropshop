<?php
use backend\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Post */
/* @var $path string */

?>

<div class="news-item__image-box">
    <?php if($model->getImage(Post::IMAGE_SMALL)):?>
        <div class="news-item__image" style="background-image: url('/<?= $model->getImage(Post::IMAGE_SMALL) ?>')">
        </div>
<!--        <a class="news-item__image-link" href="--><?//=Url::to(['post/view', 'id' => $model->id])?><!--">-->
<!--            <img class="news-item__image" src="/--><?//=$model->getImage(Post::IMAGE_SMALL)?><!--">-->
<!--        </a>-->
    <?php endif ?>
</div>
<div class="news-descr">
    <div class="news-descr__title">
        <a href='<?= Url::to(['post/view', 'id' => $model->id]) ?>'><?= $model->title ?></a>
    </div>
    <div class="news-descr__info">
        <div class="news-descr__info-date"><?=$model->add_date?></div>
        <div class="news-descr__info-views"><?=$model->views?></div>
    </div>
    <div class="news-descr__text">
        <?=$model->short_text?>
    </div>
</div>