<?php
use backend\models\Product;
use backend\models\search\ProductSearch;

/* @var $this yii\web\View */
/* @var $model Product */
/* @var $currency integer */
/* @var $rate integer */

switch($currency) {
    case ProductSearch::CURRENCY_USD:
        $price = $model->price;
        break;
    case ProductSearch::CURRENCY_TG:
        $price = $model->getPriceRound();
        break;
    default:
        $price = $model->price;
}

?>

<a href="<?= \yii\helpers\Url::to([ ($model->isPhone() ? 'phone' : 'gadget') . '/view', 'id' => $model->id]) ?>">
    <div class="content-item__box">
        <div class="content-item__icon">
            <?=\rmrevin\yii\fontawesome\FontAwesome::i('eye fa-3x')?>
        </div>
        <div class="content-item__background" style="background-image: url('/<?= $model->getImage(Product::IMAGE_MAJOR) ?>')"></div>
        <div class="content-item__text-box">
            <div class="content-item__text">
                <div class="content-item__text-brand"><?= $model->name ?></div>
                <div class="content-item__text-info">
                    <?= $currency == ProductSearch::CURRENCY_USD ? "$" : "" ?><?= number_format($price, 0, ',', ' ') ?>
                    <?= $currency == ProductSearch::CURRENCY_TG ? " тг" : "" ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-item__chars">
        <?php if(isset($model->phoneDisplay)): ?>
        <div class="content-item__char" title="<?=$model->phoneDisplay->value?>">
            <span class="content-item__char-name"><?=$model->phoneDisplay->char->name?></span>
            <br>
            <span class="content-item__char-value"><?=$model->phoneDisplay->value?></span>
        </div>
        <?php endif; ?>
        <?php if(isset($model->phoneBattery)): ?>
        <div class="content-item__char" title="<?=$model->phoneBattery->value?>">
            <span class="content-item__char-name"><?=$model->phoneBattery->char->name?></span>
            <br>
            <span class="content-item__char-value"><?=$model->phoneBattery->value?></span>
        </div>
        <?php endif; ?>
        <?php if(isset($model->phoneCamera)): ?>
        <div class="content-item__char" title="<?=$model->phoneCamera->value?>">
            <span class="content-item__char-name"><?=$model->phoneCamera->char->name?></span>
            <br>
            <span class="content-item__char-value"><?=$model->phoneCamera->value?></span>
        </div>
        <?php endif; ?>
        <?php if(isset($model->phoneRam)): ?>
        <div class="content-item__char" title="<?=$model->phoneRam->value?>">
            <span class="content-item__char-name"><?=$model->phoneRam->char->name?></span>
            <br>
            <span class="content-item__char-value"><?=$model->phoneRam->value?></span>
        </div>
        <?php endif; ?>
    </div>
</a>