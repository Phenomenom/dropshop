<?php

use backend\models\PriceMail;
use backend\models\Product;
use backend\models\Setting;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Product */
/* @var $priceMail PriceMail */

$this->title = Yii::$app->name.' - '.$model->name;
$this->params['breadcrumbs'][] = [
    'label' => Yii::$app->controller->id == 'phone' ? 'Смартфоны' : 'Гаджеты',
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $model->name;

\Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->getAbsoluteUrl()]);
\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $model->name]);
\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => mb_strimwidth($model->descr, 0, 300, "...")]);
\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => Url::home(true) . $model->getImage(Product::IMAGE_MAJOR)]);

const IMAGE_COUNT = 4;
const JS_IMAGE = 'js-image-';
const JS_BUTTON = 'js-button-';

$rate = Setting::getExchange();

?>

<?= Html::hiddenInput('product-sets', json_encode(ArrayHelper::getColumn($model->productSets,'id'))) ?>
<?= Html::hiddenInput('product-price', $model->price) ?>
<?= Html::hiddenInput('currency', $rate) ?>
<div class="view-product">
    <?= $this->render('view/_header', ['model' => $model, 'priceMail' => $priceMail, 'rate' => $rate]) ?>
    <div class="product-media-background">
        <div class="product-media">
            <div class="product-media__images-box">
                <?php
                $count = 0;
                $items = [];

                foreach($model->getImages(Product::IMAGE_MINOR) as $image) {
                    $items[] = [
                        'url' => '/'.$image,
                        'src' => '/'.$image,
                        'imageOptions' => [
//                            'title' => 'Camposanto monumentale (inside)',
                            'class' => 'product-media__image img-thumbnail'
                        ]
                    ];

                    $count++;
                    if ($count >= IMAGE_COUNT) {
                        break;
                    }
                }
                ?>
                <?= dosamigos\gallery\Gallery::widget(['items' => $items]);?>
            </div>
            <div class="product-media__video">
                <h3>Видеообзор</h3>
                <iframe width="100%" height="335" src="<?= $model->video_url ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <?= $this->render('view/_chars', ['model' => $model]) ?>
</div>

<div class="reviews-background">
    <div class="reviews-content">
        <h3>Отзывы</h3>
        <div id="spk-widget-reviews" style="display:none; width: 100%;"
             shop-id="<?= Yii::$app->params['review-id'] ?>"
             good-id="<?= $model->id ?>"
             good-title="<?= $model->name ?>"
             good-url="<?= Yii::$app->request->getAbsoluteUrl() ?>">
        </div>
        <script async="async" type="text/javascript"
                src="//static.sprosikupi.ru/js/widget/sprosikupi.bootstrap.js">
        </script>
    </div>
</div>

<div style="background-color: #eeeeee">
    <?= $this->render('view/_accessories', ['model' => $model]) ?>
</div>

<div style="background-color: #eeeeee">
    <?= $this->render('view/_mates', ['model' => $model]) ?>
</div>

<?php
    $this->registerJsFile('/js/product.js?v=1', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>