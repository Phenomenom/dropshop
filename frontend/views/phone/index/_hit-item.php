<?php

use backend\models\Product;

/* @var $this yii\web\View */
/* @var $model Product */

$cost = $model->getPriceRound();

?>

<a href="<?= \yii\helpers\Url::to(['phone/view', 'id' => $model->id]) ?>">
    <div class="hit-item__image" style="background-image: url('/<?=$model->getImage($image)?>')"></div>
    <div class="hit-item__name"><?= $model->name ?></div>
    <div class="hit-item__price"><?= number_format($cost, 0, ',', ' ') ?> тг.</div>
</a>

