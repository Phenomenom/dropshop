<?php

/* @var $this yii\web\View */

use backend\models\Post;

$posts = Post::find()->latest()->published()->limit(5)->all();

?>

<div class="mainpost-background">
    <div class="mainpost">
        <h2 class="text-center">Последние новости</h2>
        <div class="mainpost-items">
            <?php foreach($posts as $post): ?>
                <?= $this->render('_post-item',[
                    'model' => $post
                ]); ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>