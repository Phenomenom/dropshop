<?php

use backend\models\Post;

/* @var $this yii\web\View */
/* @var $model Post */

?>

<a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $model->id]) ?>">
    <div class="mainpost-item">
        <div class="mainpost-item__image" style="background-image: url('/<?= $model->getImage(Post::IMAGE_SMALL) ?>')"></div>
        <div class="mainpost-item__title"><?= $model->title ?></div>
    </div>
</a>


