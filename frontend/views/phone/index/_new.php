<?php

/* @var $this yii\web\View */

use backend\models\Product;

$productsNew = Product::find()->active()->random()->novelty()->limit(5)->all();

?>

<div class="novelty-background">
    <div class="novelty-parent">
        <div class="novelty">
            <div class="novelty-title">
                Новинки
            </div>
            <div class="novelty-all">
                <a href="<?=\yii\helpers\Url::to(['all/index', 'states' => Product::STATE_NEW])?>">Посмотреть все</a>
            </div>
            <div class="novelty-items">
                <?php foreach($productsNew as $productNew): ?>
                <div class="novelty-item">
                    <?= $this->render('//phone/index/_new-item',[
                        'model' => $productNew,
                        'image' => Product::IMAGE_MAJOR,
                    ]); ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php if(false): ?>
        <div>
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 103372169);
            </script>
        </div>
        <?php endif; ?>
    </div>
</div>
<div class="novelty-fader"></div>