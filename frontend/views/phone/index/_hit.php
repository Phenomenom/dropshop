<?php

/* @var $this yii\web\View */

use backend\models\Product;

$productsNew = Product::find()->active()->random()->hit()->limit(4)->all();

?>

<div class="hit-background">
    <div class="hit-parent">
        <div class="hit">
            <div class="hit-title">
                Хиты продаж
            </div>
            <div class="hit-all">
                <a href="<?=\yii\helpers\Url::to(['all/index', 'states' => Product::STATE_HIT])?>">Посмотреть все</a>
            </div>
            <div class="hit-items">
                <?php foreach($productsNew as $productNew): ?>
                    <div class="hit-item">
                        <?= $this->render('//phone/index/_hit-item',[
                            'model' => $productNew,
                            'image' => Product::IMAGE_MAJOR,
                        ]); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>