<?php

?>

<?php $priceOrder = $dataProvider->sort->getAttributeOrder('price') ?>
<?php
$priceAsc = '';
$priceDesc = '';
if($priceOrder == SORT_ASC) $priceAsc = '(This)';
if($priceOrder == SORT_DESC) $priceDesc = '(This)';
?>
<?= Html::a('+Price '.$priceAsc,Url::current(['sort' => 'price'])) ?>
<?= Html::a('-Price '.$priceDesc,Url::current(['sort' => '-price'])) ?>

&nbsp;<?=$dataProvider->sort->link('price',['label' => 'Цена'])?>
&nbsp;<?=$dataProvider->sort->link('add_date',['label' => 'Новизна'])?>
