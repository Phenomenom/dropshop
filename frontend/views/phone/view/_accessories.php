<?php

use backend\models\Accessory;

/* @var $this yii\web\View */
/* @var $model Accessory */

$accessories = $model->accessories;
shuffle($accessories);
?>

<?php if($accessories): ?>
    <hr>
    <div class="hit-parent">
        <div class="hit">
            <div class="hit-title">
                Аксессуары к товару
            </div>
            <div class="hit-items">
                <?php foreach($accessories as $accessory): ?>
                    <div class="hit-item">
                        <?= $this->render('_accessory-item',[
                            'model' => $accessory,
                            'image' => Accessory::IMAGE,
                            'controller' => 'accessory',
                        ]); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>