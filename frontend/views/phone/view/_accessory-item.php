<?php

use backend\models\Accessory;
use common\helpers\shoppingcart\ShoppingCart;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Accessory */
/* @var $image string */
/* @var $controller string */

$url = 'view';
if($controller !== null) {
    $url = $controller . '/' . $url;
}
?>

<?= Html::beginForm(['cart/add-product'], 'POST'); ?>
<span style="cursor: pointer" onclick="this.parentNode.submit();">
    <div class="hit-item__image" style="background-image: url('/<?=$model->getImage($image)?>')"></div>
    <div class="hit-item__name"><?= $model->name ?></div>
    <div class="hit-item__price"><?= $model->price * \backend\models\Setting::$exchange ?> тг.</div>
</span>
<?= Html::hiddenInput('id', $model->id) ?>
<?= Html::hiddenInput('type', ShoppingCart::TYPE_ACCESSORY) ?>
<?= Html::hiddenInput('product_set', NO_PRODUCT_SET) ?>
<?= Html::endForm() ?>

