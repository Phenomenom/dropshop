<?php

use backend\models\PriceMail;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $priceMail PriceMail */

?>

<div id="js-subscribe">

    <p>Подпишитесь, чтобы первым узнать о снижении цены на данный товар</p>

    <?php $form = ActiveForm::begin(['id' => 'js-subscribe-form']); ?>

    <?= $form->field($priceMail, 'product_id')->hiddenInput()->label(false) ?>
    <?= $form->field($priceMail, 'email')->textInput()->label('Укажите E-mail') ?>
    <?= \yii\helpers\Html::button('Подписаться', ['class' => 'btn btn-warning', 'id' => 'js-subscribe-button']) ?>

    <?php ActiveForm::end(); ?>
</div>

<div class="spinner" id="js-loading" style="display: none">

</div>

<div class="text-center" id="js-success" style="display: none">
    <h3>Вы успешно подписались.</h3>
    <br>
    <br>
</div>

<?php $this->registerJsFile('/js/product.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>

<style>
    .spinner {
        width: 40px;
        height: 40px;
        background-color: #333;

        margin: 100px auto;
        -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;
        animation: sk-rotateplane 1.2s infinite ease-in-out;
    }

    @-webkit-keyframes sk-rotateplane {
        0% { -webkit-transform: perspective(120px) }
        50% { -webkit-transform: perspective(120px) rotateY(180deg) }
        100% { -webkit-transform: perspective(120px) rotateY(180deg)  rotateX(180deg) }
    }

    @keyframes sk-rotateplane {
        0% {
            transform: perspective(120px) rotateX(0deg) rotateY(0deg);
            -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg)
        } 50% {
              transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
              -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg)
          } 100% {
                transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
                -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
            }
    }
</style>