<?php

use backend\models\Product;
use backend\models\ProductSet;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Product */

?>

<div class="product-chars-background">
    <div class="product-chars">
        <h3>Характеристики</h3>
        <table class="product-chars__table">
            <thead>
            <tr>
                <td class="product-chars__name"></td>
                <td class="product-chars__value"></td>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($model->productChars as $productChar): ?>
                    <tr class="product-chars__row">
                        <td class="product-chars__name-item">
                            <div ><?= $productChar->char->name ?></div>
                        </td>
                        <td class="product-chars__value-item">
                            <div  ><?= $productChar->value ?></div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>