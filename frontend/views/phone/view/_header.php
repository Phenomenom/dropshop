<?php

use backend\models\PriceMail;
use backend\models\Product;
use backend\models\ProductSet;
use common\helpers\shoppingcart\ShoppingCart;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Product */
/* @var $priceMail PriceMail */
/* @var $rate integer */

$noProductSet = -1;

?>

<div class="product-header-background">
    <div class="product-header">
        <div class="product-header__image">
            <div class="js-image product-header__image-item" style="background-image: url(<?= '/' . $model->getImage(Product::IMAGE_MAJOR) ?>)"></div>
            <?php foreach ($model->productSets as $productSet): ?>
                <div class="js-image js-image-<?= $productSet->id ?> product-header__image-item product-header__image-item--hidden"
                     style="background-image: url(<?= '/' . $productSet->getImage(ProductSet::IMAGE) ?>)">
                </div>
            <?php endforeach; ?>
        </div>
        <div class="product-header__info">
            <div class="spk-good-rating" shop-id="<?= Yii::$app->params['review-id'] ?>" good-id="<?= $model->id ?>"></div>
            <div class="product-header__name"><h1><?= $model->name ?></h1></div>
            <div style="margin-bottom: 10px;">
                <div class="product-header__states">
                    <?php foreach($model->stateArray as $state): ?>
                        <div class="product-header__states-item label label-<?= $model->getStateLabelColor()[$state]?>">
                            <?= $model->getStateLabels()[$state] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="product-header__price-notify">

                    <?php
                    Modal::begin([
                        'header' => '<h3>Уведомление о снижении цены</h3>',
                        'id' => 'js-modal-price',
                        'clientOptions' => [
                            'keyboard' => false,
                            'backdrop' => 'static',
                        ],
                        'toggleButton' => ['label' => 'Узнать о снижении цены', 'class' => 'btn btn-link'],
                    ]);
                    echo $this->render('_modal_price', ['priceMail' => $priceMail]);
                    Modal::end();
                    ?>
                </div>
            </div>
            <div class="product-header__set">
                <?php foreach($model->productSets as $productSet): ?>
                    <?= Html::button($productSet->text, [
                        'data-price' => $productSet->price,
                        'class' => 'btn btn-default button-disabled js-button js-button-'.$productSet->id
                    ]) ?>
                <?php endforeach; ?>
            </div>
            <div class="product-header__price">
                <span>Цена: </span>
                <span class='js-price'><?= $model->getPriceRound() ?></span> тг
            </div>
            <div class="product-header__order">
                <?php
    //            Modal::begin([
    //                'header' => '<h3>Заказать</h3>',
    //                'id' => 'js-modal-order',
    //                'toggleButton' => ['label' => 'Заказать', 'class' => 'product-header__order-my btn btn-danger'],
    //            ]);
    //            echo $this->render('_modal_order');
    //            Modal::end();
                ?>

                <?php $form = ActiveForm::begin(['action' => ['cart/add-product'], 'method' => 'POST']); ?>
                    <?= Html::hiddenInput('id', $model->id) ?>
                    <?= Html::hiddenInput('type', ShoppingCart::TYPE_PRODUCT) ?>
                    <?= Html::hiddenInput('product_set', $noProductSet) ?>
                    <?= Html::submitButton(Yii::t('app', 'To Cart'), ['class' => 'btn btn-success', 'id' => 'js-to-cart', 'disabled' => 'true']) ?>
                <?php ActiveForm::end(); ?>

    <!--            <button class="product-header__order-kaspi btn btn-danger">KASPI</button>-->
            </div>
        </div>
    </div>
</div>