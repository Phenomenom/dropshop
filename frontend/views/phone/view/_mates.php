<?php

use backend\models\Product;

/* @var $this yii\web\View */
/* @var $model Product */

$productMates = $model->productMates;
shuffle($productMates);
?>

<?php if($productMates): ?>
    <hr>
    <div style="margin-bottom: 15px;" class="hit-parent">
        <div class="hit">
            <div class="hit-title">
                С этим товаром также смотрят
            </div>
            <div class="hit-items">
                <?php foreach($productMates as $productMate): ?>
                    <div class="hit-item">
                        <?= $this->render('//phone/index/_new-item',[
                            'model' => $productMate,
                            'image' => Product::IMAGE_MAJOR,
                        ]); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>