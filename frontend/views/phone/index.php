<?php

use backend\models\search\ProductSearch;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $currency integer */
/* @var $rate integer */
/* @var $title string */

$this->title = Yii::$app->name.' - Смартфоны';
$this->params['breadcrumbs'][] = ['label' => 'Смартфоны'];

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'pjax-container',
    'timeout' => 6500,
    'clientOptions' => ['container' => '#pjax-container']]); ?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'id' => 'js-activeform',
    'options' => ['data-pjax' => true ],
]); ?>

<?php ActiveForm::end(); ?>

<h1 class="text-center"><?= $title !== null ? $title : "Смартфоны" ?></h1>

<div class="text-center">
    <?= $this->render('index/_search', ['model' => $searchModel, 'dataProvider' => $dataProvider]) ?>
</div>

<div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'js-listview content-background',
        ],
        'itemOptions' => ['class' => 'content-item'],
        'itemView' => '_item',
        'viewParams' => [
            'currency' => $currency,
            'rate' => $rate,
        ],
        'layout' => '{pager}<div class="content">{items}</div>',
        'emptyTextOptions' => [
            'class' => 'text-center',
        ],
        'emptyText' => '<img src="/img/not_found.png"><br>Ничего не найдено..',
    ]); ?>
</div>
<?php \yii\widgets\Pjax::end(); ?>

<div>
    <?= $this->render('/layouts/_map') ?>
</div>

<?php
$this->registerJsFile('/js/index.js?v=1', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>