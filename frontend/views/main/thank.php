<?php

use backend\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

?>

<body style="width: 100%; height: 100%; display: flex; flex-direction: column; justify-content: center; align-items: center">
<h1>ВАШ ЗАКАЗ ПРИНЯТ!</h1>
<h2 style="text-align: center">
    Уважаемый <?=$name?>,ваш заказ №<?=$id?> принят!<br>
    Если у Вас возникли вопросы, пожалуйста свяжитесь с нами.<br>
    Спасибо за покупки в нашем интернет-магазине!
</h2>
<a href="<?=Url::base(true)?>">Продолжить</a>
</body>