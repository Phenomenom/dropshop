<?php

use backend\models\Setting;

/* @var $this yii\web\View */
/* @var $products backend\models\Product[] */

?>

<style>
    body {
        margin: 0;
        padding: 0;
    }

    .wrapper {
        width: 100%;
        height: 300px;
        background-color: #fe134e;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .wrapper-title {
        font-family: ralewaylight;
        color: white;
    }

    .title {
        font-size: 60px;
    }

    .title-small {
        font-size: 36px;
    }

    .items {
        display: flex;
        justify-content: space-around;
        flex-wrap: wrap;
        width: 100%;
        padding: 0 100px;
        box-sizing: border-box;
    }


    .item {
        margin-top: 50px;
        width: 300px;
        border: 1px solid black;
        height: 500px;
        /*flex-grow: 1;*/
    }

    .item-inner {
        width: 100%;
        padding-bottom: 100%;
    }

    .access__descr {
        font-size: 18px;
        line-height: 22px;
        list-style-type: none;
        padding: 0;
    }

</style>

<html>
<head>
    <meta charset="utf-8">
    <title>Смартфоны по ценам Aliexpress.com</title>
    <meta name="generator" content="WYSIWYG Web Builder 9 - http://www.wysiwygwebbuilder.com">
    <link href="/images/faviconka_ru_1119.png" rel="shortcut icon">
    <link href="/css/Безымянный4.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/index.css" rel="stylesheet">
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/jquery.ui.effect.min.js"></script>
    <script src="/js/jquery.ui.effect-fade.min.js"></script>
    <script src="/js/wb.carousel.effects.min.js"></script>
    <script src="/fancybox/jquery.easing-1.3.pack.js"></script>
    <link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.0.css">
    <script src="/fancybox/jquery.fancybox-1.3.0.pack.js"></script>
    <script src="/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
    <script src="/js/wwb9.min.js"></script>
</head>
<body>
    <div class="wrapper">
        <div class="wrapper-title">
            <span class="title">Запчасти</span>
            <br>
            <span class="title-small">Хахаха</span>
        </div>
    </div>
    <div class="items">
        <?php
        foreach ($products as $product) {

            ?>
            <div class="item">

                <div class="product__header">
                    <a style="text-decoration:none;" href="/main/product?id=<?=$product->id?>">
                        <div class="product__title"><?=$product->name?></div>
                        <div class="product__marks">* * * * *</div>
                    </a>
                </div>
                <a style="text-decoration:none;" href="/main/product?id=<?=$product->id?>">
                    <div class="product__image"><img width="300" height="300" src="<?=$path.'/'.$product->getProductImageMain()->fullpath?>"></div>
                </a>
                <div>
                    <ul class="access__descr">
                    <?php
                    foreach($product->productChars as $productChar) {
                        echo "<li><b>{$productChar->char->name}</b>: {$productChar->value}</li>";
                    }
                    ?>
                    </ul>
                </div>
            </div>
        <?php
        }
        ?>

    </div>

    <div id="Layer8" style="position:relative;text-align:center;left:0%;width:100%;z-index:158;" title="">
        <div id="wb_Text12" style="top:-30px;width:688px;height:44px;text-align:center;margin: 15px auto;">
            <div id="adres"><span style="color:#000000;font-family:ralewaylight;font-size:37px;">Адрес нашего магазина</span></div></div>
        <div id="wb_Text12" style="left:0%;top:70px;width:100%;text-align:center;">
            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=00Su50IqsXM0Yna-PAwxJPjyqiiJfNNy&width=100%&height=400"></script>
        </div>
    </div>
    <div id="Layer7" style="position:relative;text-align:center;left:0%;width:100%;height:350px;z-index:159;" title="">
        <div id="Layer7_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
            <div id="wb_Image53" style="position:absolute;left:41px;top:49px;width:228px;height:39px;z-index:143;">
                <img src="/images/logfuter.png" id="Image53" alt="" style="width:228px;height:39px;"></div>
            <div id="wb_Text71" style="position:absolute;left:41px;top:121px;width:408px;height:96px;z-index:144;text-align:left;">
                <span style="color:#c0c0c0;font-family:ralewaylight;font-size:20px;">&#1048;&#1055; &quot;RAYANA&quot;<br>&#1070;&#1088;&#1080;&#1076;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1081; &#1080; &#1092;&#1072;&#1082;&#1090;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1081; &#1072;&#1076;&#1088;&#1077;&#1089;: &#1075;.&#1040;&#1083;&#1084;&#1072;&#1090;&#1099;, &#1091;&#1083;.&#1040;&#1073;&#1072;&#1103;, &#1076;.44, &#1058;&#1056;&#1062; &#8220;Symphony&#8221;<br></span></div>
            <div id="wb_Text72" style="position:absolute;left:621px;top:43px;width:347px;height:125px;text-align:right;z-index:145;">
                <span class="lptracker_phone" style="color:#ffffff;font-family:ralewaylight;font-size:21px;">&#1058;&#1077;&#1083;. 8 727 224 27 62<br></span>
                <span class="lptracker_phone" style="color:#ffffff;font-family:ralewaylight;font-size:21px;">8 775 762 12 82</span>
                <span style="color:#ffffff;font-family:ralewaylight;font-size:21px;"><br><br>Email : dropshop.kz@gmail.com<br>Skype : dropshop.kz</span></div>
            <div id="wb_Image55" style="position:absolute;left:795px;top:71px;width:23px;height:23px;z-index:146;">
                <img src="/images/wh.png" id="Image55" alt="" style="width:23px;height:23px;"></div>
            <div id="wb_Image56" style="position:absolute;left:751px;top:201px;width:52px;height:52px;z-index:147;">
                <a href="https://vk.com/dropshop_kz" target="_blank"><img src="/images/tuzk0HKJfzM.jpg" id="Image56" alt="" style="width:52px;height:52px;"></a></div>
            <div id="wb_Image57" style="position:absolute;left:808px;top:201px;width:50px;height:50px;z-index:148;">
                <a href="https://instagram.com/dropshop.kz/" target="_blank"><img src="/images/insta.png" id="Image57" alt="" style="width:50px;height:50px;"></a></div>
            <div id="wb_Image58" style="position:absolute;left:863px;top:201px;width:50px;height:50px;z-index:149;">
                <img src="/images/face.png" id="Image58" alt="" style="width:50px;height:50px;"></div>
            <div id="wb_Image59" style="position:absolute;left:919px;top:201px;width:50px;height:50px;z-index:150;">
                <img src="/images/you.png" id="Image59" alt="" style="width:50px;height:50px;"></div>
            <div id="wb_Image60" style="position:absolute;left:490px;top:51px;width:100px;height:100px;z-index:150;">
                <img src="/images/barcode.gif" id="Image60" alt="" style="width:100px;height:100px;"></div>
            <div id="wb_Text71" style="position:absolute;left:0%;top:230px;width:600px;height:96px;z-index:13;text-align:left;">
                <span style="color:#c0c0c0;font-family:ralewaylight;font-size:8px;">ПОЛИТИКА КОНФИНДЕНЦИАЛЬНОСТИ<br>Сайт уважает ваше право и соблюдает конфиденциальность при заполнении, передачи и хранении ваших конфиденциальных сведений. Размещение заявки на сайте означает Ваше согласие на обработку данных. Под персональными данными подразумевается информация, относящаяся к субъекту персональных данных, в частности фамилия, имя и отчество, дата рождения, адрес, контактные реквизиты (телефон, адрес электронной почты), семейное, имущественное положение и иные данные.<br><br><br>Целью обработки персональных данных является оказание сайтом услуг. В случае отзыва согласия на обработку своих персональных данных мы обязуемся удалить Ваши персональные данные в срок не позднее 3 рабочих дней. Отзыв согласия можно отправить в электронном виде на наш электронный адресс. e-mail: info@dropshop.kz</span></div>
        </div>
    </div>

</body>
</html>

