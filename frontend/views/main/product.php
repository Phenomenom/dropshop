<?php

/* @var $this yii\web\View */
use backend\models\Product;
use backend\models\ProductChar;
use backend\models\ProductPrice;
use backend\models\Setting;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $model backend\models\Product */

$this->title = 'DropShop.kz - '.$model->name;

?>

<html><head>
    <meta charset="utf-8">
    <meta name="generator" content="WYSIWYG Web Builder 9 - http://www.wysiwygwebbuilder.com">
    <link href="/css/Безымянный4.css" rel="stylesheet">
    <link href="/css/XiaomiRedmiNote3.css" rel="stylesheet">
<!--    <script type="text/javascript" async="" src="https://mc.yandex.ru/metrika/watch.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="https://mc.yandex.ru/metrika/watch.js"></script><script src="js/swfobject.js"></script><style type="text/css"></style>-->
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/script.js"></script>
    <script src="/fancybox/jquery.easing-1.3.pack.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.0.css">
    <script src="/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
    <script src="/fancybox/jquery.fancybox-1.3.0.pack.js"></script>
    <script>
        $(document).ready(function()
        {
            $("a[data-rel='PhotoGallery2']").attr('rel', 'PhotoGallery2');
            $("a[rel^='PhotoGallery2']").fancybox({
                "type": "image"
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('a[href^="#"]').on('click',function (e) {
                e.preventDefault();
                var target = this.hash,
                    $target = $(target);
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
            });
            $('#Editbox2').mask('+7 (799) 999-9999');
        });
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter31787106 = new Ya.Metrika({
                        id:31787106,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>&lt;div&gt;&lt;img src="https://mc.yandex.ru/watch/31787106" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- /LPTracker counter -->
<!--    <script src="//lptracker.ru/api/jquery-1.10.2.min.js"></script><script src="//lptracker.ru/api/stats_auto.js"></script><script src="//lptracker.ru/api/admin/login.php?site_id=6390"></script><link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic-ext"><link rel="stylesheet" type="text/css" href="//lptracker.ru/api/hunter.css"><script src="//lptracker.ru/api/im/jquery.bind-first-0.1.min.js"></script><script type="text/javascript" src="//lptracker.ru/api/hunter.php?param=1&amp;site=6390&amp;ip=de1g157191o93242"></script><script src="//lptracker.ru/api/im/jquery.inputmask.js"></script><script src="//lptracker.ru/api/im/jquery.inputmask-multi.js"></script></head>-->
<body style="min-width: 1200px; position: absolute; width: 100%;">
<!--<noindex><script async="" src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTYzOTA7bHN0YXRzLnJlZmVyZXIoKX07dmFyIGpxdWVyeV9sc3RhdHM9ZnVuY3Rpb24oKXtqUXN0YXQubm9Db25mbGljdCgpO2xvYWRzY3JpcHQoInN0YXRzX2F1dG8uanMiLGluaXRfbHN0YXRzKX07bG9hZHNjcmlwdCgianF1ZXJ5LTEuMTAuMi5taW4uanMiLGpxdWVyeV9sc3RhdHMp"></script></noindex>-->
<!-- /END LPTracker counter -->

<!-- /Google.Anal counter -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-43102267-2', 'auto');
    ga('send', 'pageview');
</script>

<div id="Layer1" style="text-align:center;left:0%;width:100%;height:680px;z-index:34; background-color: gray; background-size: cover; background-position: center; background-image: url('/<?=$model->getImage(Product::IMAGE_BANNER)?>')" title="">
    <div id="Layer1_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">

        <div style="display: flex; height: 100%">
            <div style="width: 50%"></div>
            <div style="width: 50%; display: flex; justify-content: center; flex-direction: column">
                <div style="margin-bottom: 10px;">
                    <span style="color:#f5f5f5;font-family:ralewaylight;font-size:43px;"><b><?=$model->name?></b>
                        <br>
                    </span><span style="color:#f5f5f5;font-family:ralewaylight;font-size:35px;"> Практичный смартфон!</span>
                </div>
                <div style="position: relative; left: 25px; width:600px; text-align:left; display: flex; justify-content: center; flex-direction: column">
                    <?php
                    $priceArray = $model->getPriceArray();
                    /* @var $price ProductPrice */
                    ?>
                    <?php foreach($priceArray as $productPrice) {?>
                        <div>
                            <span style="color:#ffffff;font-family:'ralewaylight';font-size:35px;"><?=$productPrice->name?> - </span>
                            <span style="color:#ffd700;font-family:'gotham pro black';font-size:35px;"><?=$productPrice->price?>$
                            </span><span style="color:#ffffff;font-family:'gotham pro black';font-size:35px;">/ <?=Setting::roundPrice($productPrice->price)?> тенге</span>
                        </div>
                    <?php } ?>
                </div>

                <div style="display: none">
                    <div id="wb_Text3" style="margin-top: 20px;">
                        <span style="color:#ffffff;font-family:ralewaylight;font-size:15px;">Варианты расцветки</span>
                    </div>
                    <div id="wb_Shape1" style="width:37px;height:38px; margin-top: 10px; margin-bottom: 20px;">
                        <img src="/images/colorsnote3.png" id="Shape1" alt="" style="border-width:0;width:160px;height:38px;">
                    </div>
                </div>

                <div id="RollOver3" style="margin-top: 20px; overflow:hidden;width:220px;height:67px;">
                    <a href="#Order">
                        <img class="hover" alt="" src="/images/buttonstr2.png" style="left:0;top:0;width:220px;height:67px;">
                        <span><img alt="" src="/images/buttonstr.png" style="left:0;top:0;width:220px;height:67px"></span>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="container">

    <div style="display: flex; margin-top: 120px;">
        <div style="margin: 10px; display: flex; flex-direction: column">

            <div class="spk-good-rating" shop-id="<?=Yii::$app->params['review-id']?>" good-id="<?=$model->id?>"></div>

            <?php if(!empty($model->video_url)) { ?>
            <div>
            <span style="color:#000000;font-family:ralewaylight;font-size:43px;">Видеообзор</span>
            <iframe style="margin: 20px 0px;" width="493" height="320" src="<?=$model->video_url?>" allowfullscreen=""></iframe>
            </div>

            <?php } ?>

            <div id="PhotoGallery2" style="display: flex; flex-wrap: wrap; width:511px; justify-content: center">
                <?php foreach($model->getImages(Product::IMAGE_MINOR) as $image): ?>
                    <div class="figure" style="width:45%; display: inline-block; margin: 5px; display: flex; justify-content: center; align-items: center">
                        <a href="<?=$image?>" data-rel="PhotoGallery2" rel="PhotoGallery2">
                            <img style="max-width: 100%; height: auto;" alt="" title="" src="/<?=$image?>" style="width:auto;height:100%">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div style="margin: 10px;">
            <span style="color:#000000;font-family:ralewaylight;font-size:43px;">Технические характеристики</span>
            <span style="color:#000000;font-family:ralewaymedium;font-size:29px;"><br></span>
            <?php
            foreach($model->productChars as $productChar) {
                /* @var $productChar ProductChar */
                $char = $productChar->char;
                echo "<span style='color:#000000;font-family:ralewaybold;font-size:16px;'><br>{$char->name}: </span>";
                echo "<span style='color:#000000;font-family:ralewaylight;font-size:16px;'>{$productChar->value}<br></span>";
                echo "<span style='color:#000000;font-family:wingdings;font-size:16px;'><br></span>";
            }
            ?>
        </div>
    </div>

    <?php if(!empty($model->descr)): ?>
    <div style="margin: 0 50px 0 30px;">
        <div id="wb_Text7" style="height:60px;text-align: center">
            <span style="color:#000000;font-family:ralewaylight;font-size:43px;">Описание:</span>
        </div>
        <div id="wb_Text6" style="left:65px;text-align:left;">
            <div style="color:#000000;font-family:ralewaylight;font-size:17px;text-align: center; margin-bottom: 80px;">
                <span style="white-space: normal; line-height: 28px;">
                    <?=$model->descr?>
                </span>
            </div>
        </div>
    </div>
    <?php endif ?>

    <div>
        <div id="spk-widget-reviews" style="display:none; width: 100%;"
             shop-id="<?=Yii::$app->params['review-id']?>"
             good-id="<?=$model->id?>"
             good-title="<?=$model->name?>"
             good-url="<?=Url::toRoute(['main/product', 'id' => $model->id], true)?>">
        </div>
        <script async="async" type="text/javascript"
                src="//static.sprosikupi.ru/js/widget/sprosikupi.bootstrap.js">
        </script>
    </div>

    <div>
        <div id="Thank" style="display: none; height:150px;text-align:center;z-index:32; margin-top: 50px;">
            <div id="button">
                <div style="line-height:60px;">
                    <span style="color:#02b914;font-family:ralewaylight;font-size:43px;">Спасибо за ваш заказ!</span>
                </div>
                <br>
                <div>
                    <span style="color:#02b914;font-family:ralewaylight;font-size:43px;">Мы свяжемся с вами в ближайшее время</span>
                </div>
            </div>
        </div>
        <div id="Order">
            <div id="wb_Text8" style="height:150px;text-align:center;z-index:32;">
                <div id="button"><div style="line-height:60px;"><span style="color:#000000;font-family:ralewaylight;font-size:43px;">Хотите заказать смартфон?</span></div>
                    <div style="line-height:49px;"><span style="color:#000000;font-family:ralewaylight;font-size:43px;">Оставьте заявку прямо сейчас:</span></div>
                </div>
            </div>
            <div id="wb_Shape3" style="position:absolute; left:290px;width:407px;height:82px;z-index:25;">
                <img src="/images/img0072.png" id="Shape3" alt="" style="border-width:0;width:407px;height:82px;">
            </div>
            <div id="wb_Text9" style="position:absolute; margin-top:30px;left:312px;width:327px;height:19px;z-index:33;text-align:left;">
                <span style="color:#ffffff;font-family:'gotham pro medium';font-size:17px;">Внимательно заполните поля:</span>
            </div>
            <div id="wb_Form1" style="position:relative; top:75px; left:291px;width:403px;height:334px;z-index:24; margin-bottom: 50px;">

                <?php $form = ActiveForm::begin([
                    'id' => 'dynamic-form',
                    'action' => ['/main/request'],
                ]); ?>

                <?php ActiveForm::end(); ?>

                <form name="Form1" method="post" action="/send_email.php" enctype="multipart/form-data" id="Form1" class="-visor-no-click">
                    <input type="text" id="Editbox1" style="position:absolute;left:18px;top:26px;width:364px;height:54px;line-height:54px;padding-left: 15px;" name="client_name" value="" required="" placeholder="Введите имя: **">
                    <input type="text" id="Editbox2" style="position:absolute;left:19px;top:98px;width:364px;height:54px;line-height:54px;padding-left: 15px;" name="client_phone" value="" required="" placeholder="Введите телефон: **">
                    <input type="text" id="Editbox3" style="position:absolute;left:20px;top:171px;width:364px;height:54px;line-height:54px;padding-left: 15px;" name="client_address" value="" required="" placeholder="Ваш город: **">
                    <input type="submit" id="Button1" name="" value="Заказать" style="position:absolute;left:21px;top:241px;width:364px;height:64px;z-index:2;">
                    <input type="hidden" id="Editbox4" style="color:white;position:absolute;left:-2px;top:26px;width:0px;height:54px;line-height:54px;z-index:10;" name="phone_model" value="<?=$model->id?>" placeholder="">
                    <div id="field" style="position:absolute;left:20px;top:310px;width:300px;height:20px;z-index:22;">
                        <span style="color:#FF0000;font-family:ralewaylight;font-size:15px;">* обязательные поля</span>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<div id="Layer2" style="position:fixed;text-align:center;left:0;top:0;right:0;height:62px;z-index:35;" title="">
    <div id="Layer2_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
        <div id="RollOver1" style="position:absolute;overflow:hidden;left:60px;top:7px;width:180px;height:45px;z-index:13">
            <a href="/">
                <img class="hover" alt="" src="/images/vertnuts2.png" style="left:0;top:0;width:180px;height:45px;">
                <span><img alt="" src="/images/vertnuts.png" style="left:0;top:0;width:180px;height:45px"></span>
            </a>
        </div>
<!--        <div id="RollOver2" style="position:absolute;overflow:hidden;left:770px;top:7px;width:180px;height:45px;z-index:14">-->
<!--            <a href="./Letvx500.php">-->
<!--                <img class="hover" alt="" src="/images/next2.png" style="left:0;top:0;width:180px;height:45px;">-->
<!--                <span><img alt="" src="/images/next.png" style="left:0;top:0;width:180px;height:45px"></span>-->
<!--            </a>-->
<!--        </div>-->
        <div id="wb_Image1" style="position:absolute;left:387px;top:12px;width:228px;height:39px;z-index:15;">
            <img src="/images/logfuter.png" id="Image1" alt="" style="width:228px;height:39px;"></div>
    </div>
</div>

<div id="Layer7" style="margin-top: 100px; text-align:center;left:0%;width:100%;height:340px;z-index:36;" title="">
    <div id="Layer7_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
        <div id="wb_Image53" style="position:absolute;left:41px;top:49px;width:228px;height:39px;z-index:16;">
            <img src="/images/logfuter.png" id="Image53" alt="" style="width:228px;height:39px;"></div>
        <div id="wb_Text71" style="position:absolute;left:41px;top:121px;width:408px;height:96px;z-index:17;text-align:left;">
            <span style="color:#c0c0c0;font-family:ralewaylight;font-size:20px;">ИП "RAYANA"<br>Юридический и фактический адрес: г.Алматы, ул.Абая, д.44, ТРЦ “Symphony”<br></span></div>
        <div id="wb_Text72" style="position:absolute;left:621px;top:43px;width:347px;height:125px;text-align:right;z-index:18;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:21px;">Тел. 8 727 224 27 62<br>8 775 762 12 82<br><br>Email : dropshop.kz@gmail.com<br>Skype : dropshop.kz</span></div>
        <div id="wb_Image55" style="position:absolute;left:795px;top:71px;width:23px;height:23px;z-index:19;">
            <img src="/images/wh.png" id="Image55" alt="" style="width:23px;height:23px;"></div>
        <div id="wb_Image56" style="position:absolute;left:751px;top:251px;width:52px;height:52px;z-index:20;">
            <a href="https://vk.com/dropshop_kz" target="_blank"><img src="/images/tuzk0HKJfzM.jpg" id="Image56" alt="" style="width:52px;height:52px;"></a></div>
        <div id="wb_Image57" style="position:absolute;left:808px;top:251px;width:50px;height:50px;z-index:21;">
            <a href="https://instagram.com/dropshop.kz/"><img src="/images/insta.png" id="Image57" alt="" style="width:50px;height:50px;"></a></div>
        <div id="wb_Image58" style="position:absolute;left:863px;top:251px;width:50px;height:50px;z-index:22;">
            <img src="/images/face.png" id="Image58" alt="" style="width:50px;height:50px;"></div>
        <div id="wb_Image59" style="position:absolute;left:919px;top:251px;width:50px;height:50px;z-index:23;">
            <img src="/images/you.png" id="Image59" alt="" style="width:50px;height:50px;"></div>
    </div>
</div>

<!-- Код тега ремаркетинга Google -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 955541380;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script><iframe name="google_conversion_frame" title="Google conversion frame" width="300" height="0" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/955541380/?random=1460308022273&amp;cv=8&amp;fst=1460308022273&amp;num=1&amp;fmt=1&amp;guid=ON&amp;u_h=1080&amp;u_w=1920&amp;u_ah=1019&amp;u_aw=1920&amp;u_cd=24&amp;u_his=23&amp;u_tz=360&amp;u_java=false&amp;u_nplug=5&amp;u_nmime=7&amp;frm=0&amp;url=http%3A//dropshop.kz/XiaomiRedmiNote3.php&amp;ref=http%3A//dropshop.kz/XiaomiRedmi2.php&amp;tiba=Xiaomi%20RedMi%20Note%203" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no"></iframe>
<noscript>
    &lt;div style="display:inline;"&gt;
    &lt;img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/955541380/?value=0&amp;amp;guid=ON&amp;amp;script=0"/&gt;
    &lt;/div&gt;
</noscript>

</body></html>