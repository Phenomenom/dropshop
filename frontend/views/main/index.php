<?php

use backend\models\Product;
use backend\models\Setting;

/* @var $this yii\web\View */
/* @var $products backend\models\Product[] */

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Смартфоны по ценам Aliexpress.com</title>
<meta name="generator" content="WYSIWYG Web Builder 9 - http://www.wysiwygwebbuilder.com">
<link href="/images/faviconka_ru_1119.png" rel="shortcut icon">
<link href="/css/Безымянный4.css" rel="stylesheet">
<link href="/css/style.css" rel="stylesheet">
<link href="/css/index.css" rel="stylesheet">
<script src="/js/jquery-1.9.1.min.js"></script>
<script src="/js/jquery.ui.effect.min.js"></script>
<script src="/js/jquery.ui.effect-fade.min.js"></script>
<script src="/js/wb.carousel.effects.min.js"></script>
<script src="/fancybox/jquery.easing-1.3.pack.js"></script>
<link rel="stylesheet" href="/fancybox/jquery.fancybox-1.3.0.css">
<script src="/fancybox/jquery.fancybox-1.3.0.pack.js"></script>
<script src="/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
<script src="/js/wwb9.min.js"></script>
<!-- Разместите этот код один раз на странице категории -->
<script async="async" type="text/javascript"
        src="//static.sprosikupi.ru/js/widget/sprosikupi.bootstrap.js">
</script>
<script>
    $(document).ready(function()
    {
        var Carousel1Opts =
        {
            delay: 3000,
            duration: 500,
            easing: 'linear',
            mode: 'fade',
            direction: '',
            pagination: true,
            pagination_img_default: '/images/page_default.png',
            pagination_img_active: '/images/page_active.png',
            start: 0,
            width: 929
        };
        $("#Carousel1").carouseleffects(Carousel1Opts);
        $("#Carousel1_back a").click(function()
        {
            $('#Carousel1').carouseleffects('prev');
        });
        $("#Carousel1_next a").click(function()
        {
            $('#Carousel1').carouseleffects('next');
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('a[href^="#"]').on('click',function (e) {
            e.preventDefault();
            var target = this.hash,
                $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });
    });
</script>

<!-- CarrotQuest BEGIN -->
<!--<script type="text/javascript">
  (function(){ 
    if (typeof carrotquest === 'undefined') {
      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; 
      s.src = '//cdn.carrotquest.io/api.min.js'; 
      var x = document.getElementsByTagName('head')[0]; x.appendChild(s);
      
      carrotquest = {}; window.carrotquestasync = []; carrotquest.settings = {};
      m = ['connect', 'track', 'identify', 'auth'];
      function Build(name, args){return function(){window.carrotquestasync.push(name, arguments);} }
      for (var i = 0; i < m.length; i++) carrotquest[m[i]] = Build(m[i]);
    }
  })();
  carrotquest.connect('1385-2e6bb7fa97d9d6e7db51e2cce99');
</script> -->
<!-- CarrotQuest END -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31787106 = new Ya.Metrika({
                    id:31787106,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31787106" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- /LPTracker counter -->
<noindex><script async src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTYzOTA7bHN0YXRzLnJlZmVyZXIoKX07dmFyIGpxdWVyeV9sc3RhdHM9ZnVuY3Rpb24oKXtqUXN0YXQubm9Db25mbGljdCgpO2xvYWRzY3JpcHQoInN0YXRzX2F1dG8uanMiLGluaXRfbHN0YXRzKX07bG9hZHNjcmlwdCgianF1ZXJ5LTEuMTAuMi5taW4uanMiLGpxdWVyeV9sc3RhdHMp"></script></noindex>
<!-- /END LPTracker counter -->

<!-- /Google.Anal counter -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-43102267-2', 'auto');
    ga('send', 'pageview');
</script>

<script language=javascript>
    USD=346;
    /*Minotepro=151;
     Honor716=128;
     Honor764=148;
     Oneplustwo16=127;
     Oneplustwo64=137;
     Mx516=99;
     Mx532=116;
     P816=122;
     P864=161;
     Pro532=149;
     Pro564=174;
     Minote16=113;
     Minote64=133;
     Mate832=203;
     Mate864=228;
     Mate8128=262;
     Z2pro=110;
     Zenfone216=73;
     Zenfone232=87;
     Mi4C16=86;
     Mi4C32=96;
     Honor616=89;
     Honor632=98;
     Mx4pro16=77;
     Mx4pro32=98;
     Oneplusone16=93;
     Oneplusone64=109;
     Oneplusx=105;
     M2note16=57;
     M2note32=72;
     Redminote216=59;
     Redminote232=69;
     K3note=59;
     M2mini16=50;
     Redmi28=38;
     Redmi216=50;
     Redminote316=72;
     Redminote332=83;
     Letvx500=71;
     Letvx50032=78;
     Letvx600=67;
     Letvx60032=75;
     Letvx800=105;
     Letvx80064=118;
     Lenovop1=105;
    <!--Elephone=59;-->*/
    <!-- Доллары -->
    Minotepro_dol=415;//Math.round(Minotepro/USD*1000);
    Honor716_dol=340;//Math.round(Honor716/USD*1000);
    Honor764_dol=420;//Math.round(Honor764/USD*1000);
    Oneplustwo16_dol=370;//Math.round(Oneplustwo16/USD*1000);
    Oneplustwo64_dol=390;//Math.round(Oneplustwo64/USD*1000);
    Mx516_dol=255;//Math.round(Mx516/USD*1000);
    Mx532_dol=305;//Math.round(Mx532/USD*1000);
    P816_dol=374;//Math.round(P816/USD*1000);
    P864_dol=434;//Math.round(P864/USD*1000);
    Pro532_dol=420;//Math.round(Pro532/USD*1000);
    Pro564_dol=465;//Math.round(Pro564/USD*1000);
    Minote16_dol=319;//Math.round(Minote16/USD*1000);
    Minote64_dol=377;//Math.round(Minote64/USD*1000);
    Mate832_dol=500;//Math.round(Mate832/USD*1000);
    Mate864_dol=595;//Math.round(Mate864/USD*1000);
    Mate8128_dol=650;//Math.round(Mate8128/USD*1000);
    Z2pro_dol=300;//Math.round(Z2pro/USD*1000);
    Zenfone216_dol=220;//Math.round(Zenfone216/USD*1000);
    Zenfone232_dol=250;//Math.round(Zenfone232/USD*1000);
    Mi4C16_dol=215;//Math.round(Mi4C16/USD*1000);
    Mi4C32_dol=250;//Math.round(Mi4C32/USD*1000);
    Honor616_dol=251;//Math.round(Honor616/USD*1000);
    Honor632_dol=290;//Math.round(Honor632/USD*1000);
    Mx4pro16_dol=220;//Math.round(Mx4pro16/USD*1000);
    Mx4pro32_dol=260;//Math.round(Mx4pro32/USD*1000);
    Oneplusone16_dol=265;//Math.round(Oneplusone16/USD*1000);
    Oneplusone64_dol=290;//Math.round(Oneplusone64/USD*1000);
    Oneplusx_dol=310;//Math.round(Oneplusx/USD*1000);
    M2note16_dol=165;//Math.round(M2note16/USD*1000);
    M2note32_dol=203;//Math.round(M2note32/USD*1000);
    Redminote216_dol=170;//Math.round(Redminote216/USD*1000);
    Redminote232_dol=199;//Math.round(Redminote232/USD*1000);
    K3note_dol=164;//Math.round(K3note*1000/USD);
    M2mini16_dol=147;//Math.round(M2mini16/USD*1000);
    Redmi28_dol=120;//Math.round(Redmi28/USD*1000);
    Redmi216_dol=140;//Math.round(Redmi216/USD*1000);
    Redminote316_dol=197;//Math.round(Redminote316*1000/USD);
    Redminote332_dol=238;//Math.round(Redminote332*1000/USD);
    Letvx500_dol=200;//Math.round(Letvx500*1000/USD);
    Letvx50032_dol=235;
    Letvx600_dol=203;//Math.round(Letvx600*1000/USD);
    Letvx60032_dol=222;
    Letvx800_dol=300;//Math.round(Letvx800*1000/USD);
    Letvx80064_dol=333;
    Lenovop1_dol=296;//Math.round(Lenovop1*1000/USD);
    //-----------------------
    Minotepro=USD*Minotepro_dol;
    Honor716=USD*Honor716_dol;
    Honor764=USD*Honor764_dol;
    Oneplustwo16=USD*Oneplustwo16_dol;
    Oneplustwo64=USD*Oneplustwo64_dol;
    Mx516=USD*Mx516_dol;
    Mx532=USD*Mx532_dol;
    P816=USD*P816_dol;
    P864=USD*P864_dol;
    Pro532=USD*Pro532_dol;
    Pro564=USD*Pro564_dol;
    Minote16=USD*Minote16_dol;
    Minote64=USD*Minote64_dol;
    Mate832=USD*Mate832_dol;
    Mate864=USD*Mate864_dol;
    Mate8128=USD*Mate8128_dol;
    Z2pro=USD*Z2pro_dol;
    Zenfone216=USD*Zenfone216_dol;
    Zenfone232=USD*Zenfone232_dol;
    Mi4C16=USD*Mi4C16_dol;
    Mi4C32=USD*Mi4C32_dol;
    Honor616=USD*Honor616_dol;
    Honor632=USD*Honor632_dol;
    Mx4pro16=USD*Mx4pro16_dol;
    Mx4pro32=USD*Mx4pro32_dol;
    Oneplusone16=USD*Oneplusone16_dol;
    Oneplusone64=USD*Oneplusone64_dol;
    Oneplusx=USD*Oneplusx_dol;
    M2note16=USD*M2note16_dol;
    M2note32=USD*M2note32_dol;
    Redminote216=USD*Redminote216_dol;
    Redminote232=USD*Redminote232_dol;
    K3note=USD*K3note_dol;
    M2mini16=USD*M2mini16_dol;
    Redmi28=USD*Redmi28_dol;
    Redmi216=USD*Redmi216_dol;
    Redminote316=USD*Redminote316_dol;
    Redminote332=USD*Redminote332_dol;
    Letvx500=USD*Letvx500_dol;
    Letvx50032=USD*Letvx50032_dol;
    Letvx600=USD*Letvx600_dol;
    Letvx60032=USD*Letvx60032_dol;
    Letvx800=USD*Letvx800_dol;
    Letvx80064=USD*Letvx80064_dol;
    Lenovop1=USD*Lenovop1_dol;
    function div(val, by){
        return (val - val % by) / by;
    }
</script>

</head>
<body style="position: absolute; width: 100%;">
<div id="Layer1" style="text-align:center;left:0%;width:100%;height:850px;z-index:153;" title="">
    <div id="Layer2" style="width:999px;position:relative;margin-left:auto;margin-right:auto;text-align:center;left:0px;top:0px;width:100%;height:49px;z-index:0;">
        <span style="color:#000000;font-family:ralewaylight;font-size:43px;">Курс 1$ = <?=Setting::getExchange()?> тенге </span>
    </div>
    <div id="Layer1_Container" style="width:999px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
        <div id="wb_Image1" style="position:absolute;left:20px;top:29px;width:228px;height:39px;z-index:0;">
            <img src="/images/DropShop.png" id="Image1" alt="" style="width:228px;height:39px;"></div>
        <div id="wb_Text1" style="position:absolute;left:20px;top:120px;width:620px;height:124px;z-index:1;text-align:left;">
            <span style="color:#000000;font-family:ralewaylight;font-size:53px;">&#1057;&#1084;&#1072;&#1088;&#1090;&#1092;&#1086;&#1085;&#1099; &#1087;&#1086; &#1094;&#1077;&#1085;&#1077; aliexpress.com</span></div>
        <div id="wb_Text2" style="position:absolute;left:694px;top:12px;width:284px;height:64px;text-align:right;z-index:2;">
            <span class="lptracker_phone" style="color:#000000;font-family:helveticaneuecyrlight;font-size:29px;">8 727 224 27 62<br></span>
            <span class="lptracker_phone" style="color:#000000;font-family:helveticaneuecyrlight;font-size:29px;">8 775 762 12 82</span></div>
        <div id="wb_Text3" style="position:absolute;left:687px;top:78px;width:292px;height:24px;text-align:right;z-index:3;">
            <span style="color:#000000;font-family:ralewaylight;font-size:20px;"> Есть вопросы? Звоните!</span></div>
        <div id="wb_Text4" style="position:absolute;left:20px;top:290px;width:443px;height:93px;z-index:4;text-align:left;">
            <span style="color:#000000;font-family:ralewaylight;font-size:27px;">&#1057; &#1076;&#1086;&#1089;&#1090;&#1072;&#1074;&#1082;&#1086;&#1081; &#1074; 3 &#1088;&#1072;&#1079;&#1072; &#1073;&#1099;&#1089;&#1090;&#1088;&#1077;&#1077;!<br>&#1055;&#1086;&#1083;&#1091;&#1095;&#1080;&#1090;&#1077; &#1089;&#1074;&#1086;&#1081; &#1090;&#1077;&#1083;&#1077;&#1092;&#1086;&#1085; &#1091;&#1078;&#1077; &#1095;&#1077;&#1088;&#1077;&#1079; 10 &#1076;&#1085;&#1077;&#1081;!</span></div>
        <div id="wb_Text5" style="position:absolute;left:20px;top:412px;width:480px;height:38px;z-index:5;text-align:left;">
            <span style="color:#a9a9a9;font-family:ralewaylight;font-size:16px;">*&#1056;&#1072;&#1073;&#1086;&#1090;&#1072;&#1077;&#1084; &#1085;&#1072;&#1087;&#1088;&#1103;&#1084;&#1091;&#1102; &#1089; &#1082;&#1080;&#1090;&#1072;&#1081;&#1089;&#1082;&#1080;&#1084;&#1080; &#1079;&#1072;&#1074;&#1086;&#1076;&#1072;&#1084;&#1080;-&#1080;&#1079;&#1075;&#1086;&#1090;&#1086;&#1074;&#1080;&#1090;&#1077;&#1083;&#1103;&#1084;&#1080;, &#1082;&#1086;&#1090;&#1086;&#1088;&#1099;&#1077; &#1076;&#1072;&#1102;&#1090; &#1085;&#1080;&#1079;&#1082;&#1080;&#1077; &#1094;&#1077;&#1085;&#1099;</span></div>
        <div id="RollOver1" style="position:absolute;overflow:hidden;left:20px;top:500px;width:267px;height:72px;z-index:6">
            <a href="#katalog">
                <img class="hover" alt="" src="/images/katalog2.png" style="left:0;top:0;width:267px;height:72px;">
                <span><img alt="" src="/images/katalog.png" style="left:0;top:0;width:267px;height:72px"></span>
            </a>
        </div>
        <div id="RollOver2" style="position:absolute;overflow:hidden;left:314px;top:39px;width:176px;height:19px;z-index:8">
            <a href="javascript:displaylightbox('/main/dostavka',{width:900,height:2550})" target="_self">
                <img class="hover" alt="" src="/images/dostavka2.png" style="left:0;top:0;width:176px;height:19px;">
                <span><img alt="" src="/images/dostavka.png" style="left:0;top:0;width:176px;height:19px"></span>
            </a>
        </div>
        <div id="RollOver3" style="position:absolute;overflow:hidden;left:519px;top:42px;width:34px;height:13px;z-index:7">
            <a href="javascript:displaylightbox('/main/faq',{width:900,height:2550})" target="_self">
                <img class="hover" alt="" src="/images/faq2.png" style="left:0;top:0;width:34px;height:13px;">
                <span><img alt="" src="/images/faq.png" style="left:0;top:0;width:34px;height:13px"></span>
            </a>
        </div>
        <div id="Over4" style="position:absolute;left:590px;top:38px;width:90px;height:19px;z-index:7">
            <a href="#adres">
                <span style="color:#888888;font-family:ralewaylight;font-size:20px;">Адрес</span>
                <!--<img class="hover" alt="" src="/images/adres2.png" style="left:0;top:0;width:34px;height:13px;">
                <span><img alt="" src="/images/adres1.png" style="left:0;top:0;width:34px;height:13px"></span>-->
            </a>
        </div>
        <div id="wb_Image54" style="position:absolute;left:729px;top:40px;width:35px;height:35px;z-index:9;">
            <img src="/images/998789.png" id="Image54" alt="" style="width:35px;height:35px;"></div>
    </div>
</div>
<div id="Layer2" style="text-align:center;left:0%;width:100%;height:930px;z-index:154;" title="">
    <div id="Layer2_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
        <div id="wb_Text6" style="position:absolute;left:156px;top:62px;width:688px;height:44px;text-align:center;z-index:10;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:37px;">&#1055;&#1086;&#1095;&#1077;&#1084;&#1091;&nbsp; &#1079;&#1072;&#1082;&#1072;&#1079;&#1099;&#1074;&#1072;&#1090;&#1100; &#1091; &#1085;&#1072;&#1089; &#1074;&#1099;&#1075;&#1086;&#1076;&#1085;&#1086;?</span></div>
        <div id="wb_Image2" style="position:absolute;left:76px;top:175px;width:155px;height:155px;z-index:11;">
            <img src="/images/ico1.png" id="Image2" alt="" style="width:155px;height:155px;"></div>
        <div id="wb_Image3" style="position:absolute;left:422px;top:175px;width:155px;height:155px;z-index:12;">
            <img src="/images/ico2.png" id="Image3" alt="" style="width:155px;height:155px;"></div>
        <div id="wb_Image4" style="position:absolute;left:749px;top:175px;width:155px;height:155px;z-index:13;">
            <img src="/images/ico3.png" id="Image4" alt="" style="width:155px;height:155px;"></div>
        <div id="wb_Image5" style="position:absolute;left:244px;top:386px;width:155px;height:155px;z-index:14;">
            <img src="/images/ico4.png" id="Image5" alt="" style="width:155px;height:155px;"></div>
        <div id="wb_Image6" style="position:absolute;left:594px;top:386px;width:155px;height:155px;z-index:15;">
            <img src="/images/ico5.png" id="Image6" alt="" style="width:155px;height:155px;"></div>
        <div id="wb_Text7" style="position:absolute;left:53px;top:357px;width:200px;height:48px;text-align:center;z-index:16;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">&#1057;&#1072;&#1084;&#1099;&#1077; &#1085;&#1080;&#1079;&#1082;&#1080;&#1077; &#1094;&#1077;&#1085;&#1099; &#1085;&#1072; &#1088;&#1099;&#1085;&#1082;&#1077;</span></div>
        <div id="wb_Text8" style="position:absolute;left:400px;top:357px;width:200px;height:48px;text-align:center;z-index:17;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Быстрая доставка из Китая, от 7 дней!</span></div>
        <div id="wb_Text9" style="position:absolute;left:728px;top:357px;width:200px;height:48px;text-align:center;z-index:18;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Гарантия на все телефоны</span></div>
        <div id="wb_Text10" style="position:absolute;left:221px;top:569px;width:200px;height:48px;text-align:center;z-index:19;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Доставка по всем городам Казахстана!</span></div>
        <div id="wb_Text11" style="position:absolute;left:571px;top:569px;width:200px;height:48px;text-align:center;z-index:20;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Наличие запчастей и аксессуаров</span></div>
    </div>
</div>
<div id="Layer3" style="text-align:center;left:0%;width:100%;height:341px;z-index:155;" title="">
    <div id="Layer3_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
    </div>
</div>
<div id="Layer4" style="text-align:center;left:0%;width:100%;z-index:156;" title="">
<div id="Layer4_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">


<div id="wb_Text12" style="left:155px; padding: 30px 0 20px 0; height:44px;text-align:center;z-index:29;">
    <div id="katalog"><span style="color:#000000;font-family:ralewaylight;font-size:37px;">Каталог смартфонов</span></div></div>

    <div class="catalog">
    <?php
    foreach ($products as $product) {

        ?>
        <div class="product">

            <div class="product__header">
                <a style="text-decoration:none;" href="/main/product?id=<?=$product->id?>">
                <div class="product__title"><?=$product->name?></div>
                <div class="product__marks"><div class="spk-good-rating" shop-id="<?=Yii::$app->params['review-id']?>" good-id="<?=$product->id?>"></div></div>
                </a>
            </div>
            <a style="text-decoration:none;" href="/main/product?id=<?=$product->id?>">
            <div class="product__image"><img width="300" height="300" src="/<?=$product->getImage(Product::IMAGE_MAJOR)?>"></div>
            </a>
            <div class="product__content">
                <div class="product__price"><?=$product->price?>$ / <?=Setting::roundPrice($product->price, 500)?> тенге</div>
                <a class="product__buy" href="/main/product?id=<?=$product->id?>"></a>
            </div>
        </div>
        <?php
    }
    ?>
    </div>

</div>
</div>
<div id="Layer5" style="text-align:center;left:0%;width:100%;height:720px;z-index:157;" title="">
    <div id="Layer5_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
        <div id="wb_Text49" style="position:absolute;left:542px;top:60px;width:175px;height:44px;z-index:112;text-align:left;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:37px;">Оплата</span></div>
        <div id="wb_Text50" style="position:absolute;left:544px;top:114px;width:411px;height:184px;z-index:113;text-align:left;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">&#1053;&#1072;&#1083;&#1080;&#1095;&#1085;&#1099;&#1084;&#1080; &#1074; &#1085;&#1072;&#1096;&#1080;&#1093; &#1086;&#1092;&#1080;&#1089;&#1072;&#1093; &#1074; &#1075;. &#1040;&#1083;&#1084;&#1072;&#1090;&#1099;, &#1040;&#1090;&#1099;&#1088;&#1072;&#1091;, &#1040;&#1089;&#1090;&#1072;&#1085;&#1072;, &#1040;&#1082;&#1090;&#1072;&#1091;<br>&#1054;&#1090;&#1087;&#1088;&#1072;&#1074;&#1080;&#1090;&#1100; &#1076;&#1077;&#1085;&#1100;&#1075;&#1080; &#1085;&#1072; Qiwi-&#1082;&#1086;&#1096;&#1077;&#1083;&#1077;&#1082; (+4%)<br>&#1055;&#1077;&#1088;&#1077;&#1074;&#1086;&#1076; &#1095;&#1077;&#1088;&#1077;&#1079; &#1050;&#1072;&#1079;&#1087;&#1086;&#1095;&#1090;&#1091; (+1,5%)<br>&#1055;&#1077;&#1088;&#1077;&#1074;&#1086;&#1076; &#1085;&#1072; &#1089;&#1095;&#1077;&#1090; (+3,5%)<br>Вы можете оплатить заказ в отделении любого банка<br></span></div>
        <div id="wb_Text51" style="position:absolute;left:46px;top:470px;width:434px;height:144px;z-index:114;text-align:left;">
            <span style="color:#000000;font-family:ralewaylight;font-size:20px;">Доставка до Алматы <b>бесплатно - 7-10 дней</b>, во все остальные населенные пункты Казахстана курьерской службой АлемТАТ и<br> почтовой службой КазПочта<br>Бесплатная доставка 22-30 дней<br>Доставка 2-3 дней (+2500 тг).</span></div>
        <div id="wb_Text52" style="position:absolute;left:48px;top:411px;width:175px;height:44px;z-index:115;text-align:left;">
            <span style="color:#000000;font-family:ralewaylight;font-size:37px;">Доставка</span></div>
    </div>
</div>
<div id="Layer6" style="text-align:center;left:0%;width:100%;height:643px;z-index:158;" title="">
    <div id="Layer6_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
        <div id="wb_Image43" style="position:absolute;left:154px;top:125px;width:50px;height:51px;z-index:116;">
            <img src="/images/i1.png" id="Image43" alt="" style="width:50px;height:51px;"></div>
        <div id="wb_Text53" style="position:absolute;left:308px;top:35px;width:384px;height:44px;text-align:center;z-index:117;">
            <span style="color:#ffffff;font-family:ralewaylight;font-size:37px;">Как мы работаем?</span></div>
        <div id="wb_Text54" style="position:absolute;left:62px;top:219px;width:250px;height:104px;z-index:118;text-align:left;">
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">&#1042;&#1099; &#1074;&#1099;&#1073;&#1080;&#1088;&#1072;&#1077;&#1090;&#1077; &#1089;&#1084;&#1072;&#1088;&#1090;&#1092;&#1086;&#1085; </span></div>
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">&#1080;&#1079; &#1085;&#1072;&#1096;&#1077;&#1075;&#1086; &#1082;&#1072;&#1090;&#1072;&#1083;&#1086;&#1075;&#1072; &#1080; </span></div>
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">&#1086;&#1089;&#1090;&#1072;&#1074;&#1083;&#1103;&#1077;&#1090;&#1077; &#1079;&#1072;&#1103;&#1074;&#1082;&#1091; &#1085;&#1072; </span></div>
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">&#1089;&#1072;&#1081;&#1090;&#1077;</span></div>
        </div>
        <div id="wb_Text55" style="position:absolute;left:374px;top:219px;width:250px;height:26px;z-index:119;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Мы оформляем заказ</span></div>
        </div>
        <div id="wb_Image44" style="position:absolute;left:477px;top:125px;width:42px;height:42px;z-index:120;">
            <img src="/images/i2.png" id="Image44" alt="" style="width:42px;height:42px;"></div>
        <div id="wb_Text56" style="position:absolute;left:649px;top:219px;width:250px;height:26px;z-index:121;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Вы вносите предоплату</span></div>
        </div>
        <div id="wb_Image45" style="position:absolute;left:745px;top:125px;width:56px;height:30px;z-index:122;">
            <img src="/images/i3.png" id="Image45" alt="" style="width:56px;height:30px;"></div>
        <div id="wb_Image46" style="position:absolute;left:151px;top:372px;width:48px;height:59px;z-index:123;">
            <img src="/images/i4.png" id="Image46" alt="" style="width:48px;height:59px;"></div>
        <div id="wb_Image47" style="position:absolute;left:476px;top:372px;width:48px;height:31px;z-index:124;">
            <img src="/images/i5.png" id="Image47" alt="" style="width:48px;height:31px;"></div>
        <div id="wb_Image48" style="position:absolute;left:757px;top:372px;width:34px;height:50px;z-index:125;">
            <img src="/images/i6.png" id="Image48" alt="" style="width:34px;height:50px;"></div>
        <div id="wb_Text57" style="position:absolute;left:63px;top:469px;width:250px;height:52px;z-index:126;text-align:left;">
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Наши поставщики </span></div>
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">отправляют ваш заказ</span></div>
        </div>
        <div id="wb_Text58" style="position:absolute;left:374px;top:469px;width:250px;height:52px;z-index:127;text-align:left;">
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Мы получаем и проводим </span></div>
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">проверку качества</span></div>
        </div>
        <div id="wb_Text59" style="position:absolute;left:653px;top:469px;width:250px;height:78px;z-index:128;text-align:left;">
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">Отправляем заказ на ваш </span></div>
            <div style="line-height:26px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">адрес, либо забираете </span></div>
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;">заказ самовывозом</span></div>
        </div>
        <div id="wb_Text60" style="position:absolute;left:63px;top:125px;width:83px;height:26px;z-index:129;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;"><strong>01</strong></span></div>
        </div>
        <div id="wb_Text61" style="position:absolute;left:371px;top:125px;width:83px;height:26px;z-index:130;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;"><strong>02</strong></span></div>
        </div>
        <div id="wb_Text62" style="position:absolute;left:648px;top:125px;width:83px;height:26px;z-index:131;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;"><strong>03</strong></span></div>
        </div>
        <div id="wb_Text63" style="position:absolute;left:63px;top:372px;width:83px;height:26px;z-index:132;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;"><strong>04</strong></span></div>
        </div>
        <div id="wb_Text64" style="position:absolute;left:377px;top:372px;width:83px;height:26px;z-index:133;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;"><strong>05</strong></span></div>
        </div>
        <div id="wb_Text65" style="position:absolute;left:653px;top:372px;width:83px;height:26px;z-index:134;text-align:left;">
            <div style="line-height:24px;"><span style="color:#ffffff;font-family:ralewaylight;font-size:20px;"><strong>06</strong></span></div>
        </div>
    </div>
</div>

<div id="container">
    <div id="wb_Text66" style="height:44px;text-align:center;z-index:152;padding-top: 20px;">
        <span style="color:#000000;font-family:ralewaylight;font-size:37px;">Отзывы наших покупателей</span>
    </div>
    <div id="wb_Carousel1" style="left:41px;width:939px;height:378px;z-index:151;overflow:hidden">
        <div id="Carousel1" style="position:absolute">
            <div class="frame">
                <div id="wb_Image49" style="position:absolute;left:137px;top:96px;width:170px;height:170px;z-index:135;">
                    <img src="/images/ot8.jpg" id="Image49" alt="" style="width:170px;height:170px;"></div>
                <div id="wb_Text67" style="position:absolute;left:334px;top:74px;width:505px;height:209px;z-index:136;text-align:left;">
                    <div><span style="color:#009ddc;font-family:ralewaylight;font-size:21px;">&#0171;&#1058;&#1077;&#1087;&#1077;&#1088;&#1100; &#1074;&#1089;&#1077;&#1075;&#1076;&#1072; &#1073;&#1091;&#1076;&#1091; &#1087;&#1086;&#1082;&#1091;&#1087;&#1072;&#1090;&#1100; &#1079;&#1076;&#1077;&#1089;&#1100;!&#0187;</span></div>
                    <div><span style="color:#000000;font-family:arial;font-size:13px;"><br></span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Покупал у вас One Plus One, вначале сомневался, цены были </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">дешевые. Знакомый сходил к ним в офис в Алматы, продавцы </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">внушили доверие, отправил деньги на счет. Понравились </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">сроки доставки, телефон прише очень быстро и цены почти, </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">как на алиекспресе. Теперь всегда буду покупать здесь.</span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;"><br></span></div>
                    <div style="line-height:20px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Ержан Хибасов, г. Актау</span></div>
                </div>
            </div>
            <div class="frame" style="display:none">
                <div id="wb_Text68" style="position:absolute;left:334px;top:65px;width:505px;height:233px;z-index:137;text-align:left;">
                    <div><span style="color:#009ddc;font-family:ralewaylight;font-size:21px;">&#0171;DropShop все сделали как обещал..!&#0187;</span></div>
                    <div><span style="color:#000000;font-family:arial;font-size:13px;"><br></span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Огромное спасибо этому сайту! Раньше заказывал напрямую </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">из Китая через другие сайты и посылка шла очень долго. </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">DropShop все сделали как обещали, привезли быстро за теже </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">деньги. Долго не решался отправлять деньги. Сначала заказал </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">аксессуары для проверки, затем перечислил деньги за </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">телефон. Им можно доверять!</span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;"><br></span></div>
                    <div style="line-height:20px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Ильяс Ратбеков, г. Чу</span></div>
                </div>
                <div id="wb_Image50" style="position:absolute;left:137px;top:96px;width:170px;height:170px;z-index:138;">
                    <img src="/images/ot2.png" id="Image50" alt="" style="width:170px;height:170px;"></div>
            </div>
            <div class="frame" style="display:none">
                <div id="wb_Text69" style="position:absolute;left:334px;top:99px;width:505px;height:161px;z-index:139;text-align:left;">
                    <div><span style="color:#009ddc;font-family:ralewaylight;font-size:21px;">&#0171;Дешевле не нашла..!&#0187;</span></div>
                    <div><span style="color:#000000;font-family:arial;font-size:13px;"><br></span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Долго не могла определиться с телефоном. Брат показал мне </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">этот сайт, здесь мне помогли выбрать телефон. Я очень рада!</span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Сравнила цены, дешевле не нашла! Все советую!</span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;"><br></span></div>
                    <div style="line-height:20px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Ельмира Еламанова,&nbsp; г. Алматы</span></div>
                </div>
                <div id="wb_Image51" style="position:absolute;left:137px;top:96px;width:170px;height:170px;z-index:140;">
                    <img src="/images/ot10.jpg" id="Image51" alt="" style="width:170px;height:170px;"></div>
            </div>
            <div class="frame" style="display:none">
                <div id="wb_Image52" style="position:absolute;left:137px;top:95px;width:170px;height:170px;z-index:141;">
                    <img src="/images/ot9.jpg" id="Image52" alt="" style="width:170px;height:170px;"></div>
                <div id="wb_Text70" style="position:absolute;left:334px;top:88px;width:505px;height:185px;z-index:142;text-align:left;">
                    <div><span style="color:#009ddc;font-family:ralewaylight;font-size:21px;">&#0171;Доставили быстро..!&#0187;</span></div>
                    <div><span style="color:#000000;font-family:arial;font-size:13px;"><br></span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Делал заказ дважды. Сначала заказал телефон Zoppo ZP520.</span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Затем сломал экран, заказали запчасть сделали по гарантии. </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Все как положено. Цены ниже, чем везде, доставили быстро, </span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">продавцы толковые. Молодцы, желаю успехов в будущем!</span></div>
                    <div style="line-height:24px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;"><br></span></div>
                    <div style="line-height:20px;"><span style="color:#000000;font-family:ralewaylight;font-size:17px;">Александр Ковалев, г. Алматы</span></div>
                </div>
            </div>
        </div>
        <div id="Carousel1_back" style="position:absolute;left:4px;top:149px;width:60px;height:60px;z-index:999"><a style="cursor:pointer"><img alt="Back" style="border-style:none" src="/images/let.jpg"></a></div>
        <div id="Carousel1_next" style="position:absolute;left:865px;top:149px;width:60px;height:60px;z-index:999"><a style="cursor:pointer"><img alt="Next" style="border-style:none" src="/images/right.jpg"></a></div>
    </div>
</div>

<hr>

<div style="margin: 25px 0"></div>

<div class="Layer7" style="">

    <div id="Layer9" style="position:relative;text-align:center;z-index:158;padding: 0 20px;margin-top:25px;" title="" class="row">

        <div id="spk-widget-reviews" style="display:none; width: 800px;margin:0 auto;"
             shop-id="<?=Yii::$app->params['review-id']?>"
             good-id="index"
             good-title="Отзывы клиентов"
             good-url="http://dropshop.kz/index.php">
        </div>

    </div>

    <div style="margin: 25px 0"></div>
    <script async="async" type="text/javascript"
            src="//static.sprosikupi.ru/js/widget/sprosikupi.bootstrap.js">
    </script>

    <div id="Layer8" style="position:relative;text-align:center;left:0%;width:100%;z-index:158;" title="">
        <div id="wb_Text12" style="top:-30px;width:688px;height:44px;text-align:center;margin: 15px auto;">
            <div id="adres"><span style="color:#000000;font-family:ralewaylight;font-size:37px;">Адрес нашего магазина</span></div></div>
        <div id="wb_Text12" style="left:0%;top:70px;width:100%;text-align:center;">
            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=00Su50IqsXM0Yna-PAwxJPjyqiiJfNNy&width=100%&height=400"></script>
        </div>
    </div>
    <div id="Layer7" style="position:relative;text-align:center;left:0%;width:100%;height:350px;z-index:159;" title="">
        <div id="Layer7_Container" style="width:1000px;position:relative;margin-left:auto;margin-right:auto;text-align:left;">
            <div id="wb_Image53" style="position:absolute;left:41px;top:49px;width:228px;height:39px;z-index:143;">
                <img src="/images/logfuter.png" id="Image53" alt="" style="width:228px;height:39px;"></div>
            <div id="wb_Text71" style="position:absolute;left:41px;top:121px;width:408px;height:96px;z-index:144;text-align:left;">
                <span style="color:#c0c0c0;font-family:ralewaylight;font-size:20px;">&#1048;&#1055; &quot;RAYANA&quot;<br>&#1070;&#1088;&#1080;&#1076;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1081; &#1080; &#1092;&#1072;&#1082;&#1090;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1081; &#1072;&#1076;&#1088;&#1077;&#1089;: &#1075;.&#1040;&#1083;&#1084;&#1072;&#1090;&#1099;, &#1091;&#1083;.&#1040;&#1073;&#1072;&#1103;, &#1076;.44, &#1058;&#1056;&#1062; &#8220;Symphony&#8221;<br></span></div>
            <div id="wb_Text72" style="position:absolute;left:621px;top:43px;width:347px;height:125px;text-align:right;z-index:145;">
                <span class="lptracker_phone" style="color:#ffffff;font-family:ralewaylight;font-size:21px;">&#1058;&#1077;&#1083;. 8 727 224 27 62<br></span>
                <span class="lptracker_phone" style="color:#ffffff;font-family:ralewaylight;font-size:21px;">8 775 762 12 82</span>
                <span style="color:#ffffff;font-family:ralewaylight;font-size:21px;"><br><br>Email : dropshop.kz@gmail.com<br>Skype : dropshop.kz</span></div>
            <div id="wb_Image55" style="position:absolute;left:795px;top:71px;width:23px;height:23px;z-index:146;">
                <img src="/images/wh.png" id="Image55" alt="" style="width:23px;height:23px;"></div>
            <div id="wb_Image56" style="position:absolute;left:751px;top:201px;width:52px;height:52px;z-index:147;">
                <a href="https://vk.com/dropshop_kz" target="_blank"><img src="/images/tuzk0HKJfzM.jpg" id="Image56" alt="" style="width:52px;height:52px;"></a></div>
            <div id="wb_Image57" style="position:absolute;left:808px;top:201px;width:50px;height:50px;z-index:148;">
                <a href="https://instagram.com/dropshop.kz/" target="_blank"><img src="/images/insta.png" id="Image57" alt="" style="width:50px;height:50px;"></a></div>
            <div id="wb_Image58" style="position:absolute;left:863px;top:201px;width:50px;height:50px;z-index:149;">
                <img src="/images/face.png" id="Image58" alt="" style="width:50px;height:50px;"></div>
            <div id="wb_Image59" style="position:absolute;left:919px;top:201px;width:50px;height:50px;z-index:150;">
                <img src="/images/you.png" id="Image59" alt="" style="width:50px;height:50px;"></div>
            <div id="wb_Image60" style="position:absolute;left:490px;top:51px;width:100px;height:100px;z-index:150;">
                <img src="/images/barcode.gif" id="Image60" alt="" style="width:100px;height:100px;"></div>
            <div id="wb_Text71" style="position:absolute;left:0%;top:230px;width:600px;height:96px;z-index:13;text-align:left;">
                <span style="color:#c0c0c0;font-family:ralewaylight;font-size:8px;">ПОЛИТИКА КОНФИНДЕНЦИАЛЬНОСТИ<br>Сайт уважает ваше право и соблюдает конфиденциальность при заполнении, передачи и хранении ваших конфиденциальных сведений. Размещение заявки на сайте означает Ваше согласие на обработку данных. Под персональными данными подразумевается информация, относящаяся к субъекту персональных данных, в частности фамилия, имя и отчество, дата рождения, адрес, контактные реквизиты (телефон, адрес электронной почты), семейное, имущественное положение и иные данные.<br><br><br>Целью обработки персональных данных является оказание сайтом услуг. В случае отзыва согласия на обработку своих персональных данных мы обязуемся удалить Ваши персональные данные в срок не позднее 3 рабочих дней. Отзыв согласия можно отправить в электронном виде на наш электронный адресс. e-mail: info@dropshop.kz</span></div>
        </div>
    </div>

</div>

<!-- HelloPreload http://hello-site.ru/preloader/ 
   <!-- <div id="hellopreloader"><div id="hellopreloader_preload"></div><p><a href="http://hello-site.ru">Hello-Site.ru. Бесплатный конструктор сайтов.</a></p></div>-->
<script>var hellopreloader = document.getElementById("hellopreloader_preload");function fadeOutnojquery(el){el.style.opacity = 1;var interhellopreloader = setInterval(function(){el.style.opacity = el.style.opacity - 0.05;if (el.style.opacity <=0.05){ clearInterval(interhellopreloader);hellopreloader.style.display = "none";}},16);}window.onload = function(){setTimeout(function(){fadeOutnojquery(hellopreloader);},1000);};</script>
<!-- HelloPreload http://hello-site.ru/preloader/  -->

<!-- Код тега ремаркетинга Google -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 955541380;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/955541380/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

</body>
</html>