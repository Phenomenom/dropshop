<?php

use backend\models\Accessory;
use backend\models\Product;
use backend\models\Setting;
use common\helpers\shoppingcart\ShoppingCart;
use frontend\models\RequestForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $requestForm RequestForm */
/* @var $products Product[] */
/* @var $accessories Accessory[] */
/* @var $cost int */
/* @var $count int */

/* @var $rate int */

$this->title = Yii::$app->name.' - '.Yii::t('app', 'Cart');
$this->params['breadcrumbs'][] = ['label' => 'Корзина'];

const IMAGE_COUNT = 4;
const JS_IMAGE = 'js-image-';
const JS_BUTTON = 'js-button-';

?>

<style>
    .js-button-remove {
        padding: 0;
    }
</style>

<div class="cart-background">
    <div class="cart">

        <h2>Корзина заказов</h2>
        <?php Pjax::begin([
            'id' => 'cart-pjax',
            'timeout' => 5000
        ]) ?>
        <?php if ($count > 0): ?>

            <?php if (sizeof($products) > 0): ?>
                <h4>Продукты:</h4>
                <table class="table table-striped table-bordered detail-view">
                    <thead>
                        <tr>
                            <td style="width: 50%">Название</td>
                            <td>Тип</td>
                            <td>Цена</td>
                            <td>Количество</td>
                            <td>Сумма</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td><?= Html::a($product->name, $product->link) ?></td>
                            <td>
                                <?php
                                if(($productSet = $product->getProductSet()) !== null) {
                                    echo $productSet->text;
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                $price = $product->price;
                                echo number_format($price, 0, ',', ' ') . ' тг';
                                ?>
                            </td>
                            <td><?= $product->quantity ?></td>
                            <td><?= number_format($product->quantity * $price, 0, ',', ' ') . ' тг' ?></td>
                            <td>
                                <?php
                                $params = [
                                    'class' => 'btn btn-link js-button-remove',
                                    'id' => $product->id,
                                    'type' => ShoppingCart::TYPE_PRODUCT,
                                    'product-set' => -1,
                                ];
                                if(($productSet = $product->getProductSet()) !== null) {
                                    $params['product-set'] = $productSet->id;
                                }
                                echo Html::button('<span class="glyphicon glyphicon-remove"></span>', $params)
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>

            <?php if (sizeof($accessories) > 0): ?>
                <h4>Аксессуары:</h4>
                <table class="table table-striped table-bordered detail-view">
                    <thead>
                        <tr>
                            <td style="width: 50%">Название</td>
                            <td>Цена</td>
                            <td>Количество</td>
                            <td>Сумма</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($accessories as $accessory): ?>
                        <tr>
                            <td><?= $accessory->name ?></td>
                            <td><?= number_format($accessory->price, 0, ',', ' ') . ' тг' ?></td>
                            <td><?= $accessory->quantity ?></td>
                            <td><?= number_format($accessory->quantity * $accessory->price, 0, ',', ' ') . ' тг' ?></td>
                            <td>
                                <?php
                                $params = [
                                    'class' => 'btn btn-link js-button-remove',
                                    'id' => $accessory->id,
                                    'type' => ShoppingCart::TYPE_ACCESSORY,
                                    'product-set' => -1,
                                ];
                                echo Html::button('<span class="glyphicon glyphicon-remove"></span>', $params)
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>

            <h4>Общая стоимость: <?= number_format($cost, 0, ',', ' ') ?> тг</h4>

            <?php
            Modal::begin([
                'header' => '<h3>Заказать</h3>',
                'id' => 'js-modal-order',
                'toggleButton' => ['label' => 'Заказать', 'class' => 'product-header__order-my btn btn-danger'],
            ]);
            echo $this->render('_modal_order', ['requestForm' => $requestForm]);
            Modal::end();
            ?>

        <?php else: ?>
            <p>В корзине нет заказов</p>
        <?php endif; ?>

        <?php Pjax::end() ?>
    </div>
</div>



<?php
$delete_url = Url::to(['remove-product']);
$update_url = Url::to(['remove-product']);

$js         = <<<JS
        $(document).ready(function(){

            $('.cart').on('keyup', 'input[name="js-quantity"]', function() {
                var _this = $(this);
                var id = $(this).attr('product-id');
                if(!$(this).val())
                    $(this).val(1);
                var quantity = $(this).val();

                $.post(
                    "{$update_url}",{id : id, quantity : quantity}).done(function (data) {
                        updateList();
                        updateCartCount(data['count']);
                    }).fail(function (data) {
                        console.log('connection error');
                    });
            });

            $('.cart').on('click', '.js-button-remove', function() {
                var r = confirm("Убрать продукт из корзины?");
                if(r == true) {
                    var id = $(this).attr('id');
                    var type = $(this).attr('type');
                    var product_set = $(this).attr('product-set');
                    $.post(
                        "{$delete_url}",{id : id, type : type, product_set : product_set}).done(function (data) {
                            updateCartCount(data['count']);
                            $.pjax.reload({container:'#cart-pjax'});
                        }).fail(function (data) {
                            $.pjax.reload({container:'#cart-pjax'});
                            alert(data.statusText);
                        });
                }
            });

            function updateCartCount(count) {
                if(count > 0)
                    $('.js-cart-icon').text('(' + count + ')');
                else
                    $('.js-cart-icon').text('');
            }

            function updateList() {
                var cartTotal = 0;
                var cartCount = 0;
                $('.js-calc').each(function() {
                    var quantity = $('input[name="js-quantity"]', this).val();
                    var price = $('.js-price', this).text();
                    var total = $('.js-total', this);
                    total.text(quantity * price);

                    cartTotal += parseInt(total.text());
                    cartCount += parseInt(quantity);
                });
                $('.js-cart-count').text(cartCount);
                $('.js-cart-total').text(cartTotal);
            }

        });
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>