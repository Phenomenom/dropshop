<?php

use frontend\models\RequestForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $requestForm RequestForm */

?>

<?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    'action' => ['cart/order'],
]); ?>

    <?= $form->field($requestForm, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($requestForm, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($requestForm, 'city')->textInput(['maxlength' => true]) ?>

    <?= Html::submitButton(Yii::t('app', 'Checkout'), ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end(); ?>

<?php $this->registerJsFile('/js/jquery.maskedinput.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php
$js         = <<<JS
    $(document).ready(function(){
        $('#requestform-phone').mask('+7 (799) 999-9999');
    });
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>
