<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name.' - Заказ успешно оформлен';

?>

<style>
    .success-message {
        display: flex;
        width: 100%;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        height: 200px;
    }
</style>

<div class="success-message">
<h1>Спасибо за заказ!</h1>
<h3>В ближающее время ваш заказ будет расмотрен</h3>
</div>
