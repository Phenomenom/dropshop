<?php

namespace common\helpers\shoppingcart;

use backend\models\Accessory;
use backend\models\OrderItem;
use backend\models\Product;
use backend\models\ProductSet;
use Yii;
use yii\base\Component;
use yii\di\Instance;
use yii\web\NotFoundHttpException;
use yii\web\Session;


/**
 * Class ShoppingCart
 * @property int[] $items
 * @property int $count Total count of items in the cart
 * @property int $cost Total cost of items in the cart
 * @property bool $isEmpty Returns true if cart is empty
 * @property string $hash Returns hash (md5) of the current cart, that is unique to the current combination
 * of items, quantities and costs
 * @property string $serialized Get/set serialized content of the cart
 * @package \common\shoppingcart
 */
class ShoppingCart extends Component
{

    // If item_type is Product, then product can have ProductSet
    // ITEM-ID _ ITEM-TYPE _ PRODUCT-SET-ID

    const TYPE_PRODUCT = 0;
    const TYPE_ACCESSORY = 1;

    /**
     * If true (default) cart will be automatically stored in and loaded from session.
     * If false - you should do this manually with saveToSession and loadFromSession methods
     * @var bool
     */
    public $storeInSession = true;
    /**
     * Session component
     * @var string|Session
     */
    public $session = 'session';
    /**
     * Shopping cart ID to support multiple carts
     * @var string
     */
    public $cartId = __CLASS__;
    /**
     * @var int[]
     */
    protected $_items = [];

    public function init()
    {
        if ($this->storeInSession)
            $this->loadFromSession();
    }

    /**
     * Loads cart from session
     */
    public function loadFromSession()
    {
        $this->session = Instance::ensure($this->session, Session::className());
        if (isset($this->session[$this->cartId]))
            $this->setSerialized($this->session[$this->cartId]);
    }

    /**
     * Saves cart to the session
     */
    public function saveToSession()
    {
        $this->session = Instance::ensure($this->session, Session::className());
        $this->session[$this->cartId] = $this->getSerialized();
    }

    /**
     * Sets cart from serialized string
     * @param string $serialized
     */
    public function setSerialized($serialized)
    {
        $this->_items = unserialize($serialized);
    }

    public function put($type, $item_id, $product_set_id = null, $quantity = 1)
    {
        if($type == self::TYPE_PRODUCT) {
            $product = self::productExist($item_id);
            if ($product === null) {
                throw new NotFoundHttpException('Product does not exist');
            }

            if($product_set_id === null && sizeof($product->productSets) > 0) {
                $product_set_id = $product->productSets[0]->id;
            }

            if($product_set_id !== null) {
                if (self::productSetExist($product_set_id) === null) {
                    throw new NotFoundHttpException('ProductSet does not exist');
                }
            }
        }

        if($type == self::TYPE_ACCESSORY) {
            if (self::accessoryExist($item_id) === null) {
                throw new NotFoundHttpException('Accessory does not exist');
            }
        }

        if($product_set_id !== null /*&& !self::TYPE_ACCESSORY*/) {
            $key = $item_id . '_' . $type . '_' . $product_set_id;
        } else {
            $key = $item_id . '_' . $type;
        }

        if (isset($this->_items[$key])) {
            $this->_items[$key] += $quantity;
        } else {
            $this->_items[$key] = $quantity;
        }
        if ($this->storeInSession)
            $this->saveToSession();
    }

    /**
     * Returns cart items as serialized items
     * @return string
     */
    public function getSerialized()
    {
        return serialize($this->_items);
    }

    public function update($type, $item_id, $product_set_id, $quantity)
    {
        if($product_set_id !== null) {
            $key = $item_id . '_' . $type . '_' . $product_set_id;
        } else {
            $key = $item_id . '_' . $type;
        }

        if(array_key_exists($key, $this->_items)) {
            if ($quantity <= 0) {
                $this->remove($type, $item_id);
                return;
            }

            $this->_items[$item_id] = $quantity;

            if ($this->storeInSession)
                $this->saveToSession();
        }
    }

    public function remove($type, $item_id, $product_set_id = null)
    {
        if($product_set_id !== null) {
            $key = $item_id . '_' . $type . '_' . $product_set_id;
        } else {
            $key = $item_id . '_' . $type;
        }

        if(array_key_exists($key, $this->_items)) {
            unset($this->_items[$key]);
            if ($this->storeInSession)
                $this->saveToSession();
            return true;
        }
        return false;
    }

    public function removeAll()
    {
        $this->_items = [];
        if ($this->storeInSession)
            $this->saveToSession();
    }

    /**
     * Returns item by it's id. Null is returned if item was not found
     * @param $type
     * @param $item_id
     * @return int|null
     */
    public function getItemById($type, $item_id)
    {
        $key = $item_id . '_' . $type;

        if (isset($this->_items[$key]))
            return $this->_items[$key];
        else
            return null;
    }

    /**
     * @return int[]
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * @return Product[]
     */
    public function getProducts() {
        $products = [];
        $items = $this->getItems();
        foreach($items as $key => $quantity) {

            $array = explode('_', $key);
            $item_id = $array[0];
            $type = $array[1];

            $product_set_id = null;
            if(sizeof($array) > 2) {
                $product_set_id = $array[2];
            }

            /* @var $product Product */
            if($type == self::TYPE_PRODUCT) {
                if(($product = self::productExist($item_id)) !== null) {
                    if($product_set_id !== null) {
                        /* @var $product_set ProductSet */
                        if(($product_set = self::productSetExist($product_set_id)) !== null) {
                            $product->product_set_id = $product_set->id;
                        }
                    }
                    $product->quantity = $quantity;
                    $product->price = $product->getPriceRound();
                    $products[] = $product;
                } else {
                    unset($items[$key]);
                }
            }

        }
        $this->setItems($items);
        return $products;
    }

    /**
     * @return Accessory[]
     */
    public function getAccessories() {
        $accessories = [];
        $items = $this->getItems();
        foreach($items as $key => $quantity) {

            $array = explode('_', $key);
            $item_id = $array[0];
            $type = $array[1];

            /* @var $accessory Accessory */
            if($type == self::TYPE_ACCESSORY) {
                if((($accessory = self::accessoryExist($item_id)) !== null) && sizeof($array) == 2) {
                    $accessory->quantity = $quantity;
                    $accessory->price = $accessory->getPriceRound();
                    $accessories[] = $accessory;
                } else {
                    unset($items[$key]);
                }
            }
        }
        $this->setItems($items);
        return $accessories;
    }

    /**
     * @param $product_id
     * @return null|Product
     */
    public static function productExist($product_id) {
        $product = Product::findOne($product_id);
        return $product;
    }

    /**
     * @param $accessory_id
     * @return null|Accessory
     */
    public static function accessoryExist($accessory_id) {
        $accessory = Accessory::findOne(['id' => $accessory_id]);
        return $accessory;
    }

    /**
     * @param $product_set_id
     * @return null|ProductSet
     */
    public static function productSetExist($product_set_id) {
        $productSet = ProductSet::findOne(['id' => $product_set_id]);
        return $productSet;
    }

    public function setItems($items)
    {
        foreach($items as $key => $quantity) {
            if($quantity <= 0) unset($items[$key]);
        }
        $this->_items = $items;
        if ($this->storeInSession)
            $this->saveToSession();
    }

    public function getIsEmpty()
    {
        return count($this->_items) == 0;
    }

    public function getCount()
    {
        $count = 0;
        foreach ($this->_items as $key => $quantity) {
            $count += $quantity;
        }
        return $count;
    }

    /**
     * Return full cart cost as a sum of the individual items costs
     * @return int
     */
    public function getCost()
    {
        $cost = 0;

        $products = $this->getProducts();
        foreach($products as $product) {
            $cost += $product->price * $product->quantity;
        }

        $accessories = $this->getAccessories();
        foreach($accessories as $accessory) {
            $cost += $accessory->price * $accessory->quantity;
        }

        return $cost;
    }

    public static function getTypeLabels() {
        return [
            self::TYPE_PRODUCT => Yii::t('app', 'Product'),
            self::TYPE_ACCESSORY => Yii::t('app', 'Accessory'),
        ];
    }
}
