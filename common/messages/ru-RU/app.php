<?php
return [
    // General
    'Create' => 'Добавить',
    'Update' => 'Редактировать',
    'Created At' => 'Создан',
    'Updated At' => 'Изменен',
    'Delete' => 'Удалить',
    'Value' => 'Значение',
    'Null' => 'Не определено',
    'Not important' => 'Необязательный параметр',
    'Undefined' => 'Неопределен',
    'Image' => 'Изображение',
    'Currency' => 'Валюта',
    'Tenge' => 'Тенге',
    'USD' => 'USD',
    'Search' => 'Поиск',
    'Reset' => 'Сбросить',

    // Product
    'Product' => 'Гаджет',
    'Products' => 'Гаджеты',
    'Name' => 'Название',
    'Brand' => 'Бренд',
    'Type' => 'Тип гаджета',
    'Price' => 'Цена ($)',
    'Price Credit' => 'Цена (кредит)',
    'Price Round' => 'Окр. цена (тг)',
    'Create Product' => 'Добавить гаджет',
    'Update Product' => 'Редактировать гаджет',
    'Video URL' => 'Ссылка на видео',
    'Description' => 'Описание',
    'Priority' => 'Приоритет',
    'States' => 'Состояния',
    'Product Mates' => 'Похожие гаджеты',

    // Product Types
    'Product Types' => 'Типы гаджетов',
    'Product Type' => 'Тип гаджета',
    'Create Product Type' => 'Добавить тип гаджета',
    'Update Product Type' => 'Редактировать тип гаджета',

    // Product Set
    'Product Set' => 'Разновидность',
    'Return to Product' => 'Вернуться к гаджету',
    'Create Product Set' => 'Добавить разновидность гаджета',
    'Update Product Set' => 'Редактировать разновидность гаджета',

    // Accessories
    'Accessory' => 'Аксессуар',
    'Accessories' => 'Аксессуары',
    'Create Accessory' => 'Добавить аксессуар',
    'Update Accessory' => 'Редактировать аксессуар',

    // Accessory Types
    'Accessory Type' => 'Тип аксессуара',
    'Accessory Types' => 'Типы аксессуаров',
    'Create Accessory Type' => 'Добавить тип аксессуара',
    'Update Accessory Type' => 'Редактировать тип аксессуара',

    // Users
    'Users' => 'Пользователи',
    'Create User' => 'Добавить пользователя',
    'Username' => 'Логин',
    'Email' => 'E-mail',
    'Status' => 'Статус',

    // Chars
    'Chars' => 'Характеристики',
    'Characteristic' => 'Характеристика',
    'Char List' => 'Список характеристик',
    'Char Categories' => 'Категория характеристик',
    'Values' => 'Значения',
    'Create Char' => 'Добавить характеристику',
    'Update Char' => 'Редактировать характеристику',
    'Select chars' => 'Выберите характеристики',
    'Auto addition' => 'Автодобавление к товару',

    // Categories
    'Categories' => 'Категории',
    'Category' => 'Категория',
    'Create Category' => 'Добавить категорию',
    'Update Category' => 'Редактировать категорию',
    '-- Select Category --' => '-- Выбрать категорию --',

    // ProductImages
    'Images' => 'Изображения',

    // Settings
    'Settings' => 'Настройки',
    'Save' => 'Сохранить',

    // Requests
    'Requests' => 'Заявки',
    'Status New' => 'Новый',
    'Status Viewed' => 'Просмотрен',
    'Status Completed' => 'Выполнен',
    'Phone' => 'Телефон',
    'City' => 'Адрес',
    'Body' => 'Описание',
    'Product ID' => 'ID Товара',
    'Created at' => 'Создан',
    'Person Name' => 'Имя',
    'City Request' => 'Город',

    'Smartphone' => 'Смартфоны',
    'Part' => 'Ремонт & Запчасти',
    'Gadget' => 'Гаджеты',

    // Posts
    'Posts' => 'Новости',
    'Create Post' => 'Добавить Новость',
    'Views' => 'Просмотры',
    'Title' => 'Оглавление',
    'Short Text' => 'Краткое описание',
    'Full Text' => 'Полное описание',
    'Add Date' => 'Дата добавления',
    'Post Hidden' => 'Новость скрыта',
    'Post Published' => 'Новость опубликована',

    // Slider
    'Slider' => 'Слайдер',
    'Sliders' => 'Элементы слайдера',
    'Create Slider' => 'Добавить элемент слайдера',
    'Update Slider' => 'Изменить элемент слайдера',
    'Link' => 'Ссылка',
    'Text' => 'Текст',

    // Brands
    'Brands' => 'Бренды',
    'Create Brand' => 'Добавить бренд',
    'Update Brand' => 'Редактировать бренд',

    // PriceMail
    'Broadcast' => 'Рассылка (цены)',
    'Price Mails' => 'Рассылка о снижении цены',
    'Create Price Mail' => 'Добавить рассылку',
    'Update Price Mail' => 'Редактировать рассылку',

    // Cart
    'Cart' => 'Корзина',
    'To Cart' => 'В корзину',
    'Checkout' => 'Оформить заказ',

];