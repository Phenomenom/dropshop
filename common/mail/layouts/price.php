<?php
use backend\models\Product;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product Product */
/* @var $rate int */

/* @var $email string */
/* @var $hash string */

$unsignLink = Yii::$app->urlManager->createAbsoluteUrl(['price-mail/unsign',
    'email' => $email,
    'hash' => $hash
]);

?>
<div class="password-reset">
    <p>Вы подписались на уведомление о снижении цены продукта</p>

    <h3><?= $product->name ?></h3>

    <p>Сейчас его стоимость составляет? <?= $product->price ?> USD</p>
    <p>Курс тенге: <?= $rate?></p>

    <p>Пройдите по следующей ссылке, если вы больше не хотите получать уведомления на данный продукт</p>

    <p><?= Html::a(Html::encode($unsignLink), $unsignLink) ?></p>
</div>
