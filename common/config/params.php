<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'mail_to' => 'Dropshop.kz@gmail.com',
    'mail_to_copy' => 'kaz_ktl_93@mail.ru',

    'shopMail' => 'info@dropshop.kz',
];
