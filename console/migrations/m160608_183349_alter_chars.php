<?php

use yii\db\Migration;

class m160608_183349_alter_chars extends Migration
{
    public function up()
    {
        $this->addColumn('chars', 'product_type_id', $this->integer());
        $this->addForeignKey('fk_char_product_type', 'chars', 'product_type_id', 'product_types', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_char_product_type', 'chars');
        $this->dropColumn('chars', 'product_type_id');
    }
}
