<?php

use yii\db\Migration;

/**
 * Handles the creation for table `slider`.
 */
class m160609_175916_create_slider extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
            'link' => $this->string(255),
            'text' => $this->text(),
        ]);
        $this->createTable('price_mail', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'email' => $this->string(255),
        ]);
        $this->addForeignKey('fk_price_mail_product', 'price_mail', 'product_id', 'products', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_price_mail_product', 'price_mail');
        $this->dropTable('price_mail');
        $this->dropTable('slider');
    }
}
