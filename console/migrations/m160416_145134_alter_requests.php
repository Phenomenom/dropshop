<?php

use yii\db\Migration;
use yii\db\Schema;

class m160416_145134_alter_requests extends Migration
{
    public function up()
    {
        $this->addColumn('requests', 'status', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('requests', 'created_at', $this->timestamp()->notNull().' DEFAULT CURRENT_TIMESTAMP');
    }

    public function down()
    {
        $this->dropColumn('requests', 'status');
        $this->dropColumn('requests', 'created_at');
    }
}
