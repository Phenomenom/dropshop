<?php

use yii\db\Migration;

class m160725_151239_alter_foreign_keys extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_price_mail_product', 'price_mail');
        $this->addForeignKey('fk_price_mail_product', 'price_mail', 'product_id', 'products', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_price_mail_product', 'price_mail');
        $this->addForeignKey('fk_price_mail_product', 'price_mail', 'product_id', 'products', 'id');
    }
}
