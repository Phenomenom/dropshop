<?php

use yii\db\Migration;

class m160609_153632_remode_alias extends Migration
{
    public function up()
    {
        $this->dropColumn('chars', 'alias');
    }

    public function down()
    {
        $this->addColumn('chars', 'alias', $this->string(255)->notNull()->defaultValue(''));
    }
}
