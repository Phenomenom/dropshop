<?php

use yii\db\Migration;

class m160607_161434_mega_migration extends Migration
{
    public function up()
    {
        $this->createTable('brands',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('product_types',[
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->addColumn('products', 'brand_id', $this->integer());
        $this->addColumn('products', 'product_type_id', $this->integer());
        $this->addColumn('products', 'states', $this->string(255));

        $this->addForeignKey('fk_product_brand2', 'products', 'brand_id', 'brands', 'id');
        $this->addForeignKey('fk_product_product_type', 'products', 'product_type_id', 'product_types', 'id');

        $this->createTable('product_sets',[
            'id' => $this->primaryKey(),
            'price' => $this->integer()->notNull(),
            'text' => $this->string(255),
            'product_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_product_set_product', 'product_sets', 'product_id', 'products', 'id');

        $this->createTable('product_product_map',[
            'product_id' => $this->integer()->notNull(),
            'product_mate_id' => $this->integer()->notNull(),
            'PRIMARY KEY (product_id, product_mate_id)',
        ]);
        $this->addForeignKey('product_product_map_product1', 'product_product_map', 'product_id', 'products', 'id');
        $this->addForeignKey('product_product_map_product2', 'product_product_map', 'product_mate_id', 'products', 'id');

        $table = Yii::$app->db->schema->getTableSchema('products');
        if(isset($table->columns['type'])) {
            $this->dropColumn('products', 'type');
        }
        if(isset($table->columns['status'])) {
            $this->dropColumn('products', 'status');
        }
        if(isset($table->columns['prices'])) {
            $this->dropColumn('products', 'prices');
        }
    }

    public function down()
    {
        $this->dropForeignKey('product_product_map_product1', 'product_product_map');
        $this->dropForeignKey('product_product_map_product2', 'product_product_map');
        $this->dropForeignKey('fk_product_set_product', 'product_sets');

        $this->dropForeignKey('fk_product_brand2', 'products');
        $this->dropForeignKey('fk_product_product_type', 'products');

        $this->dropTable('product_product_map');
        $this->dropTable('product_sets');
        $this->dropTable('product_types');
        $this->dropTable('brands');

        $this->dropColumn('products', 'brand_id');
        $this->dropColumn('products', 'product_type_id');
        $this->dropColumn('products', 'states');
    }
}
