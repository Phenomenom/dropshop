<?php

use yii\db\Migration;

class m160605_064639_alter_instock extends Migration
{
    public function up()
    {
        $this->renameColumn('products', 'instock', 'status');
        $this->addColumn('products', 'visible', $this->boolean()->notNull()->defaultValue(true));
    }

    public function down()
    {
        $this->dropColumn('products', 'visible');
        $this->renameColumn('products', 'status', 'instock');
    }
}
