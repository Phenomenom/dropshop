<?php

use yii\db\Migration;

class m160414_163834_add_descr extends Migration
{
    public function up()
    {
        $this->addColumn('products','descr',$this->text());
        $this->addColumn('products','video_url',$this->string(255));
    }

    public function down()
    {
        $this->dropColumn('produtcs','descr');
        $this->dropColumn('products','video_url');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
