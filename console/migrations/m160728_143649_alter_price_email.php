<?php

use backend\models\PriceMail;
use yii\db\Migration;

class m160728_143649_alter_price_email extends Migration
{
    public function up()
    {
        $this->addColumn('price_mail', 'hash', $this->string(255));

        /* @var $priceMails PriceMail[] */
        $priceMails = PriceMail::find()->all();
        foreach($priceMails as $priceMail) {
            $priceMail->hash = Yii::$app->security->generateRandomString();
            $priceMail->save(false);
        }
    }

    public function down()
    {
        $this->dropColumn('price_mail', 'hash');
    }
}
