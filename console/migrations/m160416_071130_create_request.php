<?php

use yii\db\Migration;

class m160416_071130_create_request extends Migration
{
    public function up()
    {
        $this->createTable('requests', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'phone' => $this->string(18)->notNull(),
            'city' => $this->string(255)->notNull(),
            'body' => $this->text(),
            'product_id' => $this->integer()
        ]);
        $this->addForeignKey('fk_request_product', 'requests', 'product_id', 'products', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropTable('requests');
    }
}
