<?php

use yii\db\Migration;

class m160525_171033_add_created_date extends Migration
{
    public function up()
    {
        $this->addColumn("products", "add_date",  $this->timestamp() . ' DEFAULT CURRENT_TIMESTAMP');
    }

    public function down()
    {
        $this->dropColumn('products', 'add_date');
    }
}
