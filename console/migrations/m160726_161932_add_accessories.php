<?php

use yii\db\Migration;

class m160726_161932_add_accessories extends Migration
{
    public function up()
    {
        $this->createTable('accessories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'price' => $this->integer()->notNull()->defaultValue(0),
            'descr' => $this->text(),
            'accessory_type_id' => $this->integer()
        ]);

        $this->createTable('accessory_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->addForeignKey('fk_accessory_accessory_type', 'accessories', 'accessory_type_id', 'accessory_types', 'id', 'SET NULL');

        $this->createTable('product_accessory_map', [
            'product_id' => $this->integer()->notNull(),
            'accessory_id' => $this->integer()->notNull(),
            'PRIMARY KEY (product_id, accessory_id)',
        ]);

        $this->addForeignKey('fk_product_accessory_map_product', 'product_accessory_map', 'product_id', 'products', 'id');
        $this->addForeignKey('fk_product_accessory_map_accessory', 'product_accessory_map', 'accessory_id', 'accessories', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_product_accessory_map_product','product_accessory_map');
        $this->dropForeignKey('fk_product_accessory_map_accessory','product_accessory_map');

        $this->dropTable('product_accessory_map');

        $this->dropForeignKey('fk_accessory_accessory_type', 'accessories');

        $this->dropTable('accessory_types');
        $this->dropTable('accessories');
    }
}
