<?php

use yii\db\Migration;

class m160409_142152_alter_products2 extends Migration
{
    public function up()
    {
        $this->addColumn('products','hash',$this->string('63')->notNull());
        $this->addColumn('product_images','type',$this->integer()->notNull()->defaultValue(0));
        $this->addColumn('product_images','name',$this->string('127')->notNull());
    }

    public function down()
    {
        $this->dropColumn('products','hash');
        $this->dropColumn('product_images','type');
        $this->dropColumn('product_images','name');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
