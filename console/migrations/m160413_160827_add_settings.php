<?php

use yii\db\Migration;

class m160413_160827_add_settings extends Migration
{
    public function up()
    {
        $this->createTable('settings',[
            'id' => $this->primaryKey(),
            'alias' => $this->string(127)->notNull(),
            'value' => $this->string(127)->notNull(),
        ]);
        $this->insert('settings',[
            'alias' => 'exchange',
            'value' => '333',
        ]);
    }

    public function down()
    {
        $this->dropTable('settings');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
