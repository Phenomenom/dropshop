<?php

use yii\db\Migration;

class m160410_093209_alter_product3 extends Migration
{
    public function up()
    {
        $this->addColumn('chars','is_required',$this->boolean()->defaultValue(false)->notNull());
        $this->dropColumn('products','brand');
    }

    public function down()
    {

    }

}
