<?php

use yii\db\Migration;

class m160416_154757_alter_products4 extends Migration
{
    public function up()
    {
        $this->addColumn('products','priority',$this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('products','priority');
    }
}
