<?php

use yii\db\Migration;

class m160408_042634_alter_products extends Migration
{
    public function up()
    {
        $this->addColumn('products','text',$this->text());
        $this->createTable('product_images',[
            'id' => $this->primaryKey(),
            'path' => $this->string(255)->notNull(),
            'product_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_product2', 'product_images', 'product_id', 'products', 'id');
    }

    public function down()
    {
        $this->dropColumn('products','text');
        $this->dropTable('product_images');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
