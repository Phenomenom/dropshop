<?php

use yii\db\Migration;

class m160728_160413_alter_requests extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_request_product', 'requests');
        $this->addColumn('requests', 'type', $this->smallInteger()->notNull()->defaultValue(0));
        $this->addColumn('requests', 'product_set_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('requests', 'product_set_id');
        $this->dropColumn('requests', 'type');
        $this->addForeignKey('fk_request_product', 'requests', 'product_id', 'products', 'id', 'CASCADE');
    }
}
