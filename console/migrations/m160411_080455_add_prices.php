<?php

use yii\db\Migration;

class m160411_080455_add_prices extends Migration
{
    public function up()
    {
        $this->addColumn('products','prices',$this->string(255));
        $this->dropForeignKey('fk_product','product_char');
        $this->dropForeignKey('fk_char','product_char');
        $this->dropIndex('unique_product_char','product_char');
        $this->addForeignKey('fk_product','product_char','product_id','products','id');
        $this->addForeignKey('fk_char','product_char','char_id','chars','id');
    }

    public function down()
    {
        $this->dropColumn('products','prices');
    }
}
